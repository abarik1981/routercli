=========
RouterCli
=========

Program
=======
Router CLI

- RouterCli is a Python program that uses expect (winexpect for windows and pexpect for Linux) for automating execution of CLI commands on network devices of various platforms

Features
========

- Ability to login to multiple routers at same time or in sequence

- Ability to do nested login

- Ability to use RSA Token as password

- Hierarchical architecture � Each Platform (e.g ASR9K) and each OS (e.g XR) has its own package

- Ability to run useful pre-defined functions (e.g reload_router()) on multiple nodes

Supported Platforms
===================
Following network devices are supported currently for following vendors:

- Cisco

	- Platform: ASR9K, OS: IOS-XR
	
	- Platform: CRS, OS: IOS-XR
	
	- Platform: C7600, OS: IOS
	
	- Platform: Nexus5000, OS: NX-OS

Dependencies
============
The only dependencies are:

- Python 2.7

- Python module: pexpect for Linux environment and winexpect,pywin32 for Windows environment

- If you get ImportError, try installing Visual Studios first (http://www.akbintel.com/mediawiki/index.php/Python/Conda#Installing_Visual_Studio)

Installation
============
1) Windows - Install Dependencies using Conda
---------------------------------------------

Follow these CLI commands to install all dependencies::

    $ # No need to be admin
    $ cd /tmp/
    $ # Install py2.7 32-bit MiniConda from here: http://conda.pydata.org/miniconda.html
    $ conda create -n routercli python
    $ activate routercli
    $
    $ # Download py2.7 pip: https://bootstrap.pypa.io/get-pip.py
    $ python get-pip.py
    $
    $ # Download py2.7 setuptools: https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
    $ python ez_setup.py
    $ 
    $ # Install dependencies
    $ conda install pywin32
    $ pip install winpexpect

2) Linux - Install Dependencies using Conda
---------------------------------------------

Follow these CLI commands to install all dependencies::

    $ # No need to sudo
    $ cd /tmp/
    $ # Install Miniconda
    $ wget http://repo.continuum.io/miniconda/Miniconda-3.5.5-Linux-x86_64.sh
    $ chmod +x Miniconda-3.5.5-Linux-x86_64.sh
    $ ./Miniconda-3.5.5-Linux-x86_64.sh
    $ 
    $ # Create a Python env
    $ conda create --name routercli python
    $ source activate routercli
    $ conda install pip
    $ 
    $ # Use following args for pip if server doesn't allow SSL: --index-url http://pypi.gocept.com/simple/ --allow-all-external --timeout 60
    $ pip install pexpect


Tutorial
========

Hello World Example to just login to one box and run commands::

	>>> #! /usr/bin/env python
	...
	>>> import Router
	>>> r1 = Router.Router('10.0.0.1')
	>>> r1.login(username='cisco', password='cisco')
	>>> r1.exec_cmd('show ip int br | i Lo')
	'Loopback0                  10.1.1.3        YES NVRAM  up                    up'
	>>> r1.logout()
	>>> 