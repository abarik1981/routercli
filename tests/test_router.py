import re
from routercli.router import Router
#import routercli.operatingsystems.iosgeneric
from routercli.operatingsystems.iosgeneric import IOSGeneric
from device_polling.utils.utils import read_json

def test_ssh(hostnames, router, username, password):
    for hostname in hostnames:
        login_cmd = 'ssh -2 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no mopgen_script@{0}'.format(hostname)
        out = router.login(login_cmd=login_cmd, username=username, password=password)
        if not out[0]:
            raise Exception(out[1])
        return out

# Testing
def main():
    # r1 = Router('R1', logger=False, logfile_console=None) # NO logging
    r1 = Router('R1', log_dir='/var/lib/logs',
                logfile_prgm='R1-prgm.log', logfile_console='R1-console.log',
                autoset_os_name=True, autoset_terminal_length=True, autoset_cast=True, timeout_login=5) # with logging
    
    
    #nested_login = [{'login_cmd': 'ASR9006-RSP0', 'username': 'cisco', 'password': 'cisco'}]
    #nested_login = [{'login_cmd': '7606a', 'username': 'cisco', 'password': 'cisco'}]
    nested_login = []
    print login_test(r1, login_cmd, username, password, nested_login)
#     test_mcast_rib(r1)
#     print r1.unicast.rib.is_directly_connected(route_prefix='96.34.49.173', intf='Ethernet1/2')
#     print r1.mcast.igmp.is_group_present(gp_addr='239.129.2.17', intf='Ethernet3/26')
    
#     
#     x1 = r1.exec_cmd('! This is a really long command......... It better not screw anything up ')
#     print 'out0', repr(x1)
#     print 'out1', repr(r1.exec_cmd('show ip int br'))
#     print 'out2', repr(r1.exec_cmd('show hosts'))
#     print 'out3', repr(r1.exec_cmd('show clock'))
#     print 'out4', repr(r1.exec_cmd('show mrib route 239.240.150.1 detail'))
#     print 'out4', repr(r1.exec_cmd('show ip mroute'))
#     
#     print repr(r1.exec_cmd('show ip int br | e un'))
    #print repr(r1.exec_cmd('admin install commit', timeout=60))
    #print repr(r1.exec_cmd('terminal exec prompt no-timestamp'))
#     o = r1.exec_cmd('show cdp neighbors detail')
#     print IOSGeneric.parse_show_cdp_neighbors_detail(o)
#     print r1.l2.get_connected_nbr('Bundle-Ether1.17')   # dtr04mdfdor
#     print r1.l2.get_connected_nbr('Ethernet18/8')   # nxs01sghlga
#     print r1.show_vdc()
    print r1.exec_cmd('show version')
    r1.logout()
    r1.__del__()
    print 'test'
    
if __name__ == '__main__':
    main()
    