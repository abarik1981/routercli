import re
import base64
from routercli.router import Router
from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS, OS_NAME_ARRIS_CMTS, OS_NAME_JUNOS, OS_NAME_SCREENOS,\
    OS_NAME_LINUX0, OS_NAME_EXTREMEXOS
from device_polling.utils.utils import read_json
import json

CONF_DATA = json.load(open('test_all_os_names.json'))
LOG_DIR = CONF_DATA['LOG_DIR']

def test_login_without_hostname(hostname, username, password, port, ipaddr, is_ssh=True):
    if is_ssh:
        login_cmd = 'ssh -2 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p  {port} {username}@{ipaddr}'.format(ipaddr=ipaddr, port=port, username=username)
    else:
        login_cmd = 'telnet {ipaddr} {port}'.format(ipaddr=ipaddr, port=port)
    r1 = Router(hostname, logger=False)
    out = r1.login(login_cmd=login_cmd, username=username, password=password)    # Without any hostname_prompt
    if not out[0]:
        print 'out', out, 'LOG_DIR', LOG_DIR
        raise Exception(out[1], hostname, username, password, port, ipaddr)
    return r1

def test_login(hostname, username, password, port, ipaddr, is_ssh=True, **kwargs):
    os_name = kwargs.get('os_name')
    if is_ssh:
        login_cmd = 'ssh -2 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p  {port} {username}@{ipaddr}'.format(ipaddr=ipaddr, port=port, username=username)
    else:
        login_cmd = 'telnet {ipaddr} {port}'.format(ipaddr=ipaddr, port=port)
    logfile_prgm = hostname + '-prgm.log'
    logfile_console = hostname + '-console.log'
    if os_name == OS_NAME_EXTREMEXOS:
        r1 = Router(hostname, log_dir=LOG_DIR, logfile_prgm=logfile_prgm, logfile_console=logfile_console,
                    autoset_os_name=False, autoset_cast=True, timeout_login=5, timeout_session=10, logger=False, delay_before_cmd=0.1, os_name=OS_NAME_EXTREMEXOS)
        CHTR_HOSTNAME_PROMPT = '.*?{hostname}.*?\s+'
        hostname_prompt = CHTR_HOSTNAME_PROMPT.format(hostname=hostname)
    else:
        r1 = Router(hostname, log_dir=LOG_DIR, logfile_prgm=logfile_prgm, logfile_console=logfile_console,
                autoset_os_name=True, autoset_terminal_length=True, autoset_cast=True, timeout_login=5)
        CHTR_HOSTNAME_PROMPT = '([^\s\*]+)?{hostname}'
        hostname_prompt = CHTR_HOSTNAME_PROMPT.format(hostname=hostname)
    out = r1.login(login_cmd=login_cmd, username=username, password=password, hostname_prompt=hostname_prompt, timeout=10)
    if not out[0]:
        print 'out', out, 'LOG_DIR', LOG_DIR
        raise Exception(out[1], hostname, username, password, port, ipaddr)
    if os_name == OS_NAME_EXTREMEXOS:
        r1._prompt_current = '.*?{hostname}.*?\s+#\s+'.format(hostname=hostname)
        r1.delay_before_cmd = 0
    return r1

def test_ios():
    print 'Testing IOS'
    port, ipaddr, username, password, hostname = CONF_DATA['IOS']['port'], CONF_DATA['IOS']['ipaddr'], CONF_DATA['IOS']['username'], CONF_DATA['IOS']['password'], CONF_DATA['IOS']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr, is_ssh=False)
    assert (r1.os_name == OS_NAME_IOS)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, username, password, port, ipaddr, is_ssh=False)
    r1.logout()

def test_ios_xr():
    print 'Testing IOSXR'
    port, ipaddr, username, password, hostname = CONF_DATA['IOSXR']['port'], CONF_DATA['IOSXR']['ipaddr'], CONF_DATA['IOSXR']['username'], CONF_DATA['IOSXR']['password'], CONF_DATA['IOSXR']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr, is_ssh=False)
    assert (r1.os_name == OS_NAME_IOS_XR)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, username, password, port, ipaddr, is_ssh=False)
    r1.logout()

def test_nxos():
    print 'Testing NXOS'
    port, ipaddr, username, password, hostname = CONF_DATA['NXOS']['port'], CONF_DATA['NXOS']['ipaddr'], CONF_DATA['NXOS']['username'], CONF_DATA['NXOS']['password'], CONF_DATA['NXOS']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr, is_ssh=False)
    assert (r1.os_name == OS_NAME_NX_OS)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, username, password, port, ipaddr, is_ssh=False)
    r1.logout()

def test_arris_cmts(username, password):
    print 'Testing ARRIS CMTS'
    port, ipaddr, username1, password1, hostname = CONF_DATA['ARRIS_CMTS']['port'], CONF_DATA['ARRIS_CMTS']['ipaddr'], CONF_DATA['ARRIS_CMTS']['username'], CONF_DATA['ARRIS_CMTS']['password'], CONF_DATA['ARRIS_CMTS']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr)
    assert (r1.os_name == OS_NAME_ARRIS_CMTS)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, username, password, port, ipaddr)
    r1.logout()

def test_junos(username, password):
    print 'Testing JUNOS'
    port, ipaddr, username_encoded, password_encoded, hostname = CONF_DATA['JUNOS']['port'], CONF_DATA['JUNOS']['ipaddr'], CONF_DATA['JUNOS']['username_encoded'], CONF_DATA['JUNOS']['password_encoded'], CONF_DATA['JUNOS']['hostname']
    r1 = test_login(hostname, base64.b64decode(username_encoded), base64.b64decode(password_encoded), port, ipaddr)
    r1.exec_cmd('show interfaces brief', timeout=10)
    assert (r1.os_name == OS_NAME_JUNOS)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, base64.b64decode(username_encoded), base64.b64decode(password_encoded), port, ipaddr)
    r1.logout()

def test_screenos(username, password):
    print 'Testing SCREENOS'
    port, ipaddr, username_encoded, password_encoded, hostname = CONF_DATA['SCREENOS']['port'], CONF_DATA['SCREENOS']['ipaddr'], CONF_DATA['SCREENOS']['username_encoded'], CONF_DATA['SCREENOS']['password_encoded'], CONF_DATA['SCREENOS']['hostname']
    r1 = test_login(hostname, base64.b64decode(username_encoded), base64.b64decode(password_encoded), port, ipaddr)
    r1.exec_cmd('get system version', timeout=10)
    assert (r1.os_name == OS_NAME_SCREENOS)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, base64.b64decode(username_encoded), base64.b64decode(password_encoded), port, ipaddr)
    r1.logout()

def test_linux0():
    print 'Testing LINUX'
    port, ipaddr, username, password, hostname = CONF_DATA['LINUX0']['port'], CONF_DATA['LINUX0']['ipaddr'], CONF_DATA['LINUX0']['username'], CONF_DATA['LINUX0']['password'], CONF_DATA['LINUX0']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr)
    r1.exec_cmd('ls -la')
    r1.exec_cmd('pwd')
    assert (r1.os_name == OS_NAME_LINUX0)
    r1.logout()
    
    r1 = test_login_without_hostname(hostname, username, password, port, ipaddr)
    r1.logout()

def test_extremexos(username, password):
    print 'Testing EXTREMEXOS'
    port, ipaddr, hostname = CONF_DATA['EXTREMEXOS']['port'], CONF_DATA['EXTREMEXOS']['ipaddr'], CONF_DATA['EXTREMEXOS']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr, is_ssh=False, os_name=OS_NAME_EXTREMEXOS)
    r1.exec_cmd('show configuration')
    assert (r1.os_name == OS_NAME_EXTREMEXOS)
    r1.logout()
    

def test_basic_login(username, password):
    test_ios()
    test_ios_xr()
    test_nxos()
    test_arris_cmts(username, password)
    test_junos(username, password)
    test_screenos(username, password)
    test_linux0()
    test_extremexos(username, password)

def test_ios_xr_copy_tftp_run():
    print 'Testing test_ios_xr_copy_tftp_run'
    port, ipaddr, username, password, hostname = CONF_DATA['IOSXR']['port'], CONF_DATA['IOSXR']['ipaddr'], CONF_DATA['IOSXR']['username'], CONF_DATA['IOSXR']['password'], CONF_DATA['IOSXR']['hostname']
    r1 = test_login(hostname, username, password, port, ipaddr, is_ssh=False)
    print repr(r1.exec_cmd('copy tftp://172.16.1.254/PEER-OUTBOUND-PROTECTION-XR run'))
    print repr(r1.exec_cmd('show rpl prefix-set PEER-OUTBOUND-PROTECTION'))
    r1.logout()

def main():
    d = read_json(CONF_DATA['AUTH_FILE'])['HPNA']
    username = base64.b64decode(d['user'])
    password = base64.b64decode(d['password'])
#     return test_ios_xr_copy_tftp_run()
    return test_basic_login(username, password)
    
    
if __name__ == '__main__':
    main()
