'''
Created on Mar 15, 2014

@author: ambarik
'''

import sys
import os
import ntpath
if 'cygwin' in sys.platform:    # To support Cygwin
    os_path_pi = ntpath         # OS Path Platform Independent
else:
    os_path_pi = os.path        # OS Path Platform Independent
BASE_DIR = os_path_pi.dirname(os_path_pi.dirname(__file__))
sys.path.append(BASE_DIR)       # TODO: DELETE when you install routercli package
from routercli import wrapper
from routercli import router
import subprocess

def run_wrapper():
    '''Shows how to run wrapper.py
    
    This will execute cmds on routers found from routercli.ini
    '''
    PYTHON_INTERPRETER = r'python2.6.exe'
    WRAPPER_SCRIPT = os_path_pi.abspath(wrapper.__file__).replace('.pyc', '.py')
    
    args = ('--dir "{0}"'.format(os_path_pi.dirname(__file__)))
    cmd = '"{0}" "{1}" {2}'.format(PYTHON_INTERPRETER, WRAPPER_SCRIPT, args)
    print 'Executing remote cmd: {0}'.format(cmd)
    out = subprocess.Popen(cmd, shell=True, cwd=r'C:\cygwin\bin')
    print 'Out for remote cmd: {0}'.format(out)
    
def router_login():
    '''Shows how to use router class
    '''
    r1 = router.Router('comcast-term-2')
    prompt = r1.login(username='cisco', password='cisco')
    print 'Prompt is:', prompt
    out = r1.exec_cmd('show ip int br')
    print 'Out of show ip int br is:', out
    
    
def test_ncs():
    r1 = router.Router('172.18.87.121')
    r1.timeout_session = 30
    import base64
    prompt = r1.login(username='ambarik', password=base64.b64decode('cm9jay5zb2xxpZ12DM0NQ=='))
    print 'Prompt is:', prompt
    out = r1.exec_cmd('show ipv4 int')
    print 'Out is:', repr(out)
    out = r1.exec_cmd('admin', prompt=(router.GENERIC_CISCO_PROMPT1.format(prompt="sysadmin-vm:\w+") + '\s'))
    print 'Out is:', out
    out = r1.exec_cmd('show log')
    print 'Out is:', repr(out)
    out = r1.exec_cmd('show clock')
    print 'Out is:', repr(out)
    #out = r1.exec_cmd('exit')
    #print 'Out is:', out
    r1.logout()
    
def main():
    print 'Running main...'
    #test_ncs()
    router_login()
    #run_wrapper()
    pass
    
if __name__ == '__main__':
    main()
    