'''
Created on Jul 10, 2013

@author: ambarik
'''

# hostname = 'test'
# no_of_devices = 2000
# cmds = ['show clock']
# 
# data = list()
# for i in range(0, no_of_devices):
#     hostname_data = '[{0}{1}]\n'.format(hostname, i)
#     cmds_data = '\n'.join(cmds)
#     data.append(hostname_data + cmds_data + '\n')
# 
# f = open('test.cfg', 'w')
# f.write('\n'.join(data))
# f.close()


#hostname = 'test'
#no_of_devices = 2000
#hosts = range(0, no_of_devices)

# cmds = ['CCE Python Program Start', "IOSXR.ccecode_BGP_NBR_NSR_DISABLED(current_router, msg='ROUTING-BGP-3-NBR_NSR_DISABLED')", 'CCE Python Program Stop']
# hosts = open(r'hosts.txt').readlines()
# data = list()
# for host in hosts:
#     hostname_data = '[{0}]\n'.format(host.strip())
#     cmds_data = '\n'.join(cmds)
#     #cmds_data = open(r'C:\Barik\Tasks\Task10\cmd.txt').read()
#     data.append(hostname_data + cmds_data + '\n')
# 
# f = open('test.cfg', 'w')
# f.write('\n'.join(data))
# f.close()


hosts = open(r'/tmp/routerlogs/hosts.txt').readlines()
data = list()
cmds = ['show controllers fabric ltrace crossbar location 0/RSP0/CPU0', 'show controllers fabric ltrace crossbar location 0/RSP1/CPU0']
 
data = list()
for host in hosts:
    hostname_data = '[{0}]\n'.format(host.strip())
    cmds_data = '\n'.join(cmds)
    data.append(hostname_data + cmds_data + '\n')
 
f = open('/tmp/routerlogs/test.cfg', 'w')
f.write('\n'.join(data))
f.close()
