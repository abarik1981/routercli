"""
L2 Protocol related
"""

import re

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS,\
    OS_NAME_ARRIS_CMTS

# from ipv4 import Ipv4
# from ipv6 import Ipv6

class Bgp(object):
    def __init__(self, router_obj):
        self.router = router_obj
        #self.ipv4 = IPv4(router_obj)
        #self.ipv6 = IPv6(router_obj)
        
    def get_neighbors(self, **kwargs):
        """Get neighbors
        
        by the following logic:
            based on 'show bgp summary' (CISCO)
            
        @return: If nbr found, return {'neighbor_ip':foo}, else None
        """
        if self.router.os_name == OS_NAME_IOS_XR:
            hdr = 'Neighbor        Spk    AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down  St/PfxRcd'
            o = self.router.exec_cmd('show bgp summary')
            l = [m.groupdict() for m in re.finditer('(?P<neighbor_ip>^[^\s]+?)\s+[^\s]+\s+(?P<asn>\d+)', o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        elif self.router.os_name == OS_NAME_IOS:
            hdr = 'Neighbor        V    AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/Pf'
            o = self.router.exec_cmd('show bgp summary')
            l = [m.groupdict() for m in re.finditer('(?P<neighbor_ip>^[^\s]+?)\s+(?P<version>\d+)\s+(?P<asn>\d+)', o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        elif self.router.os_name == OS_NAME_NX_OS:
            hdr = 'Neighbor        ASN    Flaps LastUpDn|LastRead|LastWrit St Port(L/R)  Notif(S/R)'
            o = self.router.exec_cmd('show bgp sessions')
            l = [m.groupdict() for m in re.finditer('(?P<neighbor_ip>^[^\s]+?)\s+(?P<asn>\d+)', o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        elif self.router.os_name == OS_NAME_ARRIS_CMTS:
            hdr = 'Neighbor                                 AS     Received    Sent        Time         Elapsed Time  Retry Count'
            o = self.router.exec_cmd('show ip bgp summary')
            l = [m.groupdict() for m in re.finditer('(?P<neighbor_ip>^[^\s]+?)\s+(?P<asn>\d+)', o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        else:
            msg = 'TODO: Implement get_neighbors: Router local_router: {0} is not Cisco.: Real prompt is: {1}. Please add device!!'.format(repr(self.router.hostname), repr(self.router._real_prompt))
            self.router.log(msg, 'WARNING')
            
    def parse_cisco_show_bgp(self, show_bgp):
        '''
        Parse only the best path: e.i show bgp having >
        
        @param show_bgp: Out of 'show bgp' as example:
        ! IOS
        Network            Next Hop            Metric     LocPrf     Weight Path
        *> 10.174.169.128/25
                            0.0.0.0                  0         32768 ?
        *> 10.174.221.0/25  0.0.0.0                  0         32768 ?
        
        ! NXOS/XR
        Network            Next Hop            Metric     LocPrf     Weight Path
        *> 10.122.136.0/24  0.0.0.0                  0         32768 ?
        *> 10.174.169.128/25
                            0.0.0.0                  0         32768 ?
        * i0.0.0.0/0          96.34.192.40             0        100          0 i
        
        ! Arris
          Network            Next Hop        Metric LocPrf Weight Path
        *> 0.0.0.0/0          69.144.189.76      250             0  i
        
        @return: {'10.0.0.1/32': {is_best:True, nxt_hop:'1.1.1.1'}}
        '''
        hash_by_prefix = dict()
        _list_prefixes = list()
        if self.router.os_name == OS_NAME_ARRIS_CMTS:
            hdr = 'Network\s+Next Hop\s+Metric\s+LocPrf\s+Weight'
        else:
            hdr = 'Network\s+Next Hop\s+Metric\s+LocPrf\s+Weight\s+Path'
        found_hdr = re.search(hdr, show_bgp)
        if found_hdr:
            o = show_bgp[found_hdr.start():]
            bgp_regexp_prefix = '(\d+\.){3}\d+/\d+'    # 1.1.1.1/11
            bgp_regexp_nxt_hop = '(\d+\.){3}\d+\s+'    # 1.1.1.1/11
            for line in o.splitlines():
                if 'Network' in line:
                    continue
                is_best = False
                prefix = None
                if '>' in line:
                    is_best = True
                found_prefix = re.search(bgp_regexp_prefix, line)
                if found_prefix:
                    prefix = found_prefix.group()
                    _list_prefixes.append(prefix)
                    hash_by_prefix[prefix] = dict()
                    hash_by_prefix[prefix]['is_best'] = is_best
                found_nxt_hop = re.search(bgp_regexp_nxt_hop, line.strip())
                if found_nxt_hop:
                    nxt_hop = found_nxt_hop.group().strip()
                    if prefix is None:
                        prefix = _list_prefixes[-1]  # Prefix from previous line (continous line of info)
                    if is_best:                  # Store only the best prefix path
                        hash_by_prefix[prefix]['nxt_hop'] = nxt_hop
                        hash_by_prefix[prefix]['is_best'] = is_best
                    if found_nxt_hop.start() == 0:  # If nxt_hop found in 2nd line
                        hash_by_prefix[prefix]['nxt_hop'] = nxt_hop
            return hash_by_prefix
        
    def parse_cisco_show_bgp_neighbors_advertised(self, show_bgp_neighbors_advertised):
        '''
        Parse only the best path: e.i show bgp having >
        
        @param show_bgp_neighbors_advertised: String format:
        ! IOS/NXOS
        same as parse_cisco_show_bgp()
        
        ! XR
        Network            Next Hop        From            AS Path
        0.0.0.0/0          96.34.192.40    Local           i
        10.64.144.0/24     96.34.198.53    96.34.196.4     ?
        
        ! Arris
          Network                  Next Hop                                Metric LocPrf Weight
        *>i10.196.112.0/22          96.34.213.224                                0             0
        '''
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS, OS_NAME_ARRIS_CMTS]:
            return self.parse_cisco_show_bgp(show_bgp_neighbors_advertised)
        elif self.router.os_name == OS_NAME_IOS_XR:
            hash_by_prefix = dict()
            hdr = 'Network\s+Next Hop\s+From\s+AS Path'
            found_hdr = re.search(hdr, show_bgp_neighbors_advertised)
            if found_hdr:
                o = show_bgp_neighbors_advertised[found_hdr.start():]
                regexp = '(?P<network>[^\s]+)\s+(?P<nxt_hop>[^\s]+)\s+(?P<from>[^\s]+)'
                for line in o.splitlines():
                    if 'Network' in line:
                        continue
                    found = re.search(regexp, line)
                    if found:
                        d = found.groupdict()
                        prefix = d.pop('network')
                        if d.get('from') == 'Local':
                            d['nxt_hop'] = '0.0.0.0'    # Standardizing with IOS/NXOS
                        d.pop('from')
                        hash_by_prefix[prefix] = d
                return hash_by_prefix
        
    def get_prefixes(self, **kwargs):
        """Get BGP prefixes
        
        by the following logic:
            based on 'show bgp' (CISCO)
            
        @return: dict, keyed by prefix {'1.1.1.1': {'nxt_hop':'x.x.x.x', 'metric': x}}
        """
        afi = kwargs.get('afi', 'ipv4')         # ipv4, ipv6
        safi = kwargs.get('safi', 'unicast')    # multicast, vpnv4, etc.
        if afi == 'ipv4' and safi == 'unicast':
            if self.router.os_name in [OS_NAME_IOS_XR, OS_NAME_IOS, OS_NAME_NX_OS]:
                o = self.router.exec_cmd('show bgp', timeout=3600)
                return self.parse_cisco_show_bgp(o)
            else:
                msg = 'TODO: Implement IPV6 get_prefixes: Router local_router: {0} is not Cisco.: Real prompt is: {1}. Please add device!!'.format(repr(self.router.hostname), repr(self.router._real_prompt))
                self.router.log(msg, 'WARNING')
        else:
            msg = 'TODO: Implement other afi/safi get_prefixes'
            self.router.log(msg, 'WARNING')
            
    def get_prefixes_advertised(self, **kwargs):
        """Get BGP prefixes advertised
        
        by the following logic:
            for each neighbor found in show bgp summary, run command:
                'show ip bgp neighbors {neighbor_ip} advertised-routes' (IOS/NXOS)
                'show bgp neighbors {neighbor_ip} advertised-routes' (IOSXR)
            
        @return: dict, keyed by neighbor_ip {'nbr_ip1': hash_by_prefix}
                 where hash_by_prefix is output of parse_cisco_show_bgp_neighbors_advertised()
                 else, None
        """
        afi = kwargs.get('afi', 'ipv4')         # ipv4, ipv6
        safi = kwargs.get('safi', 'unicast')    # multicast, vpnv4, etc.
        hash_by_neighbor_ip = dict()
        if afi == 'ipv4' and safi == 'unicast':
            if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS]:
                cmd = 'show ip bgp neighbors {neighbor_ip} advertised-routes'
            elif self.router.os_name in [OS_NAME_IOS_XR]:
                cmd = 'show bgp neighbors {neighbor_ip} advertised-routes'
            elif self.router.os_name in [OS_NAME_ARRIS_CMTS]:
                cmd = 'show ip bgp neighbor {neighbor_ip} advertised-routes'
            else:
                msg = 'TODO: Implement other os_names for get_prefixes_advertised'
                self.router.log(msg, 'WARNING')
                return
            
            nbrs = self.get_neighbors()
            for nbr in nbrs:
                neighbor_ip = nbr['neighbor_ip']
                o = self.router.exec_cmd(cmd.format(neighbor_ip=neighbor_ip), timeout=3600)
                if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS, OS_NAME_IOS_XR]:
                    hash_by_neighbor_ip[neighbor_ip] = self.parse_cisco_show_bgp_neighbors_advertised(o)
                elif self.router.os_name in [OS_NAME_ARRIS_CMTS]:
                    hash_by_neighbor_ip[neighbor_ip] = self.parse_cisco_show_bgp_neighbors_advertised(o)
            return hash_by_neighbor_ip
        else:
            msg = 'TODO: Implement other afi/safi get_prefixes_advertised'
            self.router.log(msg, 'WARNING')
            