'''
Everything related to VLAN
'''

from routercli import OS_NAME_IOS, OS_NAME_NX_OS, OS_NAME_IOS_XR
from routercli.MODEL_NAMES import *
import re


class Vlan(object):
    def __init__(self, router_obj):
        self.router = router_obj
        
    def ios_nxos_parse_vlan_ports(self, short_name_ports_sep_by_comma):
        '''
        @param: short_name_ports_sep_by_comma: e.g 'Gi1/2, Te2/3'
        @return ports with full name as list: e.g ['GigabitEthernet1/2', 'TenGigabitEthernet2/3']
        
        NOTE: Port-channel1 is seen on IOS and port-channel1 is seen on NXOS
        '''
        ports = str(short_name_ports_sep_by_comma).strip()
        ports = ports.split(', ')
        if '' in ports:
            ports.remove('')
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS, OS_NAME_IOS_XR]:
            for j, port in enumerate(ports):
                port_full_name = self.router.short_to_full_intf_name(port)
                ports[j] = port_full_name
        return ports
        
    
    def ios_nxos_show_vlan_brief_parse(self, show_vlan_brief):
        '''
        @return: vlan_data [
            {'num': 1, 'name': 'foo', 'ports': ['Ethernet1/1', 'GigabitEthernet7/2']},
            {'num': 2, 'name': 'bar', 'ports': ['Ethernet1/1', 'GigabitEthernet1/2']}
        ]
        '''
        lines = show_vlan_brief.splitlines()
        vlan_data = []
        show_vlan_patt1 = '(?P<num>^\d+)\s+(?P<name>[^\s]+?)\s+(?P<status>[^\s]+)(?P<ports>.*$)'
        show_vlan_patt2 = '^\s+(?P<extra_ports>.*$)'
        for line in lines:
            matched1 = re.search(show_vlan_patt1, line)
            if matched1:
                d = matched1.groupdict()
                d['num'] = int(d['num'])
                d['ports'] = self.ios_nxos_parse_vlan_ports(d['ports'])
                vlan_data.append(d)
            else:
                matched2 = re.search(show_vlan_patt2, line)
                if matched2 and (len(vlan_data) > 0):
                    ports = vlan_data[-1]['ports'][:]
                    d = matched2.groupdict()
                    extra_ports = self.ios_nxos_parse_vlan_ports(d['extra_ports'])
                    vlan_data[-1]['ports'] = ports + extra_ports
        return vlan_data
        
    def get_vlans(self):
        """Get all VLANs with output format as:
        
        @param local_intf: interface name for local router
        @return: vlan_data [
            {'num': 1, 'name': 'foo', 'ports': ['Ethernet1/1', 'GigabitEthernet7/2']},
            {'num': 2, 'name': 'bar', 'ports': ['Ethernet1/1', 'GigabitEthernet1/2']}
        ]
        
        Precondition:
        * local_router must be already loggedin
        * must be cisco devices
        """
        return_data = []
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS]:
            cmd = 'show vlan brief'
            if self.router.model_name in [MODEL_NAME_C1861,
                MODEL_NAME_C1841,
                MODEL_NAME_C181X,
                MODEL_NAME_C1805,
                MODEL_NAME_C1900,
                MODEL_NAME_C2600,
                MODEL_NAME_C2800,
                MODEL_NAME_C2801,
                MODEL_NAME_C3745]:
                cmd = 'show vlan-switch brief'
            o = self.router.exec_cmd(cmd)
            return_data = self.ios_nxos_show_vlan_brief_parse(o)
        else:
            self.router.log('Cannot get_vlans for os_name {0}'.format(self.router.os_name), 'EXCEPTION')
        return return_data
    

