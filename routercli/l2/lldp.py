'''
Everything related to mulicast LLDP
'''

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS

import re


class Lldp(object):
    def __init__(self, router_obj):
        self.router = router_obj
    