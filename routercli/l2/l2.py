"""
L2 Protocol related
"""

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS

from cdp import Cdp
from lldp import Lldp
from vlan import Vlan

class Layer2(object):
    def __init__(self, router_obj):
        self.router = router_obj
        self.cdp = Cdp(router_obj)
        self.lldp = Lldp(router_obj)
        self.vlan = Vlan(router_obj)
        
    def get_connected_nbr(self, local_intf):
        """Get connected neighbor
        
        This method is used for getting the remote device's hostname and
        remote interface
        by the following logic:
            based on 'show cdp neighbors $int detail' (CISCO)
            
        @param local_intf: interface name for local router
        @return: If nbr found, return {'hostname':foo, 'intf':bar}, else None
                 If Bundle-Ether, return .update({'bintfs': [list of local ports]})
        
        Precondition:
        * local_router must be already loggedin
        * cdp/lldp must be enabled on cisco devices
        
        Example:
        * R1-R2 is connected with R1's intf = Eth0/1 and
        R2's int = Eth0/2
        >>> local_router = r1 # (where r1 = Router('R1'), r1.login(...))
        >>> get_connected_nbr(r1, 'Eth0/1') # Returns...
        {'hostname':'R2', 'intf': 'Eth0/2'}
        >>> # runs R1#show cdp neighbors Eth0/1 detail
        """
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS]:
            return self.cdp.get_connected_nbr(local_intf)
        else:
            msg = 'TODO: Implement get_connected_nbr: Router local_router: {0} is not Cisco IOS/XR/NXOS.: Real prompt is: {1}. Please add device!!'.format(repr(self.router.hostname), repr(self.router._real_prompt))
            self.router.log(msg, 'WARNING')
            