'''
Everything related to mulicast CDP
'''

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS
import re


class Cdp(object):
    def __init__(self, router_obj):
        self.router = router_obj
        
    def get_neighbors(self):
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS]:
            o = self.router.exec_cmd('show cdp neighbors detail')
            return parse_show_cdp_neighbors_detail(o)
        else:
            self.router.exception('Implement get_connected_nbr for os_name {0}'.format(self.router.os_name))
            return []
    
    def get_connected_nbr(self, local_intf):
        """Get connected neighbor
        
        This method is used for getting the remote device's hostname and
        remote interface
        by the following logic:
            based on 'show cdp neighbors $int detail' (CISCO)
            
        @param local_intf: interface name for local router
        @return: If nbr found, return {'hostname':foo, 'intf':bar}, else None
                 If Bundle-Ether, return .update({'bintfs': [list of local ports]})
        
        Precondition:
        * local_router must be already loggedin
        * cdp must be enabled on cisco devices
        
        Example:
        * R1-R2 is connected with R1's intf = Eth0/1 and
        R2's int = Eth0/2
        >>> local_router = r1 # (where r1 = Router('R1'), r1.login(...))
        >>> get_connected_nbr(r1, 'Eth0/1') # Returns...
        {'hostname':'R2', 'intf': 'Eth0/2'}
        >>> # runs R1#show cdp neighbors Eth0/1 detail
        """
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS]:
            bintfs = list()     # Local interfaces associated with Bundle
            subintf_found = local_intf.rfind('.')
            if subintf_found != -1:
                local_intf_wo_vlan = local_intf[:subintf_found]     # Stripping sub-interface
                msg = 'Stripped VLAN from local_intf: {0}, and using local_intf_wo_vlan: {1} as intf'.format(local_intf, local_intf_wo_vlan)
                self.router.log(msg, 'WARNING')
                local_intf = local_intf_wo_vlan
            if 'Bundle-Ether' in local_intf and self.router.os_name == OS_NAME_IOS_XR:
                from routercli.operatingsystems.iosxr.iosxr import IOSXR
                bintfs = IOSXR.get_bundle_assoc_intfs(self.router, local_intf)
                self.router.log('Using 1st intf for cdp, from bundle: {0}, associated intfs: {1}'.format(local_intf, bintfs))
                o = self.router.exec_cmd('show cdp neighbors {0} detail'.format(bintfs[0]))
            elif self.router.os_name == OS_NAME_NX_OS:
                o = self.router.exec_cmd('show cdp neighbors interface {0} detail'.format(local_intf))
            else:
                o = self.router.exec_cmd('show cdp neighbors {0} detail'.format(local_intf))
            nbr = parse_show_cdp_neighbors_detail(o)
            
            if len(nbr) > 0:
                d = dict()
                d['hostname'] = nbr[0]['remote_hostname']
                d['intf'] = nbr[0]['remote_intf']
                if len(bintfs) > 0:
                    d['bintfs'] = bintfs
                return d
            else:
                msg = 'no cdp neighbor found for hostname: {0} on intf: {1}'.format(self.router.hostname, local_intf)
                self.router.log(msg, 'WARNING')
        else:
            self.router.exception('Implement get_connected_nbr for os_name {0}'.format(self.router.os_name))
            
            
def parse_show_cdp_neighbors_detail(o):
    '''
    return list of dict format:
    [{'remote_hostname': 'remote hostname',
      'remote_intf': 'full int name',
      'local_intf': 'full int name',
      'remote_platform': 'hw platform'}
    ]
    NOTE: remote_hostname might not actually be equal to configured hostname
    of remote as it might have dns name (e.g foo.x.com, instead of foo
    
    Tested with IOS, IOS-XR, NX-OS
    '''
    l = list()
    lines = o.splitlines()
    for line in lines:
        if 'Device ID:' in line:
            l.append(dict())
            l[-1]['remote_hostname'] = line.replace('Device ID:', '').strip()
        if 'Platform:' in line and len(l) > 0:
            l[-1]['remote_platform'] = line.split(',')[0].replace('Platform:', '').strip()
        if 'Interface:' in line and len(l) > 0:
            l[-1]['local_intf'] = line.split(',')[0].replace('Interface:', '').strip()
        if 'Port ID (outgoing port):' in line and len(l) > 0:
            s = line.split(',')
            if len(s) > 1:
                l[-1]['remote_intf'] = s[1].replace('Port ID (outgoing port):', '').strip()
            else:
                l[-1]['remote_intf'] = s[0].replace('Port ID (outgoing port):', '').strip()
    return l