"""
L2 Protocol related
"""

import re

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS, OS_NAME_ARRIS_CMTS

# from ipv4 import Ipv4
# from ipv6 import Ipv6

class Isis(object):
    def __init__(self, router_obj):
        self.router = router_obj
        #self.ipv4 = IPv4(router_obj)
        #self.ipv6 = IPv6(router_obj)
        
    def get_neighbors(self, **kwargs):
        """Get ISIS neighbors
        
        by the following logic:
            based on 'show isis neighbors' (CISCO)
            
        @return: If nbr found, return {'system_id':foo, 'intf':bar, 'snpa'
                    'hold_time', 'type', 'ietf_nsf'}, else None
        
        Precondition:
        * local_router must be already loggedin
        * isis must be enabled on cisco devices
        """
        if self.router.os_name in [OS_NAME_IOS_XR]:
            hdr = 'System Id      Interface        SNPA           State Holdtime Type IETF-NSF'
            total_nbr_cnt = 'Total neighbor count:'
            o = self.router.exec_cmd('show isis neighbors')
            total_cnt = o.find(total_nbr_cnt) + len(total_nbr_cnt)
            l = [m.groupdict() for m in re.finditer('(?P<system_id>^[^\s]+?)\s+(?P<intf>[^\s]+?)\s+(?P<snpa>[^\s]+?)\s+(?P<state>[^\s]+?)\s+(?P<holdtime>[^\s]+?)\s+(?P<type>[^\s]+?)\s+(?P<ietf_nsf>[^\s]+?)\s+?$', o[o.find(hdr)+len(hdr):total_cnt], re.MULTILINE)]
            for i in l:
                if 'BE' == i['intf'][:2]:
                    i['intf'] = 'Bundle-Ether' + i['intf'][2:]
                elif 'Te' == i['intf'][:2]:
                    i['intf'] = 'TenGigE' + i['intf'][2:]
                elif 'Gi' == i['intf'][:2]:
                    i['intf'] = 'GigabitEthernet' + i['intf'][2:]
            return l
        elif self.router.os_name in [OS_NAME_IOS]:
            hdr = 'System Id      Type Interface   IP Address      State Holdtime Circuit Id'
            o = self.router.exec_cmd('show isis neighbors')
            p = '(?P<system_id>^[^\s]+?)(\s+)?(?P<type>(L1|L2|L1L2|L2L1))\s+(?P<intf>[^\s]+?)\s+(?P<ip_addr>[^\s]+?)\s+(?P<state>[^\s]+?)\s+(?P<holdtime>[^\s]+).*?$'
            l = [m.groupdict() for m in re.finditer(p, o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            for i in l:
                if 'Vl' == i['intf'][:2]:
                    i['intf'] = 'VLAN' + i['intf'][2:]
                elif 'Te' == i['intf'][:2]:
                    i['intf'] = 'TenGigabitEthernet' + i['intf'][2:]
                elif 'Gi' == i['intf'][:2]:
                    i['intf'] = 'GigabitEthernet' + i['intf'][2:]
                elif 'Po' == i['intf'][:2]:
                    i['intf'] = 'Port-channel' + i['intf'][2:]
                elif 'Tu' == i['intf'][:2]:
                    i['intf'] = 'Tunnel' + i['intf'][2:]
                elif 'Se' == i['intf'][:2]:
                    i['intf'] = 'Serial' + i['intf'][2:]
                elif 'Fa' == i['intf'][:2]:
                    i['intf'] = 'FastEthernet' + i['intf'][2:]
            return l
        elif self.router.os_name in [OS_NAME_NX_OS]:
            hdr = 'System ID       SNPA            Level  State  Hold Time  Interface'
            o = self.router.exec_cmd('show isis adjacency')
            p = '(?P<system_id>^[^\s]+?)\s+(?P<snpa>[^\s]+?)\s+(?P<level>[^\s]+?)\s+(?P<state>[^\s]+?)\s+(?P<holdtime>[^\s]+?)\s+(?P<intf>[^\s]+).*?$'
            l = [m.groupdict() for m in re.finditer(p, o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        elif self.router.os_name in [OS_NAME_ARRIS_CMTS]:
            hdr = 'System ID      Interface                    SNPA           State Hold  Type Type Circuit Id           Protocol'
            o = self.router.exec_cmd('show isis neighbor')
            p = '(?P<system_id>^[^\s]+?)\s+(?P<intf>[a-zA-Z].*?\s+?.*?)\s+(?P<snpa>[^\s]+?)\s+(?P<state>[^\s]+?)\s+(?P<holdtime>[^\s]+?)\s+(?P<type>[^\s]+).*?$'
            l = [m.groupdict() for m in re.finditer(p, o[o.find(hdr)+len(hdr):], re.MULTILINE)]
            return l
        else:
            msg = 'TODO: Implement get_neighbors: Router local_router: {0} is not Cisco XR.: Real prompt is: {1}. Please add device!!'.format(repr(self.router.hostname), repr(self.router._real_prompt))
            self.router.log(msg, 'WARNING')
            