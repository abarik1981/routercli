from rib import Rib

class Unicast(object):
    def __init__(self, router_obj):
        self.router = router_obj
        self.rib = Rib(router_obj)