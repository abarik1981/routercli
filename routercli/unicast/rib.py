'''
Everything related to mulicast RIB
'''

ROUTE_CMD_IOS = 'show ip route'
ROUTE_CMD_IOS_XR = 'show route'

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS
import re


class Rib(object):
    def __init__(self, router_obj):
        self.router = router_obj
        
    def is_directly_connected(self, **kwargs):
        '''check if a route is directly connected
        @param route_prefix: prefix
        @param intf: Interface name
        
        If directly connected, then return True
        If not directly connected, then return False
        If route not found, then return None
        '''
        route_prefix = kwargs.get('route_prefix')
        intf = kwargs.get('intf')
        
        if self.router.os_name in [OS_NAME_IOS, OS_NAME_NX_OS]:
            cmd = ROUTE_CMD_IOS
        elif self.router.os_name == OS_NAME_IOS_XR:
            cmd = ROUTE_CMD_IOS_XR
        
        if route_prefix:
            if self.router.os_name in [OS_NAME_IOS, OS_NAME_IOS_XR]:
                patt_for_connected = 'directly connected'
                patt_for_not_connected = 'Known via'
                patt_for_not_found = 'Network not in table'
                
                cmd += ' {0}'.format(route_prefix)
                o = self.router.exec_cmd(cmd)
                if intf:
                    patt_for_connected += ', via {0}'.format(intf)
                if patt_for_connected in o:
                    return True
                elif patt_for_not_connected in o:
                    return False
                if patt_for_not_found in o:
                    return None
                
            elif self.router.os_name in [OS_NAME_NX_OS]:
                patt_for_connected = 'attached'
                patt_for_not_found = 'Route not found'
                
                cmd += ' {0}'.format(route_prefix)
                if intf:
                    cmd += ' interface {0}'.format(intf)
                o = self.router.exec_cmd(cmd)
                if patt_for_not_found in o:
                    return None
                
                if re.search('{0}.*?{1}'.format(route_prefix, patt_for_connected), o):
                    return True
                else:
                    return False
                
        else:
            raise NotImplementedError
            
#
# Functions
#
