'''
Everything related to mulicast RIB
'''

ROUTE_CMD_IOS = 'show ip mroute'
ROUTE_CMD_IOS_XR = 'show mrib route'

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS,\
    OS_NAME_EXTREMEXOS

import re


class Rib(object):
    def __init__(self, router_obj):
        self.router = router_obj
        
    def get_route(self, **kwargs):
        '''get mcast route in list(dict()) format
        @param gp_addr: multicast group address
        @param src_addr: source ip address for group
                         Default None (incase of PIM-SM)
                         
        If device os_name not found, return None
        '''
        gp_addr = kwargs.get('gp_addr')
        src_addr = kwargs.get('src_addr')
        
        if self.router.os_name == OS_NAME_IOS:
            cmd = ROUTE_CMD_IOS
            if src_addr:
                cmd += ' {0}'.format(src_addr)
            if gp_addr:
                cmd += ' {0}'.format(gp_addr)
            o = self.router.exec_cmd(cmd)
            return ios_show_ip_mroute_parse(o)
        
        elif self.router.os_name == OS_NAME_NX_OS:
            cmd = ROUTE_CMD_IOS
            if src_addr:
                cmd += ' {0}'.format(src_addr)
            if gp_addr:
                cmd += ' {0}'.format(gp_addr)
            o = self.router.exec_cmd(cmd)
            return nx_os_show_ip_mroute_parse(o)
        
        elif self.router.os_name == OS_NAME_IOS_XR:
            cmd = ROUTE_CMD_IOS_XR
            if src_addr:
                cmd += ' {0}'.format(src_addr)
            if gp_addr:
                cmd += ' {0}'.format(gp_addr)
            o = self.router.exec_cmd(cmd)
            return ios_xr_show_mrib_route_parse(o)
        
        elif self.router.os_name == OS_NAME_EXTREMEXOS:
            cmd = 'show pim cache detail'
            o = self.router.exec_cmd(cmd)
            return extremexos_show_pim_cache_detail_parse(o)
        
#
# Functions
#


def extremexos_show_pim_cache_detail_parse(o):
    '''
    parses output of show pim cache detail and returns a list of dict() output
    
    ref:
    http://extrcdn.extremenetworks.com/wp-content/uploads/2014/04/Multicast.pdf
    Sample output
    [0001] 239.1.1.159     172.21.11.211 (S)  vlan4067-video-blnmt-lvtmt Sparse
           Expires after 211 secs UpstNbr: 172.21.252.89
           RP: 172.21.255.1 via 172.21.252.89 in vlan4067-video-blnmt-lvtmt
           EgressIfList =  vlan4092-LocalVideo(0)(FW)(SM)(I) , vlan4099-XXX(0)(FW)(SM)(I)
           
    return list of dict output format:
    [{'gp_addr': '239.240.44.51',
    'src_addr': '172.16.173.150',
    'uptime': '7w0d',
    'rpf_nbr_addr': '96.34.48.131',
    'rp_addr': None,
    'expires': '00:03:20',
    'in_intf': {'name': 'Port-channel3'},
    'flags': 'sT', 
    'out_intfs': [{'state': 'Forward', 'uptime': '6d04h',
                 'name': 'TenGigabitEthernet9/3', 'expires': '00:03:20',
                 'mode': 'Sparse'}]}]
    '''
    # TODO: rpf_nbr_addr, uptime, expires, flags
    routing_table = []
    sg_patt = """
    # e.g: matches: [0001] 239.1.1.158     172.21.11.211 (S)  vlan4067-video-blnmt-lvtmt Sparse
    ^.*\[\d+\]\s+                              # Index
    ((?:\d+\.){3}\d+)\s+((?:\d+\.){3}\d+)      # (S G)
    \s+\(\w+\)\s+                              # (flag)
    ([^\s]+)\s+(\w+)                           # InVLAN Mode
    """
    egresslist_patt = 'EgressIfList\s+=\s+(.*)$'
    rp_patt = 'RP:\s+((?:\d+\.){3}\d+)\s+via'
    lines = o.splitlines()
    for i, line in enumerate(lines):
        matched_sg = re.search(sg_patt, line, re.VERBOSE)
        if matched_sg:
            gp_addr, src_addr, intf_name, mcast_mode = matched_sg.groups()
            r = {
                'gp_addr': gp_addr,
                'src_addr': src_addr,
                'in_intf': {'name': intf_name}
            }
            routing_table.append(r)
        matched_elist = re.search(egresslist_patt, line)
        if matched_elist:
            oil = matched_elist.group(1).split(',')
            for j, s in enumerate(oil):
                oil[j] = {'name': s[:s.find('(')].strip()}
            routing_table[-1]['out_intfs'] = oil
        matched_rp = re.search(rp_patt, line)
        if matched_rp:
            routing_table[-1]['rp_addr'] = matched_rp.group(1)
    return routing_table

def ios_show_ip_mroute_parse(show_ip_mroute):
    '''
    parses output of show ip mroute and returns a dict() output
    
    ref:
    http://www.cisco.com/c/en/us/td/docs/ios/12_2/switch/command/reference/fswtch_r/xrfscmd6.html
    
    return dict output format:
    {'gp_addr': '239.240.44.51',
    'src_addr': '172.16.173.150',
    'uptime': '7w0d',
    'rpf_nbr_addr': '96.34.48.131',
    'rp_addr': None,
    'expires': '00:03:20',
    'in_intf': {'name': 'Port-channel3'},
    'flags': 'sT', 
    'out_intfs': [{'state': 'Forward', 'uptime': '6d04h',
                 'name': 'TenGigabitEthernet9/3', 'expires': '00:03:20',
                 'mode': 'Sparse'}]}
    '''
    lines = show_ip_mroute.splitlines()
    routing_table = list()
    sg_patt = """
    \(((?:\d+\.){3}\d+|\*),\s((?:\d+\.){3}\d+)\),   # (S, G),
    \s(\S+),                                        # Uptime/Expires
    (?:\sRP\s((?:\d+\.){3}\d+),)?                   # RP Address
    \sflags:\s(\S+)$                                # flags
    """
    for i, line in enumerate(lines):
        matched = re.search(sg_patt, line, re.VERBOSE)
        if matched:
            d_per_sg = dict()
            d_per_sg['src_addr'] = matched.group(1)
            d_per_sg['gp_addr'] = matched.group(2)
            d_per_sg['uptime'] = matched.group(3).split('/')[0]
            d_per_sg['expires'] = matched.group(3).split('/')[1]
            d_per_sg['rp_addr'] = matched.group(4)
            d_per_sg['flags'] = matched.group(5)
            routing_table.append(d_per_sg)
            continue    # go nxt line if sg_patt found
        
        if len(routing_table) >= 1:
            last_added = routing_table[-1]
            if last_added.get('gp_addr'):
                patt_incoming = 'Incoming interface'
                if patt_incoming in line:
                    patt_in_int ="""
                    Incoming\sinterface:\s(\S+),          # Incoming interface: IntName,
                    (?:\sRPF\snbr\s((?:\d+\.){3}\d+))?    # RPF nbr 1.1.1.1
                    """
                    matched = re.search(patt_in_int, line, re.VERBOSE)
                    if matched:
                        last_added['in_intf'] = dict()
                        last_added['in_intf']['name'] = matched.group(1)
                        last_added['rpf_nbr_addr'] = matched.group(2)
                    continue    # go nxt line if patt_incoming found
                
                patt_out_int = 'Outgoing interface list'
                if patt_out_int in line:
                    last_added['out_intfs'] = list()
                    oil = last_added['out_intfs']
                    patt_oil = 'Outgoing interface list:(\sNull)?'
                    matched_oil = re.search(patt_oil, line)
                    if matched_oil:
                        if 'Null' in matched_oil.group():
                            oil = list()
                        else:
                            j = len(lines)
                            for m, n in enumerate(lines[i+1:j]):
                                # Parse through all interfaces until new line is seen
                                if n == '':
                                    j = i + m + 1
                                    break
                            patt_out_int = """
                            ^\s+(\S+),         # InterfacName
                            \s(\S+),           # State/Mode
                            \s(\S+).*?$        # Uptime/Expires
                            """
                            oil_lines = '\n'.join(lines[i+1:j])
                            l = re.findall(patt_out_int, oil_lines, re.MULTILINE|re.VERBOSE)
                            for k in l:
                                out_int_d = dict()
                                out_int_d['name'] = k[0]
                                out_int_d['state'] = k[1].split('/')[0]
                                out_int_d['mode'] = k[1].split('/')[1]
                                out_int_d['uptime'] = k[2].split('/')[0]
                                out_int_d['expires'] = k[2].split('/')[1]
                                oil.append(out_int_d)
                    continue    # go nxt line if patt_out_int found
    return routing_table
    
def ios_xr_show_mrib_route_parse(show_mrib_route):
    '''
    parses output of show mrib route (detail) and returns a dict() output
    
    returns dict of format:
    {'gp_addr': '239.255.255.250',
    'src_addr': '*',
    'uptime': '1d05h', 'rpf_nbr_addr': '172.31.0.181', 'flags': 'C',
    'in_intf': {'uptime': 'Up: 1d05h', 'flags': 'A', 'name': 'Decapstunnel0'},
    'out_intfs':
        [{'uptime': 'Up: 1d05h', 'flags': 'F NS', 'name': 'Bundle-Ether7'},
        {'uptime': 'Up: 06:57:43', 'flags': 'F NS', 'name': 'TenGigE0/4/0/6'}]}
    '''
    lines = show_mrib_route.splitlines()
    routing_table = list()
    sg_patt = """
    # matches 'show mrib route detail' too
    \(((?:\d+\.){3}\d+|\*),((?:\d+\.){3}\d+)(?:/\d+)?\).*?    # (S,G/Mask)XXX
    (?:\sRPF\snbr:\s((?:\d+\.){3}\d+))?            # Optional RPF Neighbor IP
    \sFlags:(?:\s(.*?))?(?:,)?$                    #  Flags: F G,
    """
    patt_in_int = patt_out_int = """
    ^\s+(.*?)               # InterfacName
    \sFlags:\s(.*?),        # flags
    \s(.*?)$                # Uptime/Expires
    """
    for i, line in enumerate(lines):
        matched = re.search(sg_patt, line, re.VERBOSE)
        if matched:
            d_per_sg = dict()
            d_per_sg['src_addr'] = matched.group(1)
            d_per_sg['gp_addr'] = matched.group(2)
            d_per_sg['rpf_nbr_addr'] = matched.group(3)
            d_per_sg['flags'] = matched.group(4)
            routing_table.append(d_per_sg)
            continue    # go nxt line if sg_patt found
        
        if len(routing_table) >= 1:
            last_added = routing_table[-1]
            if last_added.get('gp_addr'):
                patt_uptime = ' Up:'
                if patt_uptime in line[:6]:     # Looking at beginning of line
                    last_added['uptime'] = line.strip(patt_uptime).strip()
                    continue    # go nxt line if patt_uptime found
                
                patt_incoming = 'Incoming Interface List'
                if patt_incoming in line:
                    matched = re.search(patt_in_int, lines[i+1], re.VERBOSE)
                    if matched:
                        last_added['in_intf'] = dict()
                        last_added['in_intf']['name'] = matched.group(1)
                        last_added['in_intf']['flags'] = matched.group(2)
                        last_added['in_intf']['uptime'] = matched.group(3)
                    continue    # go nxt line if patt_incoming found
            
                patt_oil = 'Outgoing Interface List'
                if patt_oil in line:
                    last_added['out_intfs'] = list()
                    oil = last_added['out_intfs']
                    j = len(lines)
                    for m, n in enumerate(lines[i+1:j]):
                        # Parse through all interfaces until new line is seen
                        if n == '':
                            j = i + m + 1
                            break
                    oil_lines = '\n'.join(lines[i+1:j])
                    l = re.findall(patt_out_int, oil_lines, re.MULTILINE|re.VERBOSE)
                    for k in l:
                        out_int_d = dict()
                        # Strip slot - IntName (0/6/CPU0) -> IntName
                        out_int_d['name'] = re.sub('\s\(.*?\)', '', k[0])
                        out_int_d['flags'] = k[1]
                        out_int_d['uptime'] = k[2]
                        oil.append(out_int_d)
                    continue    # go nxt line if patt_oil found
                
    return routing_table
    
    

def nx_os_show_ip_mroute_parse(show_ip_mroute):
    '''
    parses output of show ip mroute and returns a dict() output
    
    return dict output format:
    {'gp_addr': '239.240.44.51',
    'src_addr': '172.16.173.150',
    'uptime': '7w0d',
    'rpf_nbr_addr': '96.34.48.131',
    'in_intf': {'name': 'Port-channel3'},
    'flags': 'sT', 
    'out_intfs': [{'state': 'Forward', 'uptime': '6d04h',
                 'name': 'TenGigabitEthernet9/3', 'expires': '00:03:20',
                 'mode': 'Sparse'}]}
    '''
    lines = show_ip_mroute.splitlines()
    routing_table = list()
    sg_patt = """
    \(((?:\d+\.){3}\d+|\*)(?:/\d+)?,\s((?:\d+\.){3}\d+)(?:/\d+)?\),   # (S, G),
    \suptime:\s(\S+),                                        # Uptime/Expires
    \s(.*?)$                                                 # flags
    """
    for i, line in enumerate(lines):
        matched = re.search(sg_patt, line, re.VERBOSE)
        if matched:
            d_per_sg = dict()
            d_per_sg['src_addr'] = matched.group(1)
            d_per_sg['gp_addr'] = matched.group(2)
            d_per_sg['uptime'] = matched.group(3)
            d_per_sg['flags'] = matched.group(4)
            routing_table.append(d_per_sg)
            continue    # go nxt line if sg_patt found
        
        if len(routing_table) >= 1:
            last_added = routing_table[-1]
            if last_added.get('gp_addr'):
                patt_incoming = 'Incoming interface'
                if patt_incoming in line:
                    patt_in_int ="""
                    Incoming\sinterface:\s(\S+),          # Incoming interface: IntName,
                    (?:\sRPF\snbr:\s((?:\d+\.){3}\d+))?    # RPF nbr: 1.1.1.1
                    """
                    matched = re.search(patt_in_int, line, re.VERBOSE)
                    if matched:
                        last_added['in_intf'] = dict()
                        last_added['in_intf']['name'] = matched.group(1)
                        last_added['rpf_nbr_addr'] = matched.group(2)
                    continue    # go nxt line if patt_incoming found
                
                patt_out_int = 'Outgoing interface list'
                if patt_out_int in line:
                    last_added['out_intfs'] = list()
                    oil = last_added['out_intfs']
                    patt_oil = 'Outgoing interface list:\s\(count:\s(\d+)\)'
                    matched_oil = re.search(patt_oil, line)
                    if matched_oil:
                        if matched_oil.group(1) == '0':
                            oil = list()
                        else:
                            j = len(lines)
                            for m, n in enumerate(lines[i+1:j]):
                                # Parse through all interfaces until new line is seen
                                if n == '':
                                    j = i + m + 1
                                    break
                            patt_out_int = """
                            ^\s+(\S+),              # InterfacName
                            \suptime:\s(\S+),       # Uptime
                            \s(.*?)$                # flags
                            """
                            oil_lines = '\n'.join(lines[i+1:j])
                            l = re.findall(patt_out_int, oil_lines, re.MULTILINE|re.VERBOSE)
                            for k in l:
                                out_int_d = dict()
                                out_int_d['name'] = k[0]
                                out_int_d['uptime'] = k[1]
                                out_int_d['flags'] = k[2]
                                oil.append(out_int_d)
                    continue    # go nxt line if patt_out_int found
    return routing_table
    
    
