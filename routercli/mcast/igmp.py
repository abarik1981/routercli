'''
Everything related to mulicast RIB
'''

ROUTE_CMD_IOS = 'show ip igmp groups'
ROUTE_CMD_IOS_XR = 'show igmp groups'

import re

from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS,\
    OS_NAME_EXTREMEXOS


class Igmp(object):
    def __init__(self, router_obj):
        self.router = router_obj
        
    def ios_show_ip_igmp_snooping_explicit(self, show_ip_igmp_snooping_explicit):
        '''
        @return snoop_out_list: [{'vlan_num': 100, 'intf_name': '', 'src_addr': '', 'gp_addr': ''}]
        '''
        # 172.19.132.13/232.95.246.12     Vl151:Gi7/11    172.19.141.61   INCLUDE
        snoop_patt = '^(?P<src_addr>[^\s]+?)\/(?P<gp_addr>(\d+\.){3}\d+)\s+Vl(?P<vlan_num>\d+)\:(?P<intf_name>[^\s]+?)\s+.*?$'
        snoop_out_list = [m.groupdict() for m in re.finditer(snoop_patt, show_ip_igmp_snooping_explicit, re.MULTILINE)]
        for i in snoop_out_list:
            if 'vlan_num' in i:
                i['vlan_num'] = int(i['vlan_num'])
            if 'intf_name' in i:
                i['intf_name'] = self.router.short_to_full_intf_name(i['intf_name'])
            if 'src_addr' in i:
                if i['src_addr'] == '*':
                    i['src_addr'] = '0.0.0.0'
        return snoop_out_list
        
    
    def iosxr_show_igmp_snooping_group(self, show_igmp_snooping_group):
        '''
        @return snoop_out_list: [{'vlan_num': 100, 'intf_name': '', 'src_addr': '', 'gp_addr': ''}]
        '''
        # 239.132.32.14   V3  IN 172.19.132.59   IN Te0/4/0/16                  93    D
        lines = show_igmp_snooping_group.splitlines()
        last_bg_bd_name = ''
        snoop_out_list = []
        snoop_patt1 = '^\s+Bridge Domain\s+(?P<bg_bd_name>[^\s]+)$'
        snoop_patt2 = '^(?P<gp_addr>(\d+\.){3}\d+).*?(?P<src_addr>(\d+\.){3}\d+)\s+\w+\s+(?P<intf_name>[^\s]+?)\s+.*?$'
        for line in lines:
            matched1 = re.search(snoop_patt1, line)
            if matched1:
                d = matched1.groupdict()
                last_bg_bd_name = d['bg_bd_name']
            else:
                matched2 = re.search(snoop_patt2, line)
                if matched2 and last_bg_bd_name:
                    d = matched2.groupdict()
                    d['bg_bd_name'] = last_bg_bd_name
                    snoop_out_list.append(d)
                    
        for i in snoop_out_list:
            if 'intf_name' in i:
                i['intf_name'] = self.router.short_to_full_intf_name(i['intf_name'])
            if 'src_addr' in i:
                if i['src_addr'] == '*':
                    i['src_addr'] = '0.0.0.0'
        return snoop_out_list
        
    
    def nxos_show_ip_igmp_snooping_explicit(self, show_ip_igmp_snooping_explicit):
        '''
        @return snoop_out_list: [{'vlan_num': 100, 'intf_name': '', 'src_addr': '', 'gp_addr': ''}]
        '''
        lines = show_ip_igmp_snooping_explicit.splitlines()
        snoop_out_list = []
        patt1 = '(?P<vlan_num>^\d+)\s+(?P<src_addr>[^\s]+?)\/(?P<gp_addr>(\d+\.){3}\d+)'
        patt2 = '^\s+(?P<intf_name>[^\s]+?)\s+'
        for line in lines:
            matched1 = re.search(patt1, line)
            if matched1:
                d = matched1.groupdict()
                snoop_out_list.append(d)
            else:
                matched2 = re.search(patt2, line)
                if matched2 and (len(snoop_out_list) > 0):
                    fd = snoop_out_list[-1]
                    d = matched2.groupdict()
                    fd.update(d)
        for i in snoop_out_list:
            if 'vlan_num' in i:
                i['vlan_num'] = int(i['vlan_num'])
            if 'intf_name' in i:
                i['intf_name'] = self.router.short_to_full_intf_name(i['intf_name'])
            if 'src_addr' in i:
                if i['src_addr'] == '*':
                    i['src_addr'] = '0.0.0.0'
        return snoop_out_list
        
    def get_groups(self, **kwargs):
        '''
        @return: list of dicts, where dict format: {gp_addr, intf_name, up_time, expires, last_reporter}
        '''
        result = []
        if self.router.os_name == OS_NAME_IOS:
            cmd = ROUTE_CMD_IOS
            o = self.router.exec_cmd(cmd)
            result = [m.groupdict() for m in re.finditer('^(?P<gp_addr>(\d+\.){3}\d+)\s+(?P<intf_name>[^\s]+?)\s+', o, re.MULTILINE)]
        elif self.router.os_name == OS_NAME_IOS_XR:
            cmd = ROUTE_CMD_IOS_XR
            o = self.router.exec_cmd(cmd)
            result = [m.groupdict() for m in re.finditer('^(?P<gp_addr>(\d+\.){3}\d+)\s+(?P<intf_name>[^\s]+?)\s+', o, re.MULTILINE)]
        elif self.router.os_name == OS_NAME_NX_OS:
            cmd = ROUTE_CMD_IOS
            o = self.router.exec_cmd(cmd)
            lines = o.splitlines()
            last_gp = None
            for line in lines:
                matched_gp = re.search('^(?P<gp_addr>(\d+\.){3}\d+)(\s+\w+\s+(?P<intf_name>[^\s]+?)\s+)?', line)
                matched_src = re.search('^\s+(?P<src_addr>(\d+\.){3}\d+)\s+\w+\s+(?P<intf_name>[^\s]+?)\s+', line)
                if matched_gp:
                    m = matched_gp.groupdict()
                    last_gp = m['gp_addr']
                    if m['intf_name']:
                        result.append(m)
                if matched_src:
                    m = matched_src.groupdict()
                    if last_gp:
                        m['gp_addr'] = last_gp
                    result.append(m)
        elif self.router.os_name == OS_NAME_EXTREMEXOS:
            '''
            Sample output
            Group Address     Ver Vlan            Port     Age
            224.0.0.0         2   grooming-1      1:17     54
            '''
            cmd = 'show igmp group'
            o = self.router.exec_cmd(cmd)
            result = [m.groupdict() for m in re.finditer('^.*?(?P<gp_addr>(\d+\.){3}\d+)\s+(?P<igmp_version>\d+)\s+(?P<vlan_name>[^\s]+?)\s+(?P<intf_name>[^\s]+?)\s+', o, re.MULTILINE)]
                
        return result
    
    def get_igmp_snooping_explicit_tracking(self):
        '''
        @return: list of dicts, where dict format: {gp_addr, intf_name, up_time, expires, last_reporter}
        '''
        result = []
        if self.router.os_name == OS_NAME_IOS:
            o = self.router.exec_cmd('show ip igmp snooping explicit-tracking')
            result = self.ios_show_ip_igmp_snooping_explicit(o)
        elif self.router.os_name == OS_NAME_NX_OS:
            o = self.router.exec_cmd('show ip igmp snooping explicit-tracking')
            result = self.nxos_show_ip_igmp_snooping_explicit(o)
        elif self.router.os_name == OS_NAME_IOS_XR:
            o = self.router.exec_cmd('show igmp snooping group')
            result = self.iosxr_show_igmp_snooping_group(o)
        return result
    
    def is_group_present(self, **kwargs):
        '''check if a group is present in IGMP entry
        @param gp_addr: Group address
        @param intf: Interface name
        
        If found, then return True
        If not found, then return False
        '''
        gp_addr = kwargs.get('gp_addr')
        intf = kwargs.get('intf')
        
        if self.router.os_name == OS_NAME_IOS:
            cmd = ROUTE_CMD_IOS
            if intf:
                cmd += ' {0}'.format(intf)
            if gp_addr:
                cmd += ' {0}'.format(gp_addr)
            o = self.router.exec_cmd(cmd)
            if gp_addr in o:
                return True
            else:
                return False
        elif self.router.os_name == OS_NAME_IOS_XR:
            cmd = ROUTE_CMD_IOS_XR
            if gp_addr:
                cmd += ' {0}'.format(gp_addr)
            if intf:
                cmd += ' {0}'.format(intf)
            o = self.router.exec_cmd(cmd)
            if gp_addr in o:
                return True
            else:
                return False
        elif self.router.os_name == OS_NAME_NX_OS:
            cmd = ROUTE_CMD_IOS
            if gp_addr:     # Gp 1st
                cmd += ' {0}'.format(gp_addr)
            if intf:        # Intf 2nd
                cmd += ' {0}'.format(intf)
            o = self.router.exec_cmd(cmd)
            if re.search('\s\d\d:\d\d:\d\d\s', o):  # Match Expires
                return True
            else:
                return False
            
            
#
# Functions
#

