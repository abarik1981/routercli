from rib import Rib
from routercli.mcast.igmp import Igmp

class Mcast(object):
    def __init__(self, router_obj):
        self.router = router_obj
        self.rib = Rib(router_obj)
        self.igmp = Igmp(router_obj)