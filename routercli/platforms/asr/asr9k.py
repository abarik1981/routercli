'''
Created on Mar 22, 2013

@author: ambarik
'''

import re, os, sys
import time

from routercli.operatingsystems.iosxr.iosxr import IOSXR

# Import util packages
from routercli.utils.utils import get_onlydigits_in_string


class ASR9K(IOSXR):
                
    def routing_table_size_maxed(self, **kwargs):
        '''
        PLATFORM-NP-3-OOR
        https://supportforums.cisco.com/docs/DOC-25489#Monitoring_L3_Scale
        
        Report if size of table is too big
        '''
        report = False
        reports = list()
        msg = kwargs.get('msg')
        if 'mode big' not in self.exec_cmd('show run | include big'):
            self.log('mode big is not configured')
            #reports.append(True)
            
        cef_resources = self.exec_cmd('show cef resource detail  | i "YELLOW|RED"')
        if 'YELLOW' in cef_resources or 'RED' in cef_resources:
            self.exec_cmd('show route vrf all sum')
            reports.append(True)
        
        reports.append(self.syslog_present(msg))
        if True in reports:
            report = True
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def verify_nv_sat_sw(self, **kwargs):
        '''
        Report if "show nv satellite status" doesn't show "Compatible (latest version)"
        '''
        report = False
        msg = kwargs.get('msg')
        list_of_nv_sate_out = self.exec_cmd('show nv satellite status brief')
        print repr(list_of_nv_sate_out)
        print list_of_nv_sate_out
        list_of_nv_sat = re.findall('^(\d+)\s+', list_of_nv_sate_out, re.MULTILINE)
        self.log('list_of_nv_sat: {0}'.format(list_of_nv_sat))
        bad_nv_sats = list()
        for nv_sat in list_of_nv_sat:
            nv_sat_info = self.exec_cmd('show nv satellite status satellite {0}'.format(nv_sat))
            if "Compatible (latest version)" not in nv_sat_info:
                bad_nv_sats.append(nv_sat)
                report = True
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        self.rcli_obj.configout.set(self.hostname, 'bad_nv_sats', str(bad_nv_sats))
        return report
    
    def CARD_INVALID(self, **kwargs):
        '''
        Report if card with PLATFORM-SHELFMGR-6-NODE_STATE_CHANGE message not seen in admin show platform
        
        %PLATFORM-SHELFMGR-6-NODE_STATE_CHANGE : 0/0/CPU0 UNKNOWN state:BRINGDOWN
        '''
        report = False
        msg = kwargs.get('msg')
        already_checked = self.rcli_obj.configout.get(self.hostname, 'CARD_INVALID')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = self.check_platform()
            logs = self.exec_cmd('show logging | include PLATFORM-SHELFMGR-6-NODE_STATE_CHANGE')
            check_modules = list(set(re.findall('CHANGE\s+:\s+(\w+/\w+/\w+)', logs, re.MULTILINE)))
            cards_invalid = list()
            
            if len(check_modules) >= 1:
                admin_show_platform = str()
                for cmd, out in self.history:
                    if cmd == 'admin show platform':
                        admin_show_platform = out
                        break
                    
                for module in check_modules:
                    if module not in admin_show_platform:
                        cards_invalid.append(module)
                        break
                            
            self.rcli_obj.configout.set(self.hostname, 'CARD_INVALID', report)
            if len(cards_invalid) >= 1 and msg is not None:
                report = True
                cards_invalid_info = 'Cards not seen in "admin show platform" cmd - {0}. '.format('; '.join(cards_invalid))
                self.rcli_obj.configout.set(self.hostname, '{0}_cards_invalid'.format(msg), cards_invalid_info)
            
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
        
    def card_reset(self, **kwargs):
        '''
        '''
        msg = kwargs.get('msg')
        report = self.check_platform(msg=msg)
        if report is True:
            return report
        else:
            return self.card_reboot_syslog(msg=msg)
    
    def PLATFORM_SHELFMGR_NODE_KERNEL_DUMP_TIMEOUT(self, msg):
        '''
        Report if card is resetting
        '''
        show_log = self.exec_cmd('show logging | include {0}'.format(msg))
        locations = list(set(re.findall('Reset node (\w+/\w+/\w+)', show_log, re.MULTILINE)))
        report = self.check_platform(msg=msg)
        if report is True:
            return report
        else:
            return self.card_reboot_syslog(msg=msg, locations=locations)
    
    def FCA_SFP_LNK_FAULT(self, msg):
        '''
        Msg:
        LC/0/4/CPU0:Mar 27 18:13:45.564 UTC: pfm_node_lc[283]: %PLATFORM-FCA-2-SFP_LNK_FAULT_ERROR_12 : Clear|fca_server[102404]|Port Interface Controller(0x1007000)|Port controller driver detected a TX fault on the SFP port 12
        
        Report if port is down
        '''
        report = False
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        l = list(set(re.findall('^\w+/(\w+/\w+/\w+):.*port (\d+)', out, re.MULTILINE)))
        list_of_ints = list()
        for t in l:
            module, port = t
            port = str(int(port))
            cpu_part = module[module.rfind('/') + 1:]
            cpu_no = get_onlydigits_in_string(cpu_part)
            loc = module.replace(cpu_part, cpu_no)
            interface = '{0}/{1}'.format(loc, port)
            list_of_ints.append((module, interface, port))
        
        # Will return something is link is down really
        cmd = 'show ip interfac brief | include {interface} | include Down | exclude Shutdown'
        list_of_ints = list(set(list_of_ints))
        if len(list_of_ints) != 0:
            ints_down = list()
            for module, interface, port in list_of_ints:
                self.exec_cmd('show controllers Phy port-status location {location} | include {port}'.format(location=module, port=port))
                self.exec_cmd('show controllers PortCtrl port-status location {location}'.format(location=module))
                out = self.exec_cmd(cmd.format(interface=interface))
                if 'Down' in out and 'Shutdown' not in out:
                    interface = re.search('(\w+{0})'.format(interface), out).group(1)
                    report = True
                    self.log('Int {0} is down due to msg: {1}'.format(interface, msg))
                    ints_down.append(interface)
                    self.exec_cmd('show run interface {0}'.format(interface))
            ints_down = ' Following interfaces are down: ' + ','.join(ints_down)
            self.rcli_obj.configout.set(self.hostname, '{msg}_ints_down'.format(msg=msg), ints_down)
        
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_ints_down(self, msg):
        # PLATFORM-XFP-2-DEV_XFP_COMPLIANCE_ERROR|PLATFORM-SFP-2-DEV_SFP_COMPLIANCE_ERROR|PLATFORM-SFP-2-DEV_SFP_PID_NOT_SUPPORTED
        # Report if PID missing
        # LC/0/0/CPU0:Nov 26 09:36:38.301 UTC: pfm_node_lc[228]: %PLATFORM-SFP-2-DEV_SFP_COMPLIANCE_ERROR : Clear|ether_ctrl_lc_ge[159861]|SFP(0x1026004)|SFP Module for port 04 is not Cisco compliant
        report = False
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        l = list(set(re.findall('^\w+/(\w+/\w+/\w+):.*port (\d+)', out, re.MULTILINE)))
        list_of_ints = list()
        for t in l:
            module, port = t
            interface = '{0}/{1}'.format(module.replace('CPU', ''), int(port))
            list_of_ints.append(interface)
        
        interfaces_info = self.get_interfaces_info(list_of_interfaces=list_of_ints)
        down_interfaces = list()
        for interface_name in interfaces_info:
            if interfaces_info[interface_name]['Status'] == 'down':
                if interfaces_info[interface_name]['ipv4 address'] != 'unassigned' or interfaces_info[interface_name]['ipv6 address'] != 'unassigned':
                    down_interfaces.append(interface_name)
                    report = True
        
        if len(down_interfaces) > 0:
            self.rcli_obj.configout.set(self.hostname, '{msg}_down_interfaces'.format(msg=msg), down_interfaces)
        
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_platform(self, **kwargs):
        # Report if not IOS XR
        # Node        Type              State           Config State
        msg = kwargs.get('msg')
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_platform')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            rcli_not_ios_xr_state = str()
            good_platform_states = 'IOS XR RUN|UNPOWERED|READY|OK'
            platform_regex = '(?P<node>\w+/\w+/.*?)\s{2,}(?P<node_type>.*?)\s{2,}(?P<state>.*?)\s{2,}(?P<config_state>.*?)$'
            out = self.exec_cmd('admin show platform')
            all_platform_states = list(set(re.findall(platform_regex, out, re.MULTILINE)))
            if all_platform_states == []:
                raise Exception("Invalid platform_regex. Should have matched something")
            for node, node_type, state, config_state in all_platform_states:
                if re.search(good_platform_states, state) is None:
                    report = True
                    rcli_not_ios_xr_state += '. Node: {0} in state: {1}'.format(node, state)
            
            if rcli_not_ios_xr_state != str():
                self.rcli_obj.configout.set(self.hostname, 'rcli_not_ios_xr_state', str(rcli_not_ios_xr_state))
            self.rcli_obj.configout.set(self.hostname, 'check_platform', report)
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def PUNT_FABRIC_DATA_PATH_FAILED(self, msg, **kwargs):
        """
        Message: PLATFORM-DIAGS-3-PUNT_FABRIC_DATA_PATH_FAILED
        Troubleshooting Steps:
        1. Run the command "show spp sid stats location $LC_LOCATION | i DIAG" at interval of 61 sec
        1.a. If DIAG counter hasn't incremented, there is a possible problem.
        2. Run the command "admin show diagnostic result location $RSP_LOCATION test 8 detail"
        2.a. If output has DIAG_FAILURE and Total failure count != 0, there is a possible problem. 
        3. If any output for the following commands shows down, there is a possible problem.
        "show controllers fabric crossbar link-status instance 0 location $$LC_LOCATION"
        "show controllers fabric fia link-status location $RSP_N_LC_LOCATION"
        "show controllers fabric fia bridge sync-status location $RSP_N_LC_LOCATION"
        "show controller fabric fia bridge ddr-status location $RSP_N_LC_LOCATION"
        4. Run the command "show controllers fabric crossbar statistics instance 0 location {location} | i Internal Error Count"
        4.a. If Error Count is incrementing, there is a possible problem.
        
        If there is a problem, open a TAC Case for further troubleshooting.
        """
        
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_platform')
        if already_checked is not None:
            report = eval(already_checked)
            if report is True:
                self.rcli_obj.configout.set(self.hostname, 'rcli_reason_PUNT_FABRIC_DATA_PATH_FAILED', '. admin show platform showed that a module is not in IOS-XR RUN State')
                return report
        
        report = False
        diff_time = 61
        crossbar_cmd_times = kwargs.get("crossbar_cmd_times", 5)
        spp_sid_diff_time = kwargs.get("spp_sid_diff_time", diff_time)
        out = self.exec_cmd('show logging | include {0}'.format(msg))
        locations = list(set(re.findall('(\d+/\w+/\w+\d+)', out)))   # e.g: 0/14/CPU0, 0/RSP1/CPU0
        rsp_locations = [loc for loc in locations if 'RSP' in loc]
        #lc_locations = list(set(locations) - set(rsp_locations))
        reasons = list()
        for rsp_location in rsp_locations:
            show_diag_result_8 = "admin show diagnostic result location {location} test 8 detail".format(location=rsp_location)
            out_show_diag_result_8 = self.exec_cmd(show_diag_result_8)
            total_fail_cnt = int(re.findall('Total failure count.*(\d+)', out_show_diag_result_8)[0])
            if 'DIAG_FAILURE' in out_show_diag_result_8 and total_fail_cnt != 0:
                report = True
                reasons.append('DIAG_FAILURE found on RSP: {0} with Failure Count of: {1}'.format(rsp_location, total_fail_cnt))
                
            crossbar_link_status_cmd = "show controllers fabric crossbar link-status instance 0 location {location}".format(location=rsp_location)
            crossbar_stats_cmd = "show controllers fabric crossbar statistics instance 0 location {location} | i Internal Error Count".format(location=rsp_location)
            for i in range(crossbar_cmd_times):
                crossbar_link_status_out = self.exec_cmd(crossbar_link_status_cmd)
                crossbar_stats_err1 = re.findall("Internal Error Count.* (\d+)", self.exec_cmd(crossbar_stats_cmd))
                if "Down" in crossbar_link_status_out:
                    report = True
                    reasons.append('fabric crossbar link-status Down found on RSP: {0}'.format(rsp_location))
                time.sleep(0.5)
                crossbar_stats_err2 = re.findall("Internal Error Count.* (\d+)", self.exec_cmd(crossbar_stats_cmd))
                if len(crossbar_stats_err1) >= 1 and len(crossbar_stats_err2) >= 1:
                    if int(crossbar_stats_err2[0]) > int(crossbar_stats_err1[0]):
                        report = True
                        reasons.append('fabric crossbar Internal Error Count increased on RSP: {0}'.format(rsp_location))
                        break
        
        show_fabric_fia_link_status_cmds = ['show controllers fabric fia link-status location {location}',
                                            'show controllers fabric fia bridge sync-status location {location}',
                                            'show controller fabric fia bridge ddr-status location {location}']
        for location in locations:
            spp_sid_cmd = "show spp sid stats location {location} | i DIAG".format(location=location)
            try:
                spp_sid_cnt1 = re.findall("DIAG.*(\d+)", self.exec_cmd(spp_sid_cmd))[0]
            except IndexError:
                continue
            time.sleep(spp_sid_diff_time)
            try:
                spp_sid_cnt2 = re.findall("DIAG.*(\d+)", self.exec_cmd(spp_sid_cmd))[0]
            except IndexError:
                continue
            if int(spp_sid_cnt2) == int(spp_sid_cnt1):
                report = True
                reasons.append('NP not forwarding at Location: {0}'.format(location))
                
            for cmd in show_fabric_fia_link_status_cmds:
                show_fabric_fia_link_status_out = self.exec_cmd(cmd.format(location=location))
                if "Down" in show_fabric_fia_link_status_out:
                    report = True
                    reasons.append('Fabric fia status down for location: {0} for command: {1}'.format(location, cmd))
                        
        self.rcli_obj.configout.set(self.hostname, 'rcli_reason_PUNT_FABRIC_DATA_PATH_FAILED', str(', '.join(reasons)))
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def fabric_fia_idrops(self, msg, **kwargs):
        report = False
        diff_time = kwargs.get("diff_time", 60)
        fia_drop_cmd = "show controllers fabric fia stats location {loc} | i Ingress drop"
        fia_idrop_diffs = list()
        out = self.exec_cmd('show logging | include {0}'.format(msg))
        locations = list(set(re.findall('\w+/(\w+/\w+/\w+):', out)))   # out=LC/0/14/CPU0:, get 0/14/CPU0
        for location in locations:
            fia_idrop1 = re.findall("Ingress drop.*(\d+)", self.exec_cmd(fia_drop_cmd.format(loc=location)))
            time.sleep(diff_time)
            fia_idrop2 = re.findall("Ingress drop.*(\d+)", self.exec_cmd(fia_drop_cmd.format(loc=location)))
            fia_idrop_diff = set(fia_idrop2) - set(fia_idrop1)
            fia_idrop_diffs.append(fia_idrop_diff)
        
        for fia_idrop_diff in fia_idrop_diffs:
            if len(fia_idrop_diff) >= 1:
                report = True
                
        self.rcli_obj.configout.set(self.hostname, 'fabric_fia_idrops', str(fia_idrop_diffs))
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def fabric_fia(self, msg):
        # Just capture outputs
        # For ASR Fabric FIA
        report = self.check_platform()
        out = self.exec_cmd('show logging | include {0}'.format(msg))
        fia_errs_locs = list(set(re.findall('\w+/(\w+/\w+/\w+):', out)))   # out=LC/0/14/CPU0:, get 0/14/CPU0
        cmds = ['show controllers np summary all location {location}',
                'show controllers np ports all location {location}',
                'show controller np counters all location {location}',
                'show controllers fabric fia link-status location {location}',
                'show controllers fabric fia bridge sync-status location {location}',
                'sh controllers fabric fia bridge stats loc {location}',
                'show controllers fabric fia errors ingress location {location}',
                'show drop location {location}',
                'show controllers fabric fia errors egress location {location}',        # Errpr should be 0
                'show controllers fabric fia drops ingress location {location}',
                'show controllers fabric fia stats location {location}',
                'show controllers fabric fia drops egress location {location}']
        if len(fia_errs_locs) != 0:
            for location in fia_errs_locs:
                for cmd in cmds:
                    self.exec_cmd(cmd.format(location=location))
            self.rcli_obj.configout.set(self.hostname, '{0}_fia_errs_locs'.format(msg), str(fia_errs_locs))
        
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def rcli_code_check_power_supply(self, **kwargs):
        '''
        %PLATFORM-PWR_MGMT-2-MODULE_FAILURE : Power-module 0/PM3/SP failure condition raised : Module fan failure observed 
        0/PM3/*
           host    PM    2100        Ok

        '''
        #TODO: Add code to account for specific module
        #TODO: Also, PM 0 Unpowered is fine
        # Report if power is not Ok
        msg = kwargs.get('msg')
        
        already_checked = self.rcli_obj.configout.get(self.hostname, 'rcli_code_check_power_supply')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            power_modules_off = list()
            report = False
            out = self.exec_cmd('admin show environment power-supply')
            for line in out.split('\n'):
                if re.search('PM\s+\d+\s+', line):
                    if re.search('PM\s+\d+\s+Ok', line) is None:
                        self.log('Power Module not Ok State')
                        report = True
                        power_modules_off.append(line)
            self.rcli_obj.configout.set(self.hostname, 'power_modules_off', power_modules_off)
            self.rcli_obj.configout.set(self.hostname, 'rcli_code_check_power_supply', report)
        
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    