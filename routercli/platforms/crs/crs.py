'''
Created on Mar 22, 2013

@author: ambarik

#http://www.cisco.com/en/US/docs/ios_xr_sw/iosxr_r4.0.1/error/messages/em401l2.html
'''

import re
from routercli.operatingsystems.iosxr.iosxr import IOSXR, MSG_LOC_PATT


class CRS(IOSXR):
    
    def L2_PLAOC768_3_ASIC_ERROR(self, **kwargs):
        '''
        Report if frequency per day > 5
        And log cmds
        '''
        
        f = self.get_frequency_of_msg(msg='rcli_msg')
        report = False
        if f >= 5:
            report = True
        
        locations = re.findall(MSG_LOC_PATT, self.show_log)
        cmds = ['! If ASIC error seen, then report',
        'admin show asic-errors all location {location}',
        'show asic-errors all detail location {location}',
        'show log location {location}']
        
        for location in locations:
            for cmd in cmds:
                cmd = cmd.format(location=location)
                self.router.exec_cmd(cmd)
        return report
    
    def check_dsc(self, **kwargs):
        '''
        Report if not in IOS XR
        PLATFORM-DSC-3-LOST_RACK
        '''
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_dsc')
        msg = kwargs.get('msg')
        report = False
        if already_checked is not None:
            report = eval(already_checked)
        else:
            cmd = 'admin show dsc all'
            out = self.exec_cmd(cmd)
            if ' NO ' in out:
                report = True
            if report is False:
                report = self.check_platform()
                
            self.rcli_obj.configout.set(self.hostname, 'check_dsc', report)
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_power_supply(self, **kwargs):
        '''
        PLATFORM-CCTL-3-READ_POWER_SHELF_TYPE_FAILURE
        Report if "Data Not Available" found in "admin show diag power-supply eeprom-info"
        '''
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_power_supply')
        msg = kwargs.get('msg')
        report = False
        if already_checked is not None:
            report = eval(already_checked)
        else:
            cmd = 'admin show diag power-supply eeprom-info | i "Rack|Data Not Available"'
            out = self.exec_cmd(cmd)
            out_lines = out.split('\n')
            bad_powers = list()
            i = 0
            for line in out_lines:
                if 'Data Not Available' in line:
                    bad_powers.append(out_lines[i-1].strip())
                i = i + 1
            if len(bad_powers) >= 1:
                report = True
                self.rcli_obj.configout.set(self.hostname, '{0}_bad_powers'.format(msg), bad_powers)
                
            self.rcli_obj.configout.set(self.hostname, 'check_power_supply', report)
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def report_if_interface_down(self, msg):
        '''
        L2-10GE_PLIM-2-XGXS_RX_SYNC
        PLATFORM-SCC-2-BAD_ID_HW
        Report if interfaces are down linked with the module(s)
        '''
        show_log = self.exec_cmd('show logging | include {0}'.format(msg))
        locations = list(set(re.findall('\w+/(\d/\d+/\w+):', show_log)))      # out=LC/0/14/CPU0:, get 0/14/CPU0
        msg_with_locations = '|'.join(locations)
        if msg_with_locations == '':
            # report false
            msg_with_locations = msg
        report = self.check_interface_status(msg_with_locations)
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_fpd(self, **kwargs):
        msg = kwargs.get('msg')
        reports = list()
        reports.append(self.check_fabric_health(msg))
        show_log = self.exec_cmd('show logging | include {0}'.format(msg))
        locations = list(set(re.findall('\w+/(\d/\d+/\w+):', show_log)))      # out=LC/0/14/CPU0:, get 0/14/CPU0
        for location in locations:
            out = self.exec_cmd("admin show hw-module fpd location {loc} | i fpga".format(loc=location))
            if "Yes" in out:
                reports.append(True)
                self.rcli_obj.configout.set(self.hostname, 'fpd_not_upgraded', "Yes {0}".format(location))
                
        if True in reports:
            report = True
        else:
            report = False
            
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def card_reset(self, **kwargs):
        '''
        Report if card reset with logs matching or Fabric Health is bad or not in IOS XR
        '''
        msg = kwargs.get('msg')
        report = self.check_fabric_health(msg)
        if report is True:
            return report
        else:
            #TODO: Test
            self.card_reboot_syslog(msg=msg)
    
    def check_fabric_bundle(self, msg):
        '''
        FABRIC-FABRIC_DRVR-3-LINK_SHUT_DOWN_PERM
        SP/0/SM6/SP:Apr  7 14:49:31.554 utc: sfe_drvr[121]: %FABRIC-FABRIC_DRVR-3-LINK_SHUT_DOWN_PERM : 
        Link superstarrx/0/SM6/SP/0/25  Shutdown permanently due to Excess correctable Error .
        '''
        report = False
        cmd = 'show logging | include {msg}'.format(msg=msg)
        out = self.exec_cmd(cmd)
        locations = list(set(re.findall('Link superstarrx/(\w+/\w+/\w+/\w+/\w+)', out)))      # out=F0/SM0/FM/0
        fabric_links_down = str()
        for location in locations:
            if 'F' in location[:location.find('/')]:    # e.i multichassis because of F0 or F1...
                stage = 's2rx'  # For FCC, look for Stage 2
            else:
                stage = 's1rx'  # For LCC, look for Stage 1
            cmd = 'admin show controller fabric link port {stage} {location} | i DOWN | exclude "Unused|N/A"'.format(stage=stage, location=location)
            per_stage_out = self.exec_cmd(cmd)
            if 'DOWN' in per_stage_out:
                report = True
                fabric_links_down += '. Used Fabric Link is down: {0}'.format(location)
        
        if fabric_links_down != str():
            self.rcli_obj.configout.set(self.hostname, '{0}_fabric_links_down'.format(msg), str(fabric_links_down))
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_interface_status(self, msg):
        report = False
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        interfaces = list(set(re.findall('\sInterface\s(.*),', out)))
        ints_down = list()
        for interface in interfaces:
            ip_out = self.exec_cmd('show ipv4 interface brief | include {interface}'.format(interface=interface))
            if 'Down' in ip_out and 'Shutdown' not in ip_out:       # e.i Down Down
                show_run = self.exec_cmd('show run interface {interface}'.format(interface=interface))
                if 'No such configuration item' not in show_run:
                    if 'ipv4 address' in show_run or 'ipv6 address' in show_run:
                        if 'shutdown' not in show_run:
                            self.log('Reporting because int {0} is Operationally Down'.format(interface))
                            report = True
                            ints_down.append(interface)
        
        if ints_down != list():
            ints_down = ' Following interfaces are down:' + ','.join(ints_down)
            self.rcli_obj.configout.set(self.hostname, '{0}_ints_down'.format(msg), str(ints_down))
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
        
    def check_platform(self, **kwargs):
        # Report if not IOS XR
        # Node          Type              PLIM             State           Config State
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_platform')
        msg = kwargs.get('msg')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            rcli_not_ios_xr_state = str()
            good_platform_states = 'IOS XR RUN|UNPOWERED|READY|OK'
            platform_regex = '(?P<node>\w+/\w+/.*?)\s{2,}(?P<node_type>.*?)\s{2,}(?P<plim>.*?)\s{2,}(?P<state>.*?)\s{2,}(?P<config_state>.*?)$'
            out = self.exec_cmd('admin show platform')
            all_platform_states = list(set(re.findall(platform_regex, out, re.MULTILINE)))
            if all_platform_states == []:
                raise Exception("Invalid platform_regex. Should have matched something")
            for node, node_type, plim, state, config_state in all_platform_states:
                if re.search(good_platform_states, state) is None:
                    report = True
                    rcli_not_ios_xr_state += '. Node: {0} in state: {1}'.format(node, state)
            
            if rcli_not_ios_xr_state != str():
                self.rcli_obj.configout.set(self.hostname, 'rcli_not_ios_xr_state', str(rcli_not_ios_xr_state))
            self.rcli_obj.configout.set(self.hostname, 'check_platform', report)
        
        # Msg specific
        if msg is not None:
            self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report
    
    def check_fabric_health(self, msg):
        # Check IOS XR Platform State and Fabric Health
        # Report if Fabric Health is bad or not in IOS XR
        report = False
        already_checked = self.rcli_obj.configout.get(self.hostname, 'check_fabric_health')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = self.check_platform()
            
            good_planes = ('UP', 'UP')
            plane_detail_regex = '^\s(?P<plane_id>\d)\s\s+(?P<admin_state>.*?)\s\s+(?P<oper_state>.*?)\s\s+(?P<up_dn_c>\d+)\s\s+(?P<up_dn_m>\d+).*'
            out = self.exec_cmd('admin show controllers fabric plane all detail')
            all_plane_states = list(set(re.findall(plane_detail_regex, out, re.MULTILINE)))
            if all_plane_states == []:
                raise Exception("Invalid plane_detail_regex. Should have matched something")
            rcli_not_plane_up = str()
            for plane_state in all_plane_states:
                planeid = plane_state[0]
                admin_state = plane_state[1]
                oper_state = plane_state[2]
                if (admin_state, oper_state) != good_planes:
                    report = True
                    rcli_not_plane_up += '. Plane: {0} in admin/oper state: {1}'.format(planeid, '{0}/{1}'.format(admin_state, oper_state))
            self.rcli_obj.configout.set(self.hostname, 'check_fabric_health', str(report))
            
            if rcli_not_plane_up != str():
                self.rcli_obj.configout.set(self.hostname, 'rcli_not_plane_up', str(rcli_not_plane_up))
            
            # Just capture logs
            cmds = ['admin show controllers fabric connectivity all detail',
                    '! See if lower UCE errors is below 60-70 and are not incrementing',
                    '! Make sure that CE > UCE, if not, then see if UCE errors are increasing.',
                    '! If they are, only then it indicates an error',
                    'admin show controllers fabric plane all statistics',
                    'admin show controllers fabric bundle all detail']
            for cmd in cmds:
                self.exec_cmd(cmd)
            
        # Msg specific
        # Just capture logs
        cmd = 'show logging | include {msg}'.format(msg=msg)
        out = self.exec_cmd(cmd)
        locations = list(set(re.findall('\w+/(\d/\d+/\w+):', out)))      # out=LC/0/14/CPU0:, get 0/14/CPU0
        cmds = ['admin show inventory location {location}',
                'admin show asic-errors all detail location {location}',
                'admin show asic-errors fabricq 0 all location {location}',
                'admin show asic-errors fabricq 1 all location {location}']
        for location in locations:
            for cmd in cmds:
                self.exec_cmd(cmd.format(location=location))
        
        self.rcli_obj.configout.set(self.hostname, '{0}_rcli_report'.format(msg), str(report))
        return report


def main():
    pass

if __name__ == '__main__':
    main()
