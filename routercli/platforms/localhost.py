import platform
p = platform.platform()
if 'Windows-7' in p:
    from routercli.operatingsystems.windows.windows7 import Windows7
    class Localhost(Windows7):
        pass
else:
    raise NotImplementedError()
