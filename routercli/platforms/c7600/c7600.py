import re

from routercli.operatingsystems.ios.ios import IOS


class C7600(IOS):
    
    def ccecode_netflow_cam_utilization(self, **kwargs):
        '''
        threshold -- if percent seen, the report
                     Default to 75%
        '''
        report = False
        msg = kwargs.get('msg')
        threshold = kwargs.get('threshold', 75)
        
        cmd = 'show logging | include {0}'.format(msg)
        self.exec_cmd(cmd)
        
        module_tcamutil_icamutils = list()
        utilization_out = self.exec_cmd('show mls netflow table-contention summary')
        found_modules = re.findall('Module.*?(\d+)', utilization_out)
        found_tcampercents = re.findall('TCAM Utilization.*?(\d+)%', utilization_out)
        found_icampercents = re.findall('ICAM Utilization.*?(\d+)%', utilization_out)
        ziped = zip(found_modules, found_tcampercents, found_icampercents)
        for found_module, found_tcampercent, found_icampercent in ziped:
            if int(found_tcampercent) >= int(threshold) or int(found_icampercent) >= int(threshold):
                report = True
                module_tcamutil_icamutils.append((found_module, found_tcampercent, found_icampercent))
        
        if report is True:
            self.cce_obj.configout.set(self.hostname, '{0}_module_tcamutil_icamutils'.format(msg), str(module_tcamutil_icamutils))
            
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_crash(self, **kwargs):
        '''
        '''
        msg = kwargs.get('msg')
        report = False
        
        cmd = 'show logging | include {0}'.format(msg)
        out = self.exec_cmd(cmd)
        cce_modules = list()
        module_crashed_crash_file = dict()
        
        if '-DFC' in out:
            cce_modules += re.findall('-DFC(\d+)-', out)
            cce_modules = ['module {0}'.format(cce_module) for cce_module in cce_modules]
        if '-SP-' in out:
            cce_modules += ['switch']
        if 'OIR-3-CRASH' in out:
            # Oct 12 09:54:30.676: %OIR-3-CRASH: The module in slot 3 has crashed
            cce_modules += re.findall('slot (\d+)', out)
            cce_modules = ['module {0}'.format(cce_module) for cce_module in cce_modules]
        
        for cce_module in cce_modules:
            if 'module' in cce_module:
                module_no = cce_module.replace('module', '').strip()
                show_module = self.exec_cmd('show module {0}'.format(module_no))
            elif 'switch' in cce_module:
                show_sup = self.exec_cmd('show module | i SUP')
                module_no = re.search('^\s+(\d+)\s+', show_sup).group().strip()
                show_module = self.exec_cmd('show module {0}'.format(module_no))
            elif 'no module in that slot' in cce_module:
                pass    # Take care of it later
            else:
                err = 'Not expected error here. Check cce_module: {0}'.format(cce_module)
                raise Exception(err)
            
            if 'no module in that slot' in cce_module:
                report = False
            else:
                cmd = 'remote command {0} show version'.format(cce_module)
                sh_version = self.exec_cmd(cmd)
                if 'Normal Reload' not in sh_version:
                    if 'Cannot remote to module' in sh_version:
                        report = False
                    else:
                        report = True
                    
            if 'switch' in cce_module:
                crashfile_locs = 'sup-bootflash:'
            else:
                show_module = self.exec_cmd('show module {0}'.format(module_no))
                if 'DFC' in show_module:
                    crashfile_locs = 'dfc#{0}-bootflash:'.format(module_no)
                elif 'SIP' in show_module:
                    crashfile_locs = 'sip#{0}-bootflash:'.format(module_no)
                elif 'WS-X6582-2PA' in show_module:
                    crashfile_locs = 'cwan{0}/0-disk0'.format(module_no)
                else:
                    # Default
                    err = 'Not expected error here. Check show_module: {0}, for module_no: {1}'.format(repr(show_module), module_no)
                    raise Exception(err)
                
            current_time = self.get_current_time()
            thisyr = current_time.year
            for crashfile_loc in crashfile_locs:
                thisyr_crash_files = self.exec_cmd('show {0} | include {1}'.format(crashfile_loc, thisyr))
                thisyr_crash_files = re.findall('(crashinfo_{0}.*)$'.format(thisyr), thisyr_crash_files, re.MULTILINE)
                for thisyr_crash_file in thisyr_crash_files:
                    self.exec_cmd('more {0}:{1} | include %'.format(crashfile_loc, thisyr_crash_file))
                    self.log('Adding thisyr_crash_file: {0}'.format(thisyr_crash_file))
                    module_crashed_crash_file[crashfile_loc] = thisyr_crash_file
                        
        if len(module_crashed_crash_file) >= 1:
            report = True
            cce_msg_module_crashed = ' that occurred on card(s): {0}'.format(crashfile_loc)
            self.cce_obj.configout.set(self.hostname, '{0}_module_crashed'.format(msg), str(cce_msg_module_crashed))
            
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
        
    def ccecode_low_memory(self, **kwargs):
        '''
        MALLOCFAIL
        '''
        # Report if diagnos fails
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_low_memory')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            msg = kwargs.get('msg')
            cmd = 'show logging | include {msg}'.format(msg=msg)
            out = self.exec_cmd(cmd)
            cce_modules = list()
            reports = list()
            if 'MALLOCFAIL' in out:
                reports.append(True)
                
            if '-DFC' in out:
                cce_modules += re.findall('-DFC(\d+)-', out)
                cce_modules = ['module {0}'.format(cce_module) for cce_module in cce_modules]
            if '-SP-' in out:
                cce_modules += ['switch']
            
            if len(cce_modules) == 0:
                cmd = 'show processes memory sorted'
                reports.append(self.ccecode_check_for_high_proc_mem(cmd=cmd, msg=msg))
                cmd = 'show processes cpu sorted'
                reports.append(self.ccecode_check_for_high_cpu(cmd=cmd, msg=msg))
                
            for cce_module in cce_modules:
                cmd = 'remote command {0} show processes memory sorted | i Total'.format(cce_module)
                reports.append(self.ccecode_check_for_high_proc_mem(cmd=cmd, msg=msg))
                cmd = 'remote command {0} show processes cpu sorted | i CPU utilization'.format(cce_module)
                reports.append(self.ccecode_check_for_high_cpu(cmd=cmd, msg=msg))
                
            if True in reports:
                report = True
                self.exec_cmd('dir bootflash:')
            self.cce_obj.configout.set(self.hostname, 'ccecode_low_memory', report)
                
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
        
    def ccecode_check_stp_inconsistency(self, msg):
        '''
        %SPANTREE-SP-2-UNBLOCK_CONSIST_PORT: Unblocking GigabitEthernet3/4 on VLAN0003. Port
        consistency restored.
        Received BPDU with inconsistent peer vlan id 2 on FastEthernet1/3 VLAN1.
        '''
        report = False
        show_stp_inconsistenty = self.exec_cmd('show spanning-tree inconsistentports')
        found_ports = set(re.findall('(\d+)\s+(\w+/\w+)', show_stp_inconsistenty, re.MULTILINE))
        if len(found_ports) >= 1:
            report = True
            cce_stp_ports = list()
            for found_port in found_ports:
                interface = found_port[0]
                vlan = found_port[1]
                cce_stp_ports.append('interface:{0} in vlan:{1}'.format(interface, vlan))
            cce_stp_ports = ';'.join(cce_stp_ports)
            self.cce_obj.configout.set(self.hostname, 'cce_stp_ports', cce_stp_ports)
            
            logs = self.exec_cmd('show logging | include {0}'.format(msg))
            found_ports = set(found_ports + re.findall('\s(\w+/\w+)\s.*?VLAN(\d+)', logs, re.MULTILINE))
            for found_port in found_ports:
                interface = found_port[0]
                vlan = str(int(found_port[1]))
                self.exec_cmd('show run interface {0}'.format(interface))
                self.exec_cmd('show spanning-tree vlan {0}'.format(vlan))
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_oir_caused_cpu_msg(self, msg):
        '''
        CPU_MONITOR-SP-3-TIMED_OUT
        '''
        
        report = False
        logs = self.exec_cmd('show logging | include OIR|admin|CPU')
        
        modules_oired = list(set(re.findall('CPU_MONITOR-SP-3-TIMED_OUT.*? resetting module \[(\d+)\/\d+\]', logs)))
        slots_not_oired = list()
        for module_oired in modules_oired:
            report = self.ccecode_check_module_status(module=module_oired)
            if 'Card inserted in slot {0}, interfaces are now online'.format(module_oired) not in logs:
                slots_not_oired += module_oired
                report = True
        if slots_not_oired != list():
            self.cce_obj.configout.set(self.hostname, '{0}_slots_not_oired'.format(msg), str(slots_not_oired))
            
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
        
    def get_ints_down(self, show_ip_int_br):
        ints_show_down = list()
        interfaces = show_ip_int_br.split('\n')
        for interface_show in interfaces:
            if 'down' in interface_show:
                if 'administratively' not in interface_show:
                    if 'unassigned' not in interface_show:
                        if interface_show not in ints_show_down:
                            ints_show_down.append(interface_show)
        return ints_show_down
    
    def ccecode_check_specific_module_health(self, msg):
        '''
        PM_SCP-SP-1-LCP_FW_ERR
        
        Report if links down for module or module itself has some issues
        
        %PM_SCP-SP-1-LCP_FW_ERR: System resetting module 4
        '''
        
        report = False
        logs = self.exec_cmd('show log | i OIR|admin|LCP_FW_ERR')
        modules = list(set(re.findall('module (\d+)', logs)))
        ints_show_down = list()
        for module in modules:
            report = self.ccecode_check_module_status(module=module)
            if report is False:
                ints_info = self.exec_cmd('show ip interface brief | include {module}/'.format(module=module))
                ints_show_down += self.get_ints_down(ints_info)
        
        if ints_show_down != list():
            ints_show_down = ' Following interfaces are down: ' + ','.join(ints_show_down)
            self.cce_obj.configout.set(self.hostname, '{0}_ints_show_down'.format(msg), str(ints_show_down))
            report = True
            
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_transceiver_unsupported(self, msg):
        # PM_SCP-SP-3-TRANSCEIVER_UNSUPPORTED
        # Report if ports are down
        report = False
        self.exec_cmd('show inventory raw')
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        un_supp_ports = list(set(re.findall('Unsupported transceiver.*(\d+/\d+)', out)))
        if len(un_supp_ports) != 0:
            self.cce_obj.configout.set(self.hostname, '{msg}_un_supp_ports'.format(msg=msg), ','.join(un_supp_ports))
        
        ints_show_down = list()
        for port in un_supp_ports:
            ints_info = self.exec_cmd('show ip interface brief | include {0}'.format(port))
            ints_show_down += self.get_ints_down(ints_info)
        
        if ints_show_down != list():
            ints_show_down = ' Following interfaces are down: ' + ','.join(ints_show_down)
            self.cce_obj.configout.set(self.hostname, '{0}_ints_show_down'.format(msg), str(ints_show_down))
            report = True
            
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_check_module_status(self, **kwargs):
        msg = kwargs.get('msg')
        module = kwargs.get('module')
        
        # Report if module is not Ok
        report = False
        if module is not None:
            out = self.exec_cmd('show module {module}'.format(module=module))
        else:
            out = self.exec_cmd('show module')
        found_diag = False
        non_passed_modules = list()
        for line in out.split('\n'):
            if re.search('Mod  Online Diag Status', line):
                found_diag = True
            if re.search('^\s+\d\s+', line, re.MULTILINE) and found_diag:
                if re.search('^\s+\d\s+.*Pass', line, re.MULTILINE) is None:
                    non_passed_modules.append(line)
                    self.log('Diag not Pass')
                    report = True
        
        if len(non_passed_modules) != 0:
            failed_modules = '"{0}"'.format(','.join(non_passed_modules))
            if msg is not None:
                self.cce_obj.configout.set(self.hostname, '{msg}_failed_modules'.format(msg=msg), failed_modules)
            else:
                self.cce_obj.configout.set(self.hostname, 'failed_modules'.format(msg=msg), failed_modules)
        
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
            
        return report
    
    def ccecode_check_diagnostic_result(self, **kwargs):
        msg = kwargs.get('msg')
        
        # Report if diagnos fails
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_check_diagnostic_result')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            out = self.exec_cmd('show diagnostic result')
            if '> F' in out:
                report = True
                failed_diagnostics = ','.join(list(set(re.findall('([a-zA-Z]+).*?> F', out, re.MULTILINE))))
                self.cce_obj.configout.set(self.hostname, '{msg}_failed_diagnostics'.format(msg=msg), failed_diagnostics)
            self.cce_obj.configout.set(self.hostname, 'ccecode_check_diagnostic_result', report)
        
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_diag_n_mod_test(self, **kwargs):
        # Report if Module or Diag failed
        msg = kwargs.get('msg')
        test1 = self.ccecode_check_diagnostic_result(**kwargs)
        test2 = self.ccecode_check_module_status(**kwargs)
        report = test1 or test2
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
    
    def ccecode_check_cef(self, **kwargs):
        '''
        Report if cef inactive or down
        '''
        msg = kwargs.get('msg')
        
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_check_cef')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            out = self.exec_cmd('show cef linecard')
            if 'down' in out or 'Inactive' in out:
                report = True
                no_cef_cards1 = list(set(re.findall('(\d+/\d+).*down', out, re.MULTILINE)))
                no_cef_cards2 = list(set(re.findall('(\d+/\d+).*Inactive', out, re.MULTILINE)))
                no_cef_cards = ','.join(list(set(no_cef_cards1 + no_cef_cards2)))
                self.cce_obj.configout.set(self.hostname, '{msg}_no_cef_cards'.format(msg=msg), no_cef_cards)
            self.cce_obj.configout.set(self.hostname, 'ccecode_check_cef', report)
        
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_check_power(self, **kwargs):
        # Report if power is off
        msg = kwargs.get('msg')
        
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_check_power')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            out = self.exec_cmd('show power status all')
            if 'off' in out:
                report = True
                powers_off = ','.join(list(set(re.findall('(^\d+).*?off', out, re.MULTILINE))))
                self.cce_obj.configout.set(self.hostname, '{msg}_powers_off'.format(msg=msg), powers_off)
            self.cce_obj.configout.set(self.hostname, 'ccecode_check_power', report)
        
        if msg is not None:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
