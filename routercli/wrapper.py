'''
#! /usr/bin/env python

Program
=======
* This is Wrapper for executing multiple commands on multiple Routers
* Input - .ini format
* Output - Output can be in various types:
** 'xls' - In excel
** 'sqlite3' - In sqlite3
** 'ini' - In .ini format

This is a program to execute automate CLI command execution on network devices from this program.

How to use it?
# Create a config file with DEFAULT_CFG_TEMPLATE file format
# Execute this program

Primary features of this program:
* Allows multiple CLI commands against multiple network devices
* Allows simultaneous execution
* Allows dynamic python code execution

Usage
=====
Option 1) As Python module
--------------------------
wrapper = Wrapper()
wrapper.set_arguments()

config_data = wrapper.parse_cfg()
wrapper.execute_wrapper(config_data)

Option 2) As Python CLI Script
------------------------------
Wrapper.py [options]

Options:
  -h, --help            show this help message and exit
  --dir=DIR             Main Directory which will be used by all other options
                        as relative path; accepts full directory path only
                        (unix style); Required
  --logfile_prgm=LOGFILE_PRGM
                        Program Log Filename where logs of program are stored;
                        accepts filename only; default: routercli.log
  --cfgfile_prgm=CFGFILE_PRGM
                        Program Config File where all hostnames with cmds to
                        execute are stored; accepts filename only; default: routercli.ini
  --devicesdir=DEVICESDIR
                        Devices Directory where dirs for each network device
                        will exist; accepts directory name only; default: DIR

Thanks to Amit Barik!!
Created on Jan 7, 2013

@author: ambarik
'''

__author__  = "Amit Barik <wilson.amit@gmail.com>"
__status__  = "production"
__version__ = "1.0.0"
__date__    = "04 Aug 2014"


import os, sys
from utils.logger import MyLogger
from utils.utils import string_format

# Import custom python packages
ROOT_PACKAGE_PATH = os.path.dirname(os.path.dirname(os.path.join(os.getcwd(), __file__)))
if ROOT_PACKAGE_PATH not in sys.path:
    sys.path.append(ROOT_PACKAGE_PATH)
    
# These are only used for casting (e.i with auto_assign set to True)
from routercli.operatingsystems.ios.ios import IOS
from routercli.operatingsystems.iosxr.iosxr import IOSXR
from routercli.platforms.asr.asr9k import ASR9K
from routercli.platforms.crs.crs import CRS
from routercli.platforms.c7600.c7600 import C7600
from routercli.platforms.cat4500.cat4500 import Cat4500

# To Cast router to specific platform/version
SW_VERSION_CLASSES_MAP = [(IOS, 'Cisco Internetwork Operating System Software|Cisco ios Software'), 
                      (IOSXR, 'Cisco ios XR Software')]
PLATFORM_CLASSES_MAP = [(ASR9K, 'asr9k'), (CRS, 'CRS'), (C7600, 'c7600'), (C7600, 's72033_rp'), (Cat4500, 'cat4500')]

# Importing these as they can be used during eval/exec
from routercli.router import GENERIC_CISCO_PROMPT  # @UnusedImport
from routercli.router import GENERIC_CISCO_PROMPT1  # @UnusedImport


from router import Router
from utils import myconfigparser

# Default Constants
WELCOME_NOTE = """Welcome to routercli!!"""
LOGGER_NAME = 'routercli.Wrapper'
DEFAULT_CFG_TEMPLATE = '../docs/template.ini'
DEFAULT_CFG_FILE = r'/tmp/routercli.ini'
DEFAULT_LOG_FILE = r'/tmp/routercli.log'
DEFAULT_DEVICE_DIR = ''             # Defaults to dir argument
CFG_OUT = r'{filename}-out.{ext}'
OUTPUT_TYPE_INI = 'ini'
OUTPUT_TYPE_SQL = 'sqlite3'
OUTPUT_TYPE_XLS = 'xls'
DEFAULT_OUT_TYPES = [OUTPUT_TYPE_INI]
CFG_OUT_ERROR_SEC = 'ERRORS'
PRGM_TITLE = 'Router CLI Cmds Executor(routercli)'
PRGM_EPILOG = 'Thanks to Amit Barik!!'
DESCRIPTION_OF_PRGM = '''
This is a program to execute automate CLI command execution on network devices from this program.

How to use it?
# Create a config file with {cfg_template} file format
# Execute this program

Primary features of this program:
* Allows multiple CLI commands against multiple network devices
* Allows simultaneous execution
* Allows dynamic python code execution

Have fun!!!
'''.format(cfg_template=DEFAULT_CFG_TEMPLATE)
TOKEN_CHANGE_WAIT = 15      # Wait in seconds for RSA Token to Change
from routercli.router import DEFAULT_TIMEOUT_SESSION

# DEFAULT_CFG_FILE parameters
# TODO: Any future argument needs to be added here 1st
# CFG_PARAMETER_FOO = ('parameter_foo', 'default_value')
CFG_GLOBAL_PARAMS = 'GLOBAL_PARAMS'
CFG_HOSTNAME = 'hostname'
CFG_USERNAME = 'username'
CFG_PASSWORD = 'password'
CFG_LOGIN_CMD = ('login_cmd', 'telnet {hostname}')              # Login Command. Defaults to telnet to value of CFG_HOSTNAME
CFG_USE_RSA_TOKEN = 'USE_RSA_TOKEN'                     # Optional Value of CFG_PASSWORD. If so, then use RSA Token
CFG_MAX_PARALLEL_SESSIONS = ('max_parallel_sessions', 1)        # At a time, no. of open sessions (e.i login to each device). Defaults to sequential
CFG_LOGIN_RETRY = ('login_retry', False)                        # If True, retry login if failed 1st time. Defaults to don't retry login
CFG_LOGIN_RETRY_TIMER = ('login_retry_timer', 60)               # Max wait period in seconds after a login failure
CFG_TIMEOUT_SESSION = ('timeout_session', DEFAULT_TIMEOUT_SESSION)      # Max wait period in seconds for the prompt
CFG_TERM_LENGTH = 'term_length'                                 # If provided, command TERM_LEN_CMD will be executed with specific length
CFG_TERM_WIDTH = 'term_width'                                   # If provided, command TERM_WIDTH_CMD will be executed with specific length
CFG_SET_OS_NAME = 'set_os_name'
CFG_SET_ROUTER_CAST = 'set_router_cast'
CFG_AUTO_ASSIGN = ('auto_assign', True)                         # Run pre-setup commands to assign device to a specific Platform/SW Type. Defaults to True
CFG_AUTO_CLEAN = ('auto_clean', True)                           # Clean console logfile by removing special characters. Defaults to True
CFG_DEVICE_DIR = 'device_dir'                                   # Device Directory Full Path. Defaults to dir argument + hostname (overriden by  create_dir_per_device)
CFG_DIR_PER_DEVICE = ('create_dir_per_device', False)           # If True, Create directory per device to store logfile
CFG_LOGFILE_CONSOLE = 'logfile_console'                         # Console logfile Name (only filename). Defaults to format: hostname-timestamp.log
CFG_ENCODE_PASSWORD = ('encode_password', False)                # If True, then value of CFG_PASSWORD accepts only base64-encoded password
CFG_ENCODE_USERNAME = ('encode_username', False)                # If True, then value of CFG_USERNAME accepts only base64-encoded password
CFG_OUTPUT_TYPES = ('output_types', DEFAULT_OUT_TYPES)          # Can have following values separated by comma: ini, sqlite3, xls

# Private Parameters (not to be used by users)
_CFG_LOGFILE_PRGM_PER_DEVICE = ('_create_logfile_prgm_per_device', False)   # If True, create logfile-prgm file per device. Defaults to False

# Dynamic Code
TERM_LEN_CMD = 'terminal length {len}'
TERM_WIDTH_CMD = 'terminal width {width}'
DYN_PY_START = '$$$PyCodeBegin$$$'
DYN_PY_END = '$$$PyCodeEnd$$$'
DYN_PY_ARG_FORMAT = r'{[A-Z_]([A-Z\d_]+)?}'       # Variable in routercli config cmd: e.g: {FOO} must be uppercase starting with Alphabet
DYN_PY_GLOBAL_VARS = {'ASR9K':ASR9K, 'CRS': CRS, 'C7600':C7600,
                      'Cat4500':Cat4500, 
                      'IOS':IOS, 'IOSXR':IOSXR, 'Router':Router}
CODE_TAB = '    '
CODE_PRESENT = '! {0}'.format(CODE_TAB)


import re
import time
import threading
import base64
from optparse import OptionParser

class CCEOptionParser(OptionParser):
    def format_description(self, formatter):
        return self.description
    
    
class Wrapper(object):
    def __init__(self, **kwargs):
        self.logger = None
        self.dir = kwargs.get('dir')
        self.logfile_prgm = kwargs.get('logfile_prgm', DEFAULT_LOG_FILE)
        self.cfgfile_prgm = kwargs.get('cfgfile_prgm', DEFAULT_CFG_FILE)
        self.cfgoutfiles = kwargs.get('cfgoutfiles', list())                         # Filenames with extension CFG_OUTPUT_TYPES
        self.devicesdir = kwargs.get('devicesdir', DEFAULT_DEVICE_DIR)
        self.options = None
        self.config = None
        self.configout = None   # Must be of XConfigParser type
        self.output_types = list()

        # DEFAULT_CFG_FILE params
        self.cfg_global_params = dict()

        # Private
        self._failed_device_list = list()
        self._rsa_token_old_password = str()
        self._rsa_token_new_password = str()
    
    def parse_arguments(self):
        parser = CCEOptionParser()
        parser.title = PRGM_TITLE
        parser.description = DESCRIPTION_OF_PRGM
        parser.epilog = PRGM_EPILOG
        parser.add_option("--dir",
                          dest='dir',
                          help="Main Directory which will be used by all other options as relative path; accepts full directory path only (platform style); Required")
        parser.add_option("--logfile_prgm",
                          dest='logfile_prgm',
                          help="Program Log Filename where logs of program are stored; accepts filename with full/relative path; default: {0}".format(DEFAULT_LOG_FILE),
                          default=DEFAULT_LOG_FILE)
        parser.add_option("--cfgfile_prgm",
                          dest='cfgfile_prgm',
                          help="Program Config File where all hostnames with cmds to execute are stored; accepts filename with full/relative path; default: {0}".format(DEFAULT_CFG_FILE),
                          default=DEFAULT_CFG_FILE)
        parser.add_option("--devicesdir",
                          dest='devicesdir',
                          help="Devices Directory where folders for each Cisco device will exist; accepts directory name only; default: DIR",
                          default=DEFAULT_DEVICE_DIR)
        
        options = parser.parse_args()[0]
        if not options.dir: # dir is Required
            parser.error('--dir not provided. Use -h to see Help')
            
        self.options = options
        
        _dir = options.dir
        logfile_prgm = options.logfile_prgm
        cfgfile_prgm = options.cfgfile_prgm
        devicesdir = options.devicesdir
        self.set_arguments(dir=_dir, logfile_prgm=logfile_prgm,
                                  cfgfile_prgm=cfgfile_prgm, devicesdir=devicesdir)
        
    def set_arguments(self, **kwargs):
        """Uses parsed/provided arguments to:
        * create directory/assign directory
        * define directory path
        * set configout
        """
        _dir = kwargs.get('dir', self.dir)
        logfile_prgm = kwargs.get('logfile_prgm', self.logfile_prgm)
        cfgfile_prgm = kwargs.get('cfgfile_prgm', self.cfgfile_prgm)
        devicesdir = kwargs.get('devicesdir', self.devicesdir)
        
        logs_to_add = list()
        logs_to_add_debug = list()
        logs_to_add.append(WELCOME_NOTE)
        logs_to_add.append('Arguments provided(default)...')
        logs_to_add.append('dir:                  {0}'.format(repr(_dir)))
        logs_to_add.append('logfile_prgm:         {0}'.format(repr(logfile_prgm)))
        logs_to_add.append('cfgfile_prgm:         {0}'.format(repr(cfgfile_prgm)))
        logs_to_add.append('devicesdir:           {0}'.format(repr(devicesdir)))
        
        if _dir is None:
            self.exception("Argument 'dir' not specified")
        self.dir = os.path.abspath(_dir)
        if not os.path.isdir(self.dir):
            self.exception("No such directory: {0}".format(self.dir))
        
        if not os.path.isfile(logfile_prgm):
            # file doesn't exist
            if os.path.isdir(os.path.dirname(logfile_prgm)) is False:
                # dir doesn't exists or not provided; use only filename
                self.logfile_prgm = os.path.join(self.dir, os.path.basename(logfile_prgm))
                logs_to_add_debug.append("logfile_prgm's basename is being used only with dir argument as directory path")
            else:
                # dir exists; use full path
                self.logfile_prgm = logfile_prgm
                logs_to_add_debug.append("Using logfile_prgm's directory path and filename. Not using dir argument here")
        else:
            # file exists; use full path
            logs_to_add_debug.append("Using existing logfile_prgm: {0}".format(logfile_prgm))
            self.logfile_prgm = logfile_prgm
        
        self.logger = MyLogger(name=LOGGER_NAME, filename=self.logfile_prgm)
        for _logs_to_add_debug in logs_to_add_debug:
            self.log(_logs_to_add_debug)
        for _logs_to_add in logs_to_add:
            self.log(_logs_to_add, 'INFO')
        
        if not os.path.isfile(cfgfile_prgm):
            self.log("Not using directory from {0} as filename doesn't exist. Using dir argument with basename".format(cfgfile_prgm))
            # file doesn't exist; use filename only
            self.cfgfile_prgm = os.path.join(self.dir, os.path.basename(cfgfile_prgm))
        else:
            # file exists; use full path
            self.log("Using logfile_prgm: {0}".format(cfgfile_prgm))
            self.cfgfile_prgm = cfgfile_prgm
        
        if devicesdir == DEFAULT_DEVICE_DIR:
            self.devicesdir = self.dir
        else:
            self.devicesdir = os.path.join(self.dir, os.path.basename(devicesdir))
            
        if os.path.isdir(self.devicesdir) is False:
            self.log('Creating directory devicesdir: {0}'.format(self.devicesdir), 'INFO')
            os.makedirs(self.devicesdir)
        else:
            self.log('Using directory devicesdir: {0}'.format(self.devicesdir), 'INFO')
    
    def parse_cfg(self):
        config = myconfigparser.MyConfigParser()
        config.optionxform = str
        if self.cfgfile_prgm is None:
            self.exception("cfgfile_prgm not found. Have you parsed/set arguments?")
        elif os.path.exists(self.cfgfile_prgm) is False:
            self.exception("File cfgfile_prgm: '{0}' not found. Please create a config file".format(self.cfgfile_prgm))
        self.log('Parsing Config File: {0}'.format(self.cfgfile_prgm))
        config.readfp(open(self.cfgfile_prgm))
        self.config = config
        
        config_data = config._sections
        max_devices = len(config_data)
        self.log('No. of devices being accessed: {0}'.format(max_devices))
        return config_data

    def create_cfgout(self):
        # Saving cfgout
        filename, ext = os.path.splitext(os.path.basename(self.cfgfile_prgm))
        xkwargs = dict()
        for ext in self.output_types:
            cfgoutfile = os.path.join(self.dir, CFG_OUT.format(filename=filename, ext=ext))
            if ext == OUTPUT_TYPE_INI:
                xkwargs['OUTPUTFILE_INI'] = cfgoutfile
            elif ext == OUTPUT_TYPE_SQL:
                xkwargs['OUTPUTFILE_SQL'] = cfgoutfile
            elif ext == OUTPUT_TYPE_XLS:
                xkwargs['OUTPUTFILE_XLS'] = cfgoutfile
            self.log('Creating Config Out file: {0}'.format(cfgoutfile), 'INFO')
        
        self.configout = myconfigparser.XConfigParser(**xkwargs)
        self.configout.optionxform = str

    def _exec_cmd(self, cmd, current_router):
        """
        Execute one cmd and log necessary info
        """
        try:
            self.log('For hostname: {0} executing command: {1}'.format(current_router.hostname, repr(cmd)), 'INFO')
            out = current_router.exec_cmd(cmd)
        except Exception as e:
            msg = 'Error found while executing: {0}'.format(e)
            self.log(msg, 'EXCEPTION')
            self.configout.set(current_router.hostname, cmd, repr(msg))
            self._failed_device_list.append(current_router.hostname)
            self.configout.set(CFG_OUT_ERROR_SEC, 'Error while exec_cmd for {0}'.format(current_router.hostname),
                               repr(msg))
        else:
            msg = ('DEVICE: {0}, CMD: {1}, OUTPUT: {2}'
                   ''.format(current_router.hostname,
                             repr(cmd), repr(out)))
            self.log(msg)
            self.configout.set(current_router.hostname, cmd, repr(out))
            
    def exec_cmds(self, cmds, current_router):
        '''
        This method will execute all the commands sequentially.
        Python Code in config file must exist b/w DYN_PY_START and DYN_PY_END
        
        Arguments:
            cmds:    List of cmds to be executed (also contains dynamic code)
        
        Example:
        # Cmds will be executed as shown as command (Cisco's comment)
        # Output of variables with variable name in all uppercase can be used 
        # in later commands by using it in format like: DYN_PY_ARG_FORMAT
        # ./routercli.cfg
        [router name]
        ! 1st cmd
        $$$PyCodeBegin$$$
        router.exec_cmd(current_router, "! 2nd")
        $$$PyCodeEnd$$$
        ! 3rd
        $$$PyCodeBegin$$$
        router.exec_cmd(current_router, "! 4th")
        out = router.exec_cmd(current_router, "foo cmd")
        FOO = bar(out)
        $$$PyCodeEnd$$$
        ! 5th {FOO}
        '''
        
        self.log('For hostname: {0}, Cmds to execute: {1}'
                 ''.format(current_router.hostname, cmds), 'INFO')
        
        no_of_dyn_code = 0
        py_code_args = dict()
        indexes_to_skip = list()    # Those indexes that have Python Dynamic Code
        for i, cmd in enumerate(cmds):
            if cmd == DYN_PY_START:
                # Dynamic-Codes
                no_of_dyn_code = no_of_dyn_code + 1
                dynamic_code_name = 'DynamicCode{0}'.format(no_of_dyn_code)
                self.log('Dynamic code found - No.: {0}'.format(no_of_dyn_code))
                
                # Python Code must exist b/w DYN_PY_START and DYN_PY_END
                dyn_start_ith = i
                
                try:
                    dyn_end_ith = cmds.index(DYN_PY_END, i+1)
                except ValueError:
                    # No DYN_PY_END for DYN_PY_START
                    dyn_end_ith = -1
                    if cmds.count(DYN_PY_END) >= 1:
                        # DYN_PY_END found before DYN_PY_START
                        msg = ('INVALID syntax in config file. '
                               'Found "{1}" before "{0}". '
                               ''.format(DYN_PY_START, DYN_PY_END))
                    msg += ('For "{0}",'
                           ' "{1}" was not found for a "{0}". Must '
                           'be in pair'.format(dynamic_code_name, 
                                               DYN_PY_START, DYN_PY_END))
                    self.log(msg, 'WARNING')
                    self.configout.set(CFG_OUT_ERROR_SEC, 
                                       ('Error while gathering dyn_code for {0}'
                                        ''.format(current_router.hostname)),
                                       repr(msg))
                    self._failed_device_list.append(current_router.hostname)
                    continue
                
                # Get Python Code
                py_code = '\n'.join(cmds[dyn_start_ith+1:dyn_end_ith])
                indexes_to_skip += range(dyn_start_ith, dyn_end_ith+1)
                
                # Execute Python Code
                py_code_args, py_code_err = self.dyn_py_code(py_code, current_router)
                store = ('py_code: """{0}""", py_code_args: {1}, py_code_err: {2}'
                         ''.format(repr(py_code), repr(py_code_args), repr(py_code_err)))
                self.configout.set(current_router.hostname, dynamic_code_name, store)
            elif cmd != DYN_PY_START:
                # Static-Commands
                
                # Skip Dynamic Python Codes
                if i in indexes_to_skip:
                    continue
                
                # Update cmds with args
                new_cmd = cmd
                if (re.search(DYN_PY_ARG_FORMAT, cmd) is not None or
                    py_code_args == dict()):
                    try:
                        new_cmd = cmd.format(**py_code_args)
                    except KeyError as e:
                        msg = 'Key {0} was not found. Therefore, commenting cmd'.format(e)
                        self.log(msg, 'EXCEPTION')
                        self.configout.set(CFG_OUT_ERROR_SEC, 
                                           ('Error while using py_code_args for {0}'
                                            ''.format(current_router.hostname)),
                                           repr(msg))
                        new_cmd = '! ' + cmd
                    self.log('Dynamic Code - For hostname "{0}", updated cmd from "{1}" to "{2}"'
                             ''.format(current_router.hostname, cmd, new_cmd))
                self._exec_cmd(new_cmd, current_router)
            # end of if statement
        return
    
    def dyn_py_code(self, py_code, current_router):
        """
        Allow routers to run dynamic code
        
        Returns variables whose names match pattern: DYN_PY_ARG_FORMAT1
        as tuple with dict()
        """
        self.log('Dynamic Code - For hostname "{0}", executing python code \n"""{1}"""'.format(current_router.hostname, py_code))
        exported_errs = str()
        code_object = compile(py_code, '<string>', 'exec')
        code_globals = DYN_PY_GLOBAL_VARS
        code_globals['current_router'] = current_router
        code_globals['save_to_report'] = self.save_to_report 
        code_locals = {}
        try:
            exec code_object in code_globals, code_locals
        except Exception as e:
            msg = 'Dynamic Code - Skipping code. Exception found: {0}'.format(e)
            self.log(msg, 'EXCEPTION')
            self.configout.set(CFG_OUT_ERROR_SEC, 'Error while executing dyn_py_code {0} for {1}'.format(repr(py_code), current_router.hostname), repr(msg))
            self._failed_device_list.append(current_router.hostname)
            exported_errs = str(e)
        exported_vars = dict()
        for arg_name in code_locals:
            if arg_name.isupper():
                exported_vars[arg_name] = code_locals[arg_name]
        return exported_vars, exported_errs
    
    def rsa_token(self):
        # Waiting for RSA password to change
        from routercli.platforms.localhost import Localhost
        localhost = Localhost(logger=self.logger)
        self._rsa_token_new_password = localhost.get_rsa_token()
        while self._rsa_token_new_password == self._rsa_token_old_password:
            wait = TOKEN_CHANGE_WAIT
            msg = 'Waiting {0} secs for RSA Token to change'.format(wait)
            self.log(msg)
            time.sleep(wait)
            self._rsa_token_new_password = localhost.get_rsa_token()
        self._rsa_token_old_password = self._rsa_token_new_password
    
    def init_router(self, router_kwargs):
        '''
        Initialize router
        
        Arguments:
        router_kwargs        -- Dict that contains all kwargs present in <router.__init__> Object
        
        '''
        
        hostname = router_kwargs.pop('hostname')
        log_dir = router_kwargs.pop('device_dir')
        rcli_obj = self
        current_router = Router(hostname, log_dir=log_dir, rcli_obj=rcli_obj, **router_kwargs)
        
        # Update the specific arguments
        # Set the password
        password = router_kwargs.get('password')
        if password == CFG_USE_RSA_TOKEN:
            self.rsa_token()
            password = self._rsa_token_new_password
        current_router.password = password
        return current_router
    
    def execute_one_device(self, hostname, config_data):
        cfg_per_host = config_data[hostname].copy()
        self.configout.add_section(hostname)
        router_kwargs = self._parse_cfg_params(hostname, cfg_per_host)
        
        # Initialize variables and create router
        current_router = self.init_router(router_kwargs)
        
        # Login to router
        skip_device, current_router = self.login_to_router(current_router, router_kwargs)
        
        # Build cmds
        cmds = list()
        cfg_for_host_with_dups = cfg_per_host.items()       # Allow to see duplicate keys
        for key, val in cfg_for_host_with_dups:
            if val is None:
                cmds.append(key)
        if skip_device is False:
            try:
                # Add term_length 1st
                if current_router.term_length is not None:
                    cmd = TERM_LEN_CMD.format(len=current_router.term_length)
                    current_router.exec_cmd(cmd)
                
                # Add term_width
                if current_router.term_width is not None:
                    cmd = TERM_WIDTH_CMD.format(width=current_router.term_width)
                    current_router.exec_cmd(cmd)
                
                # Cast generic router to specific platform/version
                if router_kwargs.get('auto_assign') is True:
                    current_router = self.cast_router(current_router)
                
                # set_os_name
                if router_kwargs.get('set_os_name'):
                    current_router.set_os_name(router_kwargs.get('set_os_name'))
                
                # set_router_cast
                if router_kwargs.get('set_router_cast'):
                    current_router.set_router_cast()
                
                # Execute commands
                self.exec_cmds(cmds, current_router)
            except Exception as e:
                msg = 'Error found while execute_one_device: {0}'.format(e)
                self.log(msg, 'EXCEPTION')
                self.configout.set(CFG_OUT_ERROR_SEC, 'Error in execute_one_device for {0}'.format(current_router.hostname), repr(msg))
                self._failed_device_list.append(current_router.hostname)

        elif skip_device is True:
            self._failed_device_list.append(hostname)

        try:
            current_router.logout()
        except Exception as e:
            msg = 'Error found while logging out: {0}'.format(e)
            self.log(msg, 'EXCEPTION')
            self.configout.set(CFG_OUT_ERROR_SEC, 'Error while logging out for {0}'.format(current_router.hostname),
                               repr(msg))

    def cast_router(self, current_router):
        '''
        Cast the generic router into specific platform/specific sw version
        so that specific methods may be used
        
        e.g If router is C7600, then class C7600 will be casted on
        '''
        
        # Get show version
        show_version = current_router.show_version()
        
        # Try to find platform/version object
        cast_class = None
        # Try 1st specific platform
        for class_obj, patt in PLATFORM_CLASSES_MAP:
            if patt in show_version:
                cast_class = class_obj
                break
            
        # Try last resort for sw
        if cast_class == None:
            for class_obj, patt in SW_VERSION_CLASSES_MAP:
                if patt in show_version:
                    cast_class = class_obj
                    break
                else:
                    if re.search(patt, show_version) is not None:
                        cast_class = class_obj
                        break
        
        if cast_class != None:
            #TODO: Fix the "Casting Object: router to Object: type. You can use type object methods"
            self.log('Casting Object: {0} to Object: {1}. You can use {1} object methods...'
                     ''.format(current_router.__class__.__name__, cast_class.__name__))
            # Casting... e.g Making Generic router into specific Platform/SW Object
            current_router.__class__ = cast_class
        else:
            self.log('Couldnt Cast Object for hostname: {0} as show_version: "{1}" didnt match SW_VERSION_CLASSES_MAP: {2}'
                     ''.format(current_router.hostname, str(repr(show_version))[0:100], SW_VERSION_CLASSES_MAP))
        return current_router

    def login_to_router(self, current_router, router_kwargs):
        ''' Login
        '''
        # Logging in
        skip_device = False
        login_success = True
        try:
            login_success, login_outp = current_router.login()
            self.configout.set(current_router.hostname, current_router.login_cmd, 'Login Successful')
        except Exception as e:
            msg = ('Skipping device: {0} because of exception: {1}'
                   ''.format(current_router.hostname, repr(e)))
            self.log(msg, 'EXCEPTION')
            skip_device = True
            login_outp = open(current_router.logfile_console).read()
            self.configout.set(current_router.hostname, current_router.login_cmd, repr(login_outp))
            
        # Trying 2nd attempt login
        login_retry = False
        if login_outp == current_router._LOGIN_USERNAME_PROMPTS and skip_device is False:
            login_retry = router_kwargs.get('login_retry')
        if login_retry is True:
            msg = 'First time login failed. Retrying...'
            self.log(msg, level='WARNING')
            self.configout.set(CFG_OUT_ERROR_SEC, 'Error while trying-2nd-attempt-login1 {0}'.format(current_router.hostname),
                               repr(msg))
            current_router.logout()

            wait = router_kwargs.get('login_retry_timer')
            msg = 'Waiting {0} secs to forget the failure'.format(wait)
            self.log(msg, level='WARNING')
            time.sleep(wait)
            
            current_router = self.init_router(router_kwargs)
            login_success, login_outp = current_router.login()
            if login_outp == current_router._LOGIN_USERNAME_PROMPTS:
                msg = 'Login failed. Quitting'
                self.log(msg, level='WARNING')
                self.configout.set(CFG_OUT_ERROR_SEC, 'Error while trying-2nd-attempt-login2 {0}'.format(current_router.hostname),
                                   repr(msg))
                current_router.logout()
                skip_device = True
                
        if not login_success:
            skip_device = True
            
        return skip_device, current_router
    
    def _get_nested_login(self, cfg_dict, router_kwargs):
        '''
        Returns nested_login, a list used in router_kwargs
        
        Arguments:
        cfg_dict            -- Dictionary containing nested_login format information
        router_kwargs       -- kwargs to be used for getting username, password, hostname if used
        '''
        
        nested_login = list()
        cfg_dict_to_use = cfg_dict
        # login_cmd1 is only available in cfg_per_host and not cfg_global_params
        if self.cfg_global_params.get('login_cmd1') is not None:
            self.log('Not supported login_cmd1 under global params!!', 'WARNING')
        # If you wish to support it later, undo this config
#        if cfg_dict.get('login_cmd0') is not None:
#            # login_cmd takes precedence over login_cmd1
#            cfg_dict_to_use = dict()
#        elif cfg_dict.get('login_cmd1') is not None:
#            # login_cmd1 takes precedence over data found in global config
#            cfg_dict_to_use = cfg_dict.copy()
#        elif self.cfg_global_params.get('login_cmd1') is not None:
#            cfg_dict_to_use = self.cfg_global_params.copy()
        if cfg_dict_to_use.get('login_cmd1') is not None:
            # There is a possibility of nested login
            for key in cfg_dict_to_use:
                val = cfg_dict_to_use[key]
                if isinstance(val, str) is True:
                    val = string_format(cfg_dict_to_use[key], **router_kwargs)
                login_cmd_found = re.search('login_cmd(\d+)', key)
                if login_cmd_found is not None:
                    login_no = login_cmd_found.group(1)
                    nxt_login_kwargs = {'login_cmd': val}
                    for key in cfg_dict_to_use:
                        val = cfg_dict_to_use[key]
                        nxt_username = re.search('username{0}'.format(login_no), key)
                        if nxt_username is not None:
                            nxt_login_kwargs['username'] = val
                        nxt_password = re.search('password{0}'.format(login_no), key)
                        if nxt_password is not None:
                            nxt_login_kwargs['password'] = val
                        nxt_final_prompt = re.search('final_prompt{0}'.format(login_no), key)
                        if nxt_final_prompt is not None:
                            nxt_login_kwargs['final_prompt'] = val
                    nested_login.append(nxt_login_kwargs)
        return nested_login
    
    def _parse_cfg_params(self, hostname, cfg_per_host):
        '''
        Takes in the cfg_per_host and returns the router_kwargs
        
        Arguments:
        cfg_per_host        -- Dictionary that represents config parameters of config file per host
        
        Returns router_kwargs that will be used by init_router
        '''
        
        cfg_global_params = self.cfg_global_params
        
        self.log('Parsing Host Specific Parameters...')
        self.log('Host Specific Parameters provided for host: {0}, params: {1}'.format(hostname, cfg_per_host))
        self.log('Host Specific Parameters used based on global parameters or defaults...')
        
        router_kwargs = dict()
        for key in cfg_per_host:
            val = cfg_per_host[key]
            if val != None:
                router_kwargs[key] = val
        
        # TODO: Add any future argument here also
        # Use hostname specific arguments 1st. If not found, use default values found in global config
        ## hostname
        default_hostname = cfg_per_host['__name__']
        hostname = cfg_per_host.get(CFG_HOSTNAME, cfg_global_params.get(CFG_HOSTNAME, default_hostname))
        self.log('{0}: {1}'.format(CFG_HOSTNAME, hostname))
        router_kwargs[CFG_HOSTNAME] = hostname
        
        ## login_cmd
        login_cmd = cfg_per_host.get(CFG_LOGIN_CMD[0], cfg_global_params.get(CFG_LOGIN_CMD[0], CFG_LOGIN_CMD[1]))
        self.log('{0}: {1}'.format(CFG_LOGIN_CMD[0], login_cmd))
        router_kwargs[CFG_LOGIN_CMD[0]] = login_cmd
        
        ## username
        username = cfg_per_host.get(CFG_USERNAME, cfg_global_params.get(CFG_USERNAME))
        self.log('{0}: {1}'.format(CFG_USERNAME, username))
        router_kwargs[CFG_USERNAME] = username
        
        ## encode_password
        ### Definition:      If True, then CFG_PASSWORD accepts base64 encoded value
        ### Default:         False
        encode_password = cfg_per_host.get(CFG_ENCODE_PASSWORD[0], cfg_global_params.get(CFG_ENCODE_PASSWORD[0], CFG_ENCODE_PASSWORD[1]))
        if str(encode_password).lower() == 'false':
            encode_password = False
        else:
            encode_password = True
        self.log('{0}: {1}'.format(CFG_ENCODE_PASSWORD[0], encode_password))
        
        ## password
        password = cfg_per_host.get(CFG_PASSWORD, cfg_global_params.get(CFG_PASSWORD))
        self.log('{0}: {1}'.format(CFG_PASSWORD, password))
        if encode_password is True:
            router_kwargs[CFG_PASSWORD] = base64.b64decode(password)
        else:
            router_kwargs[CFG_PASSWORD] = password
            
        ## encode_username
        ### Definition:      If True, then CFG_USERNAME accepts base64 encoded value
        ### Default:         False
        encode_username = cfg_per_host.get(CFG_ENCODE_USERNAME[0], cfg_global_params.get(CFG_ENCODE_USERNAME[0], CFG_ENCODE_USERNAME[1]))
        if str(encode_username).lower() == 'false':
            encode_username = False
        else:
            encode_username = True
        self.log('{0}: {1}'.format(CFG_ENCODE_USERNAME[0], encode_username))
        
        ## username
        username = cfg_per_host.get(CFG_USERNAME, cfg_global_params.get(CFG_USERNAME))
        self.log('{0}: {1}'.format(CFG_USERNAME, username))
        if encode_username is True:
            router_kwargs[CFG_USERNAME] = base64.b64decode(username)
        else:
            router_kwargs[CFG_USERNAME] = username
            
        ## logfile_console
        logfile_console = cfg_per_host.get(CFG_LOGFILE_CONSOLE, cfg_global_params.get(CFG_LOGFILE_CONSOLE))
        if logfile_console is not None:
            self.log('{0}: {1}'.format(CFG_LOGFILE_CONSOLE, logfile_console))
            router_kwargs[CFG_LOGFILE_CONSOLE] = logfile_console
        
        ## create_dir_per_device
        create_dir_per_device = str(cfg_per_host.get(CFG_DIR_PER_DEVICE[0], cfg_global_params.get(CFG_DIR_PER_DEVICE[0], str(CFG_DIR_PER_DEVICE[1]))))
        if create_dir_per_device.lower() == 'false':
            create_dir_per_device = False
        self.log('{0}: {1}'.format(CFG_DIR_PER_DEVICE[0], create_dir_per_device))
        router_kwargs[CFG_DIR_PER_DEVICE[0]] = create_dir_per_device
        
        ## create_logfile_prgm_per_device
        create_logfile_prgm_per_device = str(cfg_per_host.get(_CFG_LOGFILE_PRGM_PER_DEVICE[0], cfg_global_params.get(_CFG_LOGFILE_PRGM_PER_DEVICE[0], str(_CFG_LOGFILE_PRGM_PER_DEVICE[1]))))
        if create_logfile_prgm_per_device.lower() == 'false':
            create_logfile_prgm_per_device = False
        self.log('{0}: {1}'.format(_CFG_LOGFILE_PRGM_PER_DEVICE[0], create_logfile_prgm_per_device))
        router_kwargs[_CFG_LOGFILE_PRGM_PER_DEVICE[0]] = create_logfile_prgm_per_device
        if create_logfile_prgm_per_device is False:
            router_kwargs['logger'] = self.logger
        
        ## device_dir
        if create_dir_per_device is False:
            default_device_dir = self.devicesdir
        else:
            default_device_dir = os.path.join(self.devicesdir, hostname)
        device_dir = cfg_per_host.get(CFG_DEVICE_DIR, cfg_global_params.get(CFG_DEVICE_DIR, default_device_dir))
        if os.path.exists(device_dir) is False:
            # Create sub dir per device
            self.log('Creating device_dir: {0}'.format(device_dir))
            os.makedirs(device_dir)
        self.log('{0}: {1}'.format(CFG_DEVICE_DIR, device_dir))
        router_kwargs[CFG_DEVICE_DIR] = device_dir
        
        ## login_cmd1
        nested_login = self._get_nested_login(cfg_per_host, router_kwargs)
        self.log('nested_login: {0}'.format(nested_login))
        router_kwargs['nested_login'] = nested_login
        
        ## auto_clean
        auto_clean = str(cfg_per_host.get(CFG_AUTO_CLEAN[0], cfg_global_params.get(CFG_AUTO_CLEAN[0], CFG_AUTO_CLEAN[1])))
        if str(auto_clean).lower() == 'false':
            auto_clean = False
        else:
            auto_clean = CFG_AUTO_CLEAN[1]
        self.log('{0}: {1}'.format(CFG_AUTO_CLEAN[0], auto_clean))
        router_kwargs[CFG_AUTO_CLEAN[0]] = auto_clean
        
        ## login_retry
        login_retry = cfg_per_host.get(CFG_LOGIN_RETRY[0], cfg_global_params.get(CFG_LOGIN_RETRY[0], CFG_LOGIN_RETRY[1]))
        self.log('{0}: {1}'.format(CFG_LOGIN_RETRY[0], login_retry))
        router_kwargs[CFG_LOGIN_RETRY[0]] = login_retry
        
        ## login_retry_timer
        login_retry_timer = int(cfg_per_host.get(CFG_LOGIN_RETRY_TIMER[0], cfg_global_params.get(CFG_LOGIN_RETRY_TIMER[0], CFG_LOGIN_RETRY_TIMER[1])))
        self.log('{0}: {1}'.format(CFG_LOGIN_RETRY_TIMER[0], login_retry_timer))
        router_kwargs[CFG_LOGIN_RETRY_TIMER[0]] = login_retry_timer
        
        ## timeout_session
        timeout_session = int(cfg_per_host.get(CFG_TIMEOUT_SESSION[0], cfg_global_params.get(CFG_TIMEOUT_SESSION[0], CFG_TIMEOUT_SESSION[1])))
        self.log('{0}: {1}'.format(CFG_TIMEOUT_SESSION[0], timeout_session))
        router_kwargs[CFG_TIMEOUT_SESSION[0]] = timeout_session
        
        ## term_length
        term_length = cfg_per_host.get(CFG_TERM_LENGTH, cfg_global_params.get(CFG_TERM_LENGTH))
        self.log('{0}: {1}'.format(CFG_TERM_LENGTH, term_length))
        router_kwargs[CFG_TERM_LENGTH] = term_length
        
        ## term_width
        term_width = cfg_per_host.get(CFG_TERM_WIDTH, cfg_global_params.get(CFG_TERM_WIDTH))
        self.log('{0}: {1}'.format(CFG_TERM_WIDTH, term_width))
        router_kwargs[CFG_TERM_WIDTH] = term_width
        
        ## auto_assign
        ### Definition:      If True, then call cast_router()
        ### Default:         True
        auto_assign = cfg_per_host.get(CFG_AUTO_ASSIGN[0], cfg_global_params.get(CFG_AUTO_ASSIGN[0], CFG_AUTO_ASSIGN[1]))
        if str(auto_assign).lower() == 'false':
            auto_assign = False
        else:
            auto_assign = CFG_AUTO_ASSIGN[1]
        self.log('{0}: {1}'.format(CFG_AUTO_ASSIGN[0], auto_assign))
        router_kwargs[CFG_AUTO_ASSIGN[0]] = auto_assign
        
        ## set_os_name
        ### Definition:      If provided, then call router.set_os_name(val)
        ### Default:         None
        set_os_name = cfg_per_host.get(CFG_SET_OS_NAME, cfg_global_params.get(CFG_SET_OS_NAME))
        self.log('{0}: {1}'.format(CFG_SET_OS_NAME, set_os_name))
        router_kwargs[CFG_SET_OS_NAME] = set_os_name
        
        ## set_router_cast
        ### Definition:      If True, then call router.set_router_cast() and disable auto_assign
        ### Default:         False
        set_router_cast = cfg_per_host.get(CFG_SET_ROUTER_CAST, cfg_global_params.get(CFG_SET_ROUTER_CAST))
        if str(set_router_cast).lower() == 'false':
            set_router_cast = False
        else:
            set_router_cast = True
        self.log('{0}: {1}'.format(CFG_SET_ROUTER_CAST, set_router_cast))
        if set_router_cast:
            router_kwargs[CFG_AUTO_ASSIGN[0]] = False
        router_kwargs[CFG_SET_ROUTER_CAST] = set_router_cast
        
        # Update the values of kwargs with any internal arguments provided with {}
        for key in router_kwargs:
            val = router_kwargs[key]
            if isinstance(val, str) is True:
                new_val = string_format(val, **router_kwargs)
                if new_val != val:
                    self.log('Changed router_kwargs value from {0} to {1} for key {2}'.format(val, new_val, key), 'DEBUG')
                    router_kwargs[key] = new_val
        return router_kwargs
    
    def execute_wrapper(self):
        config_data = self.parse_cfg()
        
        if CFG_GLOBAL_PARAMS in config_data.keys():
            self.cfg_global_params = config_data.pop(CFG_GLOBAL_PARAMS)      # Remove it to count devices
        self.log('Global Parameters provided: {0}'.format(self.cfg_global_params))
        
        # TODO: Add any future only-global parameters here
        ## cfg_max_parallel_sessions
        cfg_max_parallel_sessions = int(self.cfg_global_params.get(CFG_MAX_PARALLEL_SESSIONS[0], CFG_MAX_PARALLEL_SESSIONS[1]))
        if cfg_max_parallel_sessions == 0:
            # Cannot be 0
            cfg_max_parallel_sessions = CFG_MAX_PARALLEL_SESSIONS[1]
        self.log('cfg_max_parallel_sessions: {0}'.format(cfg_max_parallel_sessions))
        
        ## output_types
        output_types = self.cfg_global_params.get(CFG_OUTPUT_TYPES[0], CFG_OUTPUT_TYPES[1])
        output_types = str(output_types).lower()
        if OUTPUT_TYPE_INI in output_types:
            self.output_types.append(OUTPUT_TYPE_INI)
        if OUTPUT_TYPE_SQL in output_types:
            self.output_types.append(OUTPUT_TYPE_SQL)
        if OUTPUT_TYPE_XLS in output_types:
            self.output_types.append(OUTPUT_TYPE_XLS)
        self.log('{0}: {1}'.format(CFG_OUTPUT_TYPES[0], output_types))
        self.create_cfgout()
        
        max_devices = len(config_data)
        hostnames = config_data.keys()[:]
        self.configout.add_section(CFG_OUT_ERROR_SEC)
        for i in range(0, len(hostnames), cfg_max_parallel_sessions):
            hostnames_to_execute_per_instance = hostnames[i:i+cfg_max_parallel_sessions]
            all_threads = list()
            for hostname in hostnames_to_execute_per_instance:
                j = hostnames.index(hostname)
                msg = ('Accessing device no.: {0}. No. of Devices yet to access: {1}'
                       ''.format(j+1, max_devices-j))
                self.log(msg, 'INFO')
                thread = threading.Thread(target=self.execute_one_device,
                                          args=(hostname, config_data),
                                          name=hostname)
                all_threads.append(thread)
                thread.start()
            [thread.join() for thread in all_threads]
        
        if len(self._failed_device_list) == 0:
            level = 'DEBUG'
        else:
            level = 'WARNING'
        self.log('Failed device list: {0}'.format(self._failed_device_list), level=level)
        self.configout.set(CFG_OUT_ERROR_SEC, 'Failed device list', self._failed_device_list)
        
        self.log('Saving Config Out files', 'INFO')
        self.configout.write_to_all()
        self.log('Goodbye from routercli!!', 'INFO')

    def log(self, msg, level='DEBUG'):
        if self.logger is None:
            print msg
        else:
            self.logger.select_logger(LOGGER_NAME)
            self.logger.log(msg, level=level)
    
    def exception(self, reason=None):
        if reason == None and self.logger != None:  # This is called to log the traceback (use it under except clause)
            self.logger.logger.propagate = False
            self.logger.logger.exception("Contact the creator. Time to debug!!")
            raise
        elif reason == None and self.logger == None:
            raise
        else:               # This is called just to log and raise exception, but doesn't log the traceback
            self.log(str(reason), level='WARNING')
            # Close the logfile_prgm
            self.logger = None
            raise Exception(reason)
        
    def save_to_report(self, section, option, value):
        '''Save the values to the report of your choice
        '''
        if section in self.configout.sections():
            self.configout.set(section, option, value)
        else:
            self.configout.add_section(section)
            self.configout.set(section, option, value)
    

def main():
    try:
        # Initialize
        wrapper = Wrapper()
        wrapper.parse_arguments()
        wrapper.execute_wrapper()
    except:
        wrapper.exception()
        
if __name__ == '__main__':
    main()

