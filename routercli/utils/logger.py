'''
Created on Jan 4, 2013

@author: ambarik
# http://stackoverflow.com/questions/13210436/get-full-traceback

Notes:
* use anotherGetLogger.propogate = 0, to not allow overwriting of logs
* anotherGetLogger.manager.root.handlers = []   # Removes all previous loggers
* logging.Logger.manager.loggerDict.keys() # to find all logging.Logger s
'''

import logging
from logging import StreamHandler, Formatter, root, codecs

class FileHandler(logging.FileHandler):
    """
    Overridden to not use self.baseFilename
    """
    def __init__(self, filename, mode='a', encoding=None, delay=0):
        self.__filename__ = filename
        logging.FileHandler.__init__(self, filename, mode, encoding, delay)

    def _open(self):
        """
        Copy/Paste of FileHandler._open
        Changed - self.__filename__
        """
        if self.encoding is None:
            stream = open(self.__filename__, self.mode)
        else:
            stream = codecs.open(self.__filename__, self.mode, self.encoding)
        return stream

class MyLogger():
    def __init__(self, **kwargs):
        self.logger = None
        self.name = kwargs.get('name', 'root')
        self.filename = kwargs.get('filename')
        self.filemode = kwargs.get('filemode', 'w')
        self.level = kwargs.get('level', logging.DEBUG)
        self.level_console = kwargs.get('level_console', logging.INFO)
        self.format_logger = kwargs.get('format_logger', '%(asctime)s %(name)'
            '-10s %(process)d %(threadName)-10s %(levelname)-8s %(message)s')
        self.format_console_datefmt = kwargs.get('format_console_datefmt')
        if self.format_console_datefmt:
            self.format_console = kwargs.get('format_console', '%(asctime)s %(name)-10s: %(levelname)-8s %(message)s')
        else:
            self.format_console = kwargs.get('format_console', '%(name)-10s: %(levelname)-8s %(message)s')
        self.datefmt = kwargs.get('datefmt', '%b %d %H:%M:%S')
        self.propagate = kwargs.get('propagate', 1)
        
        self.setup(**kwargs)
        if self.filename:
            msg = 'MyLogger logs are saved in {0}'.format(self.filename)
            self.log(msg)
    
    def __del__(self):
        if self.logger:
            for hdlr in self.logger.handlers:
                self.logger.removeHandler(hdlr)
                hdlr.flush()
                hdlr.close()
            
    def setup_rootlogger(self, **kwargs):
        """
        Copy/Paste of logging.basicConfig
        """
        if len(root.handlers) == 0:
            filename = kwargs.get("filename", self.filename)
            if filename:
                mode = kwargs.get("filemode", self.filemode)
                hdlr = FileHandler(filename, mode)
            else:
                stream = kwargs.get("stream")
                hdlr = StreamHandler(stream)
            
            fs = kwargs.get("format_logger", self.format_logger)
            dfs = kwargs.get("datefmt", self.datefmt)
            fmt = Formatter(fs, dfs)
            hdlr.setFormatter(fmt)
            root.addHandler(hdlr)
            level = kwargs.get("level", self.level)
            if level is not None:
                root.setLevel(level)
            
            
            # set a format which is simpler for console use
            console = logging.StreamHandler()
            console.setLevel(self.level_console)
            console_formatter = logging.Formatter(self.format_console, self.format_console_datefmt)
            console.setFormatter(console_formatter)
            self.logger = logging.getLogger('')
            self.logger.addHandler(console)
        
    def setup(self, **kwargs):
        # set up logging to file
        self.setup_rootlogger(**kwargs)
        self.logger = logging.getLogger(self.name)
        self.logger.propagate = self.propagate
        if self.filename:
            hdlr = FileHandler(self.filename, self.filemode)
            hdlr.setFormatter(Formatter(self.format_logger, self.datefmt))
            self.logger.addHandler(hdlr)
    
    def select_logger(self, name):
        self.logger = logging.getLogger(name)
        self.logger.propagate = self.propagate
        
    def log(self, msg, level='DEBUG'):
        if self.logger is None:
            self.logger = logging.getLogger('')
        if level == 'INFO':
            self.logger.info(msg)
        elif level == 'WARNING':
            self.logger.warning(msg)
        elif level == 'DEBUG':
            self.logger.debug(msg)
        elif level == 'ERROR':
            self.logger.error(msg)
        elif level == 'EXCEPTION':
            self.logger.exception(msg)


def main():
    mylogger = MyLogger(name='logger1', filename='/tmp/x1.log')
    mylogger.log('hellologger1')
#     
#     from logging import getLogger
#     log = getLogger('unrelated.foo')
#     log.propagate = 0
#     log.error('fasdasd')
     
    mylogger = MyLogger(name='logger2', filename='/tmp/x2.log', propagate=0)
    mylogger.log('hellologger2', 'DEBUG')
     
    mylogger.select_logger('logger1')
    mylogger.log('hellologger11', 'DEBUG')
     
    mylogger.select_logger('logger2')
    mylogger.log('hellologger22', 'DEBUG')
    

if __name__ == '__main__':
    main()
