'''
Created on Mar 23, 2013

@author: ambarik
'''
from collections import OrderedDict #@UnresolvedImport

import copy
import _abcoll

class MyOrderedDict(_abcoll.MutableMapping):
    """http://stackoverflow.com/questions/3387691/python-how-to-perfectly-override-a-dict
    A dictionary which applies an arbitrary key-altering function before accessing the keys
    
    Store duplicate keys in __listed_dict__
    """

    def __init__(self, *args, **kwargs):
        self.__listed_dict__ = list()
        self.__final_dict__ = OrderedDict()
        self.update(OrderedDict(*args, **kwargs)) # use the free update to set keys

    def __getitem__(self, key):
        return self.__final_dict__[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self.__listed_dict__.append((self.__keytransform__(key), value))
        self.__final_dict__[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self.__final_dict__[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.__final_dict__)

    def __len__(self):
        return len(self.__final_dict__)

    def __keytransform__(self, key):
        return key
    
    def __str__(self):
        return str(self.__final_dict__)

    def items(self):
        '''Overriding items to use __listed_dict__
        '''
        return self.__listed_dict__

    def copy(self):
        return copy.copy(self)

def main():
    d = MyOrderedDict()
    d['x'] = 1
    d['x'] = 1
    print d['x']
    d['x'] = 2
    print d['x']
    d['x'] = 3
    d['y'] = 4
    print d.items()
    print d
    x = d
    print x, type(x)
    print d.__final_dict__
    print d.__listed_dict__

if __name__ == '__main__':
    main()
