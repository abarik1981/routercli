'''
Created on Jan 25, 2013

@author: ambarik
'''

from collections import defaultdict
import os, datetime, re, string, subprocess
import threading
import pexpect
import pytz
import dateutil.parser
import dateutil.relativedelta
import json
import urllib2, urlparse, urllib
import cookielib
import base64  # @UnusedImport

try:    # For Python 2.7
    import requests # @UnresolvedImport
    from requests_ntlm import HttpNtlmAuth  # @UnresolvedImport
    import bs4 # @UnresolvedImport
except:
    pass

try:
    from ordereddict import OrderedDict  # @UnresolvedImport @UnusedImport
except:
    from collections import OrderedDict  # @UnusedImport

def get_timediff(t2, t1=None):
    '''get timedifference
    @param t2 {dict}: {ts: 'timestamp as string', ts_format:'format of timestamp'}
    @param t1 {dict}: {ts: 'timestamp as string', ts_format:'format of timestamp'}
                      If None, use current now()
    e.g:
    > print get_timediff({'ts':'2015-04-19_10-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'},
                       {'ts':'2015-04-18_19-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'})
    relativedelta(hours=+15)
    > print get_timediff({'ts':'2015-04-19_10-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'})
    relativedelta(days=+3, hours=+20, minutes=+40, seconds=+39, microseconds=+451066)
    '''
    
    t2_dt = datetime.datetime.strptime(t2['ts'], t2['ts_format'])
    if t1:
        t1_dt = datetime.datetime.strptime(t1['ts'], t1['ts_format'])
    else:
        t1_dt = datetime.datetime.now()
    diff = dateutil.relativedelta.relativedelta(t2_dt, t1_dt)
    return diff

def dt_to_epoch(**kwargs):
    '''
    @param dt_with_tz_as_str {string}: datetime in string with timezone
            http://stackoverflow.com/a/17257177/558397
            http://stackoverflow.com/a/796019/558397
    @param dt_as_dt {datetime.datetime}: datetime object
            http://stackoverflow.com/a/11111177/558397
    e.g:
    > dt_to_epoch(dt_with_tz_as_str='Tue Oct 28 13:00:30 CDT 2014')
    1414519230.0
    
    # Use datetime.datetime.fromtimestamp(ts) to get time back into datetime
    '''
    dt_as_dt = kwargs.get('dt_as_dt')
    dt_with_tz_as_str = kwargs.get('dt_with_tz_as_str')
    
    if (not dt_as_dt and not dt_with_tz_as_str):
        raise Exception('Invalid input to dt_to_epoch')
    if dt_with_tz_as_str:
        d1_my_time = dateutil.parser.parse(str(dt_with_tz_as_str))
        if not d1_my_time.tzinfo:
            d1_my_time = d1_my_time.replace(tzinfo=pytz.utc)
        # get timestamp
        d2_unix_ts = datetime.datetime(1970, 1, 1, tzinfo=pytz.utc)
        ts = (d1_my_time - d2_unix_ts).total_seconds()
        # -> 1346200430.0
        return ts
    if dt_as_dt:
        d2_unix_ts = datetime.datetime.utcfromtimestamp(0)
        ts = int((dt_as_dt - d2_unix_ts).total_seconds())
        return ts

def get_data_from_webpage(url, **kwargs):
    '''
    Args:
    username:    DOMAIN\\username
    password
    cookie_file
    return_soup:        True|False
    
    '''
    
    cookie_file = kwargs.get('cookie_file')
    username = kwargs.get('username')
    password = kwargs.get('password')
    return_soup = kwargs.get('return_soup', False)
    
    if cookie_file is not None:
        cj = cookielib.MozillaCookieJar()
        cj.load(cookie_file)
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        r = opener.open(url)
        html_source = r.read()
    
    if password is not None:
        r = requests.get(url, auth=HttpNtlmAuth(username, password))
        html_source = r.text
    
    soup = bs4.BeautifulSoup(html_source, "html5lib")
    if return_soup is True:
        return soup
    else:
        return soup.find_all(text=True)

def get_onlydigits_in_string(s):
    '''
    >>> get_onlystrings_in_string('P0')
    '0'
    >>> 
    '''
    return str(s).translate(None, string.letters)


def get_onlystrings_in_string(s):
    '''
    >>> get_onlystrings_in_string('P0')
    'P'
    >>> 
    '''
    return str(s).translate(None, string.digits)

def string_format(s, *args, **kwargs):
    '''
    Skip KeyError and maintain the string

    >>> s = '{x} {y} {z} {x} {0} {1}'
    >>> d = {'y':'Ifoundy'}
    >>> s.format(**d)
    Traceback (most recent call last):
      File "<interactive input>", line 1, in <module>
    KeyError: 'x'
    >>> print string_format(s, 222222, **d)
    {x} Ifoundy {z} {x} 222222 {1}
    >>> 
    '''
    l = re.split('({.*?})', s)
    w = list()
    for i in l:
        try:
            i = i.format(*args, **kwargs)
        except:
            pass
        w.append(i)

    s = ''.join(w)
    return s

def get_duplicate_in_list(l):
    '''
    >>> get_duplicate_in_list([1,2,3,4,1,1,1,2,2,2])
    [1, 2]
    >>> 
    '''
    dup_arr = l[:]
    for i in set(l):
        dup_arr.remove(i)
    return list(set(dup_arr))

def get_by_val_dict(d, value):
    dups = get_duplicate_in_list(d.values())
    if len(dups) != 0:
        print 'Warning: duplicate values found: {0}'.format(dups)
    return d.keys()[d.values().index(value)]

def prepend_to_ordered_dict(d, key, value):
    items = d.items()
    items.reverse()
    reversed_dict = OrderedDict(items)
    reversed_dict.update({key:value})
    reversed_items = reversed_dict.items()
    reversed_items.reverse()
    original_dict = OrderedDict(reversed_items)
    return original_dict

def get_this_sunday():
    '''
    Returns last sunday as datetime object
    '''
    this_sunday = None
    not_sunday = True
    i = 1
    while not_sunday:
        this_sunday = datetime.date.today() + datetime.timedelta(i)
        if this_sunday.strftime('%A') == 'Sunday':
            break
        i += 1
    return this_sunday

def intersect_lists(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))


def union_lists(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))


def difference_lists(a, b):
    """ show whats in list b which isn't in list a """
    return list(set(b).difference(set(a)))

def get_monday(no_of_wks=1, wk_period='before'):
    '''
    today = 3/14/2013
    get_monday(1, 'after') returns 2013-03-24
    get_monday(1, 'before') returns 2013-03-10
    get_monday(0) returns 2013-03-17
    '''
    today = datetime.datetime.today()
    if wk_period == 'before':
        anyday = today - datetime.timedelta(no_of_wks*7)
    else:
        anyday = today + datetime.timedelta(no_of_wks*7)
    monday = anyday - datetime.timedelta(days=anyday.weekday())
    return monday


def get_sunday(no_of_wks=1, wk_period='before'):
    '''
    today = 3/14/2013
    get_sunday(1, 'after') returns 2013-03-24
    get_sunday(1, 'before') returns 2013-03-10
    get_sunday(0) returns 2013-03-17
    '''
    today = datetime.datetime.today()
    if wk_period == 'before':
        anyday = today - datetime.timedelta(no_of_wks*7)
    else:
        anyday = today + datetime.timedelta(no_of_wks*7)
    sunday = anyday + datetime.timedelta(days=(6-anyday.weekday()))
    return sunday


def date_range(start_date, end_date):
    """
    Returns a generator of all the days between two date objects.
    
    Results include the start and end dates.
    
    Arguments can be either datetime.datetime or date type objects.
    
    h3. Example usage
    
        >>> import datetime
        >>> import calculate
        >>> dr = calculate.date_range(datetime.date(2009,1,1), datetime.date(2009,1,3))
        >>> dr
        <generator object="object" at="at">
        >>> list(dr)
        [datetime.date(2009, 1, 1), datetime.date(2009, 1, 2), datetime.date(2009, 1, 3)]
        
    """
    # If a datetime object gets passed in,
    # change it to a date so we can do comparisons.
    if isinstance(start_date, datetime.datetime):
        start_date = start_date.date()
    if isinstance(end_date, datetime.datetime):
        end_date = end_date.date()
    
    # Verify that the start_date comes after the end_date.
    if start_date > end_date:
        raise ValueError('You provided a start_date that comes after the end_date.')
    
    # Jump forward from the start_date...
    while True:
        yield start_date
        # ... one day at a time ...
        start_date = start_date + datetime.timedelta(days=1)
        # ... until you reach the end date.
        if start_date > end_date:
            break

def unique_list(l):
    '''
    http://stackoverflow.com/questions/7794208/python-remove-duplicate-words-in-string
    
    NOTE: If you use set(), you lose the order
    '''
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist

def read_json(filename):
    """read json and returns dict
    
    @param filename: json filename with path to read
    
    """
    with open(filename) as f:
        jsondata = json.load(f)
    return jsondata


def tree():
    """
    http://recursive-labs.com/blog/2012/05/31/one-line-python-tree-explained/
    https://gist.github.com/hrldcpr/2012250
    """
    return defaultdict(tree)


def scp_remote_to_local(remote_hostname, remote_username, remote_password, remote_file, local_file, logfile):
    logf = open(logfile, 'w')
    scp_cmd = 'scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no {0}@{1}:{2} {3}'.format(remote_username, remote_hostname, remote_file, local_file)
    child = pexpect.spawn(scp_cmd)
    child.logfile = logf
    child.expect('password:|Password:')
    child.sendline(remote_password)
    child.expect('100\%')
    child.close()
    
    tmpfile = logfile + '.tmp'
    with open(tmpfile, 'wt') as fout:
        with open(logfile) as fin:
            for line in fin:
                fout.write(line.replace(remote_password, 'xxx'))
    os.system('mv {0} {1}'.format(tmpfile, logfile))
    


def exec_in_thread(task, jobs, max_threads_per_time, logger=None):
    '''
    @param task: function that will be executed
    @param jobs: args basically must be in nested tuple format
    @param max_threads_per_time: No. of threads per run
    @param logger: logger instance
    
    WARNING: @param task should NOT have thread unsafe objects:
             * pymysql
             * suds
    '''
    for i in range(0, len(jobs), max_threads_per_time):
        jobs_set = jobs[i:i+max_threads_per_time]
        all_threads = list()
        for job in jobs_set:
            j = jobs.index(job)
            msg = ('Accessing job no.: {0}. No. of jobs yet to access: {1}'.format(j+1, len(jobs)-j))
            if not logger:
                print msg
            else:
                logger.log(msg, 'INFO')
            thread = threading.Thread(target=task, args=job)    # , name=job
            all_threads.append(thread)
            thread.start()
        [thread.join() for thread in all_threads]
        
        

def get_git_version():
    return subprocess.check_output(['git', 'describe'], cwd=os.path.dirname(os.path.join(os.getcwd(), __file__)))


def url_fix(s, charset='utf-8'):
    """Sometimes you get an URL by a user that just isn't a real
    URL because it contains unsafe characters like ' ' and so on.  This
    function can fix some of the problems in a similar way browsers
    handle data entered by the user:

    :param charset: The target charset for the URL if the url was
                    given as unicode string.
    
    ref:
    https://github.com/mitsuhiko/werkzeug/blob/master/werkzeug/urls.py
    http://stackoverflow.com/questions/120951/how-can-i-normalize-a-url-in-python
    """
    if isinstance(s, unicode):
        s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.quote(path, '/%')
    qs = urllib.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))


def get_coordinates_geolocation_from_osm(**kwargs):
    """Currently using nominatim.openstreetmap
    @param address {string}: address (e.g: '1670 HENDERSONVILLE RD ASHEVILLE NC')
    @param state {string}: state (e.g: Colorado)
    @param city {string}: city (e.g: Edgewater)
    @param street {string}: street (e.g: 25th ave)
    @param postalcode {string}: zipcode (e.g 80111)
    @param countrycodes {string}: e.g: us
    @param log {method}: log(msg, level) method
    
    Returns Json data (nested list of dict) if found, else []
    """
    address = kwargs.get('address', '')
    state = kwargs.get('state', '')
    city = kwargs.get('city', '')
    street = kwargs.get('street', '')
    countrycodes = kwargs.get('countrycodes', '')
    postalcode = kwargs.get('postalcode', '')
    log = kwargs.get('log')
    host = kwargs.get('host', 'nominatim.openstreetmap.org')
    if (not countrycodes) and (not address):
        msg = 'Invalid input!! Provide either address or countrycode/state/city/etc'
        if log:
            log(msg, 'EXCEPTION')
        else:
            print msg
        return []
    if address:
        opts = 'q={address}'.format(address=address)
    else:
        opts = 'countrycodes={countrycodes}&state={state}&street={street}&postalcode={postalcode}'.format(state=state,
                                                                                  city=city,
                                                                                  street=street,
                                                                                  countrycodes=countrycodes,
                                                                                  postalcode=postalcode)
    url_gisgraphy = 'http://{0}/search.php?{1}&format=json&addressdetails=1&polygon_geojson=1&limit=1'.format(host, opts)
    url_gisgraphy = url_fix(url_gisgraphy)  # returns: 1670+HENDERSONVILLE+RD+ASHEVILLE+NC formated url
    try:
        msg = 'Running OSM qry: {0}'.format(url_gisgraphy)
        log(msg, 'INFO')
        req = urllib2.Request(url_gisgraphy)
        response = urllib2.urlopen(req)
        response_json = json.load(response)
    except Exception as e:
        msg = e
        if 'Connection refused' in str(e):
            ssh_tunnel_cmd = 'ssh -L 2222:128.40.45.204:80 -o ServerAliveInterval=120 -p PORT USER@SSH-SERVER'
            msg = ('Make sure you have a SSH Tunnel to avoid websense. '
                   'Use command like: {0}. Error is: {1}'
                   ''.format(ssh_tunnel_cmd, e))
        elif 'Authorization Required' in str(e):
            msg = ('Websense probably blocking. Error: {0}'
                   ''.format(e))
        if log:
            log(msg, 'EXCEPTION')
        else:
            print msg
        return []
    return response_json


def main():
    pass
    #test
#    print get_sunday(1, 'before'), get_sunday(0), get_sunday(1, 'after')
#    print get_monday(1, 'before'), get_monday(0), get_monday(1, 'after')
#    print get_this_sunday()
#    print '{mon}-{sun} {year}'.format(sun=get_sunday(1).strftime('%m/%d'), mon=get_monday(1).strftime('%m/%d'), year=get_sunday(1).strftime('%Y'))        # e.g - 03/04-03/10 2013
#    print get_onlydigits_in_string('asdas123sfdasd34')

#     username, password = 'foo', 'bar'
#     #cookie_file = r'C:\Temp\cookie.txt'
#     url = 'http://foo'
#     print get_data_from_webpage(url, username=username, password=password)
    
#     x = read_json(r'C:\Temp\auth.json')
#     print type(x), dict(x)
#     t = datetime.datetime.now()
#     print dt_to_epoch('Tue Oct 28 13:00:30 CDT 2014')
#     print dt_to_epoch(t)
    
#     nodes = tree()
#     nodes['src1']
#     nodes['src2']['r1']['r2']
#     r2 = nodes['src1']['r1']['r2']
#     r2['r3']
#     r2['r4']
#     r2['r4']['r5']
#     print json.dumps(nodes)
#     print get_coordinates_geolocation_from_osm(state='Colorado', city='Denver', street='Meadow Creek', countrycodes='us')
#     print get_coordinates_geolocation_from_osm(state='Colorado', city='Denver', street='6900 W 25th Ave', countrycodes='us')
    print get_timediff({'ts':'2015-05-29_10-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'},
                       {'ts':'2015-04-18_19-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'})
    print get_timediff({'ts':'2015-04-19_10-00-01', 'ts_format':'%Y-%m-%d_%H-%M-%S'})


if __name__ == '__main__':
    main()
