'''
Created on Jan 22, 2013

@author: ambarik

Overriding pexpect
'''

# For pexpect
import sys
from pexpect import spawn
from pexpect import types
from pexpect import TIMEOUT
from pexpect import EOF
import re
import sre_constants


class spawn(spawn):
    _PROMPT_CURRENT = list()
    
    def compile_pattern_list(self, patterns):

        """Overriding this method to remove re.DOTALL,
           And catching "sre_constants.error: bad character range"
        """
        
        if patterns is None:
            return []
        if type(patterns) is not types.ListType:
            patterns = [patterns]

        # Replaced source code: "compile_flags = re.DOTALL # Allow dot to match \n" with "compile_flags = 0"
        compile_flags = 0
        if self.ignorecase:
            compile_flags = compile_flags | re.IGNORECASE
        compiled_pattern_list = []
        for p in patterns:
            try:
                if type(p) in types.StringTypes:
                    compiled_pattern_list.append(re.compile(p, compile_flags))
                elif p is EOF:
                    compiled_pattern_list.append(EOF)
                elif p is TIMEOUT:
                    compiled_pattern_list.append(TIMEOUT)
                elif type(p) is type(re.compile('')):
                    compiled_pattern_list.append(p)
                else:
                    raise TypeError ('Argument must be one of StringTypes, EOF, TIMEOUT, SRE_Pattern, or a list of those type. %s' % str(type(p)))
            except sre_constants.error:
                import sys
                sys.stderr.write('Invalid pattern: {0}'.format(p))
                raise

        return compiled_pattern_list

if 'win' in sys.platform:
    from winpexpect import winspawn
    from pexpect import searcher_re
    class spawn(spawn, winspawn):
        def __init__(self, *args, **kwargs):
            """Overriding to avoid setting env, otherwise it will hang"""
            kwargs['env'] = None
            super(spawn, self).__init__(*args, **kwargs)
        
        def expect_list(self, pattern_list, timeout=-1, searchwindowsize=-1):
            '''Overrides to include My_searcher_re'''
            if len(spawn._PROMPT_CURRENT) > 0:
                self.buffer = ''
            return self.expect_loop(My_searcher_re(pattern_list),
                    timeout, searchwindowsize)
            
        def setecho(self, state):
            '''Overrides to do nothing as its not supported'''
            pass
        
        def setwinsize(self, r, c):
            '''Overrides to do nothing as its not supported'''
            pass
        
    class My_searcher_re (searcher_re):
    
        """Overrides search to solve the issue of expecting PROMPT in the incomplete buffer
    
        """
        
        def search(self, buffer, freshlen, searchwindowsize=None):
    
            """Overrides search to match PROMPT at the end. Its expected that
            the prompt should be at the end, else wait for more buffer and return -1"""
            
            absurd_match = len(buffer)
            first_match = absurd_match
            # 'freshlen' doesn't help here -- we cannot predict the
            # length of a match, and the re module provides no help.
            if searchwindowsize is None:
                searchstart = 0
            else:
                searchstart = max(0, len(buffer)-searchwindowsize)
            for index, s in self._searches:
                match = s.search(buffer, searchstart)
                if match is None:
                    continue
                n = match.start()
                if n < first_match:
                    first_match = n
                    the_match = match
                    best_index = index
            if first_match == absurd_match:
                return -1
            ########## MY EDIT STARTS
            if best_index >= 0 and len(spawn._PROMPT_CURRENT) > 0:
                s = self._searches[best_index][1]
                if s.pattern == spawn._PROMPT_CURRENT[-1]:
                    iloc_of_prompt, jloc_of_prompt = list(re.finditer(spawn._PROMPT_CURRENT[-1], buffer))[-1].span()
                    #print 'wait-len...', iloc_of_prompt, jloc_of_prompt, len(buffer)
                    if jloc_of_prompt != len(buffer):
                        # prompt must be last element, therefore wait for more output
                        #print 'wait...', repr(buffer)
                        return -1
                    if iloc_of_prompt <= 1:
                        # Output before prompt cannot this small, therefore wait for more output
                        return -1
                    
            ########## MY EDIT ENDS
            self.start = first_match
            self.match = the_match
            self.end = self.match.end()
            return best_index


def main():
    # Test
    child = spawn('telnet comcast-term-1')
    child.logfile = open(r'C:\tmp\winpexpecttest.log', 'w+')
    print repr(' BEFORE1:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    child.expect('Password:')
    print repr(' BEFORE2:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    child.sendline('cisco')
    
    print repr(' BEFORE3:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    child.expect('>')
    print repr(' BEFORE4:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    child._PROMPT_CURRENT.append('>')
    
    child.sendline('show ip int br')
    child.buffer = ''
    child.expect('>')
    print repr(' BEFORE:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    
    child.sendline('show clock')
    child.buffer = ''
    child.expect('>')
    print repr(' BEFORE:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    
    child.sendline('show hosts')
    child.buffer = ''
    child.expect('>')
    print repr(' BEFORE:' + str(child.before) + ' BUFFER:' + str(child.buffer) + ' AFTER:' + str(child.after))
    
    child.close()

if __name__ == '__main__':
    main()
    