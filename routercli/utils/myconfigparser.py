'''
Overriding ConfigParser for my needs
'''

import sqlite3
import os
from myordereddict import MyOrderedDict
from ConfigParser import ConfigParser
from ConfigParser import RawConfigParser
from ConfigParser import DEFAULTSECT
from ConfigParser import MissingSectionHeaderError
import re

class MyConfigParser(ConfigParser):
    # Copy of ConfigParser
    SEP = ':->'
    OPTCRE = re.compile(
        r'(?P<option>.*[^{sep}])'               # Option - (will not contain the separator)
        r'\s*(?P<vi>{sep})\s*'                  # Separator - any number of space/tab, followed by separator, followed by any # space/tab
        r'(?P<value>.*)$'.format(sep=SEP)       # Value - everything up to eol
        )
    OPTCRE_NV = OPTCRE

    def __init__(self, **kwargs):
        '''
        Overriding to support for no valuev
        '''
        defaults = kwargs.get('defaults')
        dict_type = kwargs.get('dict_type', MyOrderedDict)
        allow_no_value = kwargs.get('allow_no_value', True)

        RawConfigParser.__init__(self, defaults=defaults, dict_type=dict_type,
             allow_no_value=allow_no_value)


    def optionxform(self, optionstr):
        '''
        Overriding to NOT make option lower
        '''
        return optionstr

    def set(self, section, option, value=None):
        '''
        Overriding to make value default to None
        NOTE: Not using super() as RawConfigParser is not new class type 
        '''
        ConfigParser.set(self, section, option, value)

    def get(self, section, option):
        '''
        Overriding to return None by default
        
        Args:
        section, option, raw=False, vars=None
        '''
        
        try:
            return str(self._sections[section][option])
        except KeyError:
            return None

    def write(self, fp):
        """Overriding to support self.SEP."""
        if self._defaults:
            fp.write("[%s]\n" % DEFAULTSECT)
            for (key, value) in self._defaults.items():
                write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                          val=str(value).replace('\n', '\n\t'))
                fp.write(write_line)
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key != "__name__":
                    if value is None:
                        fp.write("%s\n" % (key))
                    else:
                        write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                                  val=str(value).replace('\n', '\n\t'))
                        fp.write(write_line)
            fp.write("\n")

    def _read(self, fp, fpname):
        """Overrides for:
        * Allowing use of my separator self.SEP
        * Allowing empty optval
        """
        cursect = None                            # None, or a dictionary
        optname = None
        lineno = 0
        e = None                                  # None, or an exception
        while True:
            line = fp.readline()
            if not line:
                break
            lineno = lineno + 1
            # comment or blank line?
            if line.strip() == '' or line[0] in '#;':
                continue
            if line.split(None, 1)[0].lower() == 'rem' and line[0] in "rR":
                # no leading whitespace
                continue

            # is it a section header?
            mo = self.SECTCRE.match(line)
            if mo:
                sectname = mo.group('header')
                if sectname in self._sections:
                    cursect = self._sections[sectname]
                elif sectname == DEFAULTSECT:
                    cursect = self._defaults
                else:
                    cursect = self._dict()
                    cursect['__name__'] = sectname
                    self._sections[sectname] = cursect
                # So sections can't start with a continuation line
                optname = None
            # no section header in the file?
            elif cursect is None:
                raise MissingSectionHeaderError(fpname, lineno, line)
            # an option line?
            else:
                mo = self._optcre.match(line)
                if mo:
                    optname, vi, optval = mo.group('option', 'vi', 'value')
                    # This check is fine because the OPTCRE cannot
                    # match if it would set optval to None
                    if optval is not None:
                        if str(vi) in self.SEP and ';' in optval:
                            # ';' is a comment delimiter only if it follows
                            # a spacing character
                            pos = optval.find(';')
                            if pos != -1 and optval[pos-1].isspace():
                                optval = optval[:pos]
                        optval = optval.strip()
                    optname = optname.rstrip()
                    cursect[optname] = optval
                else:
                    optval = None
                    optname = line.rstrip()
                    cursect[optname] = optval

class XConfigParser1(MyConfigParser):

    def __init__(self, **kwargs):
        '''
        TODO: NOT WORKING... as it requires concurency
        This is an enhancement to ConfigParser to allow the following:
        * Ability to save the file in following format:
        ** .ini
        ** .xls
        ** .dbsqlite3
        
        However, this reads the config file only of .ini format (which is unchanged)
        '''
        
        MyConfigParser.__init__(self, **kwargs)
        self.OUTPUTFILE_INI = kwargs.get('OUTPUTFILE_INI')
        self._fileobj_ini = None
        self.OUTPUTFILE_SQL = kwargs.get('OUTPUTFILE_SQL')
        self.SQL_TABLE_NAME = kwargs.get('SQL_TABLE_NAME', 'output')
        self._fileobj_sql = None
        self.OUTPUTFILE_XLS = kwargs.get('OUTPUTFILE_XLS')
        self._fileobj_xls = None
        self.XLS_SHEET_NAME = kwargs.get('XLS_SHEET_NAME', 'output')
    
        if self.OUTPUTFILE_SQL:
            conn = sqlite3.connect(self.OUTPUTFILE_SQL)
            c = conn.cursor()
            c.execute("CREATE TABLE {table} (section text, option text, value text)"
                      "".format(table=self.SQL_TABLE_NAME))
            if self._defaults:
                for (key, value) in self._defaults.items():
                    c.execute("INSERT INTO {table} VALUES ('{section}','{option}','{value}')"
                          "".format(table=self.SQL_TABLE_NAME, section=DEFAULTSECT, 
                                    option=key, value=value))
            for section in self._sections:
                for (key, value) in self._sections[section].items():
                    if key != "__name__":
                        c.execute("INSERT INTO {table} VALUES ('{section}','{option}','{value}')"
                              "".format(table=self.SQL_TABLE_NAME, section=section, 
                                        option=key, value=value))
            conn.commit()
            self._fileobj_sql = conn
                
        if self.OUTPUTFILE_INI:
            fp = open(self.OUTPUTFILE_INI, 'w+')
            if self._defaults:
                fp.write("[%s]\n" % DEFAULTSECT)
                for (key, value) in self._defaults.items():
                    write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                              val=str(value).replace('\n', '\n\t'))
                    fp.write(write_line)
                fp.write("\n")
            for section in self._sections:
                fp.write("[%s]\n" % section)
                for (key, value) in self._sections[section].items():
                    if key != "__name__":
                        if value is None:
                            fp.write("%s\n" % (key))
                        else:
                            write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                                      val=str(value).replace('\n', '\n\t'))
                            fp.write(write_line)
                fp.write("\n")
            self._fileobj_ini = fp
            
    def __del__(self):
        if self._fileobj_ini:
            self._fileobj_ini.close()
        if self._fileobj_sql:
            self._fileobj_sql.close()
        if self._fileobj_xls:
            self._fileobj_xls.close()
            
    def set(self, section, option, value=None):
        '''
        Overriding to save instantly
        NOTE: Not using super() as RawConfigParser is not new class type 
        '''
        ConfigParser.set(self, section, option, value)
        self._write(section, option, value, 'INSERT')

    def _write(self, section, option, value, action):
        if self.OUTPUTFILE_INI:
            fp = self._fileobj_ini
            if fp:
                if action == 'CREATE':
                    fp.write("[%s]\n" % section)
                elif action == 'INSERT':
                    if value is None:
                        fp.write("%s\n" % (option))
                    else:
                        write_line = "{key} {sep} {val}\n".format(key=option, sep=self.SEP, 
                                                                  val=str(value).replace('\n', '\n\t'))
                        fp.write(write_line)
                
        if self.OUTPUTFILE_SQL:
            conn = self._fileobj_sql
            c = conn.cursor()
            if action == 'CREATE':
                c.execute("INSERT INTO {table} VALUES ('{section}','{option}','{value}')"
                          "".format(table=self.SQL_TABLE_NAME, section=section, 
                                    option=option, value=value))
            elif action == 'INSERT':
                c.execute("UPDATE {table} SET option = '{option}', value = '{value}' WHERE section = '{section}'"
                          "".format(table=self.SQL_TABLE_NAME, section=section, 
                                    option=option, value=value))
            conn.commit()
                
    def write(self, fp):
        """Overriding to do nothing, as already I am saving when set() add_section() are called"""
        return
        if self._defaults:
            fp.write("[%s]\n" % DEFAULTSECT)
            for (key, value) in self._defaults.items():
                write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                          val=str(value).replace('\n', '\n\t'))
                fp.write(write_line)
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key != "__name__":
                    if value is None:
                        fp.write("%s\n" % (key))
                    else:
                        write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                                  val=str(value).replace('\n', '\n\t'))
                        fp.write(write_line)
            fp.write("\n")
    
    def add_section(self, section):
        '''
        Overriding to save instantly
        NOTE: Not using super() as RawConfigParser is not new class type 
        '''
        ConfigParser.add_section(self, section)
        self._write(section, '', '', 'CREATE')

class XConfigParser(MyConfigParser):

    def __init__(self, **kwargs):
        '''
        This is an enhancement to ConfigParser to allow the following:
        * Ability to save the file in following format:
        ** .ini
        ** .xls
        ** .dbsqlite3
        
        However, this reads the config file only of .ini format (which is unchanged)
        '''
        
        MyConfigParser.__init__(self, **kwargs)
        self.OUTPUTFILE_INI = kwargs.get('OUTPUTFILE_INI')
        self._output_ini_closed = False
        self.OUTPUTFILE_SQL = kwargs.get('OUTPUTFILE_SQL')
        self._output_sql_closed = False
        self.SQL_TABLE_NAME = kwargs.get('SQL_TABLE_NAME', 'output')
        self.OUTPUTFILE_XLS = kwargs.get('OUTPUTFILE_XLS')
        self.XLS_SHEET_NAME = kwargs.get('XLS_SHEET_NAME', 'output')

    def write_to_all(self):
        """Overriding to allow multiple savings"""
        if self.OUTPUTFILE_INI and not self._output_ini_closed:
            with open(self.OUTPUTFILE_INI, 'w+') as fp:
                if self._defaults:
                    fp.write("[%s]\n" % DEFAULTSECT)
                    for (key, value) in self._defaults.items():
                        write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                                  val=str(value).replace('\n', '\n\t'))
                        fp.write(write_line)
                    fp.write("\n")
                for section in self._sections:
                    fp.write("[%s]\n" % section)
                    for (key, value) in self._sections[section].items():
                        if key != "__name__":
                            if value is None:
                                fp.write("%s\n" % (key))
                            else:
                                write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, 
                                                                          val=str(value).replace('\n', '\n\t'))
                                fp.write(write_line)
                    fp.write("\n")
                    self._output_ini_closed = True
    
        if self.OUTPUTFILE_SQL and not self._output_sql_closed:
            if os.path.isfile(self.OUTPUTFILE_SQL):
                os.remove(self.OUTPUTFILE_SQL)
            conn = sqlite3.connect(self.OUTPUTFILE_SQL)
            c = conn.cursor()
            c.execute("CREATE TABLE {table} (section text, option text, value text)"
                      "".format(table=self.SQL_TABLE_NAME))
            if self._defaults:
                for (key, value) in self._defaults.items():
                    c.execute('INSERT INTO ' + self.SQL_TABLE_NAME +  ' VALUES (?, ?, ?);', (str(DEFAULTSECT), str(key), str(value)))
            for section in self._sections:
                for (key, value) in self._sections[section].items():
                    if key != "__name__":
                        c.execute('INSERT INTO ' + self.SQL_TABLE_NAME +  ' VALUES (?, ?, ?);', (str(section), str(key), str(value)))
            conn.commit()
            self._fileobj_sql = conn
            conn.close()
            self._output_sql_closed = True
    
    def __del__(self):
        self.write_to_all()
        
def test_write_n_read():
    #test
    cfg_file = r'/tmp/routercli.ini'
    config = MyConfigParser()
    section_name1 = 'Device1'
    cmd1 = '    show run1'
    config.add_section(section_name1)
    config.set(section_name1, cmd1)
    section_name2 = 'Device2'
    cmd2 = 'cmd1'
    config.add_section(section_name2)
    config.set(section_name2, cmd2, 'test2')
    
    print config.get('Device2', 'cmd2')
    #print config.get('Device2', 'asdasdas')
    config.set('Device2', 'cmd2', 'test2')
    config.set('Device2', 'cmd2', 'test2')
    print config._sections
    with open(cfg_file, 'w+') as configfile:
        config.write(configfile)
    
    config = MyConfigParser()
    config.readfp(open(cfg_file))
    print config._sections[section_name1][cmd1]
    print config._sections[section_name2][cmd2]
    #print config._sections
    
def test_XConfigParser():
    cfg_ini_file = r'/tmp/routercli-out-test.ini'
    cfg_sql_file = r'/tmp/routercli-out-test.db'
    cfg_xls_file = r'/tmp/routercli-out-test.xls'
    try:
        os.remove(cfg_ini_file); os.remove(cfg_sql_file);
    except:
        pass 
    config = XConfigParser(OUTPUTFILE_INI=cfg_ini_file, OUTPUTFILE_SQL=cfg_sql_file)
    section_name1 = 'Device1'
    cmd1 = 'show run1'
    config.add_section(section_name1)
    config.set(section_name1, cmd1, 'fooo....')
    config.write_to_all()

def test_read():
    cfg_file = '/tmp/routerclilogs/routercli-out.ini'
    config = MyConfigParser()
    config.readfp(open(cfg_file))
    print config._sections
    for section in config._sections:
        for option in config._sections[section]:
            print 'section: {0}, option: {1}, value: {2}'.format(repr(section), repr(option), repr(config._sections[section][option]))
    
def main():
    #test_write_n_read()
    test_XConfigParser()
    #test_read()
    

if __name__ == '__main__':
    main()

