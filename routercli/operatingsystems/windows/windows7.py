import subprocess

RSA_TOKEN_AHK_DATA = '''
;; RSA Token Automation.ahk
;; # autohotkey.exe
;; # Ref: http://www.autohotkey.com/board/topic/59612-simple-debug-console-output/
DebugMessage(str)
{
 global h_stdout
 DebugConsoleInitialize()  ; start console window if not yet started
 str .= "`n" ; add line feed
 DllCall("WriteFile", "uint", h_Stdout, "uint", &str, "uint", StrLen(str), "uint*", BytesWritten, "uint", NULL) ; write into the console
 WinSet, Bottom,, ahk_id %h_stout%  ; keep console on bottom
}

DebugConsoleInitialize()
{
   global h_Stdout     ; Handle for console
   static is_open = 0  ; toogle whether opened before
   if (is_open = 1)     ; yes, so don't open again
     return
     
   is_open := 1    
   ; two calls to open, no error check (it's debug, so you know what you are doing)
   DllCall("AttachConsole", int, -1, int)
   DllCall("AllocConsole", int)

   dllcall("SetConsoleTitle", "str","Paddy Debug Console")    ; Set the name. Example. Probably could use a_scriptname here 
   h_Stdout := DllCall("GetStdHandle", "int", -11) ; get the handle
   WinSet, Bottom,, ahk_id %h_stout%      ; make sure it's on the bottom
   WinActivate,Lightroom   ; Application specific; I need to make sure this application is running in the foreground. YMMV
   return
}

;; RSA
Run, "C:\Program Files (x86)\RSA SecurID Software Token\SecurID.exe"
;; TokenName = 1234567890
WinWait, 1234567890 - RSA SecurID Token, 
IfWinNotActive, 1234567890 - RSA SecurID Token, , WinActivate, 1234567890 - RSA SecurID Token, 
WinWaitActive, 1234567890 - RSA SecurID Token, 
Send, 19811981
Sleep, 200
Send, {ENTER}
Sleep, 200
;;Send, {CTRLDOWN}c{CTRLUP}

Send, ^c
Send, ^c
Sleep, 200
X := clipboard
DebugMessage(X)
Send, {ALTDOWN}{F4}{ALTUP}
return
'''

class Windows7():
    def __init__(self, **kwargs):
        self.logger = kwargs.get('logger')

    def get_rsa_token(self):
        # This assume your windowsxp has AutoHotkey.exe installed and in the PATH
        self._log("Getting RSA Token")
        tmp_file = '/tmp/RSATokenAutomation.ahk'
        with open(tmp_file, 'w+') as f:
            f.write(RSA_TOKEN_AHK_DATA)
        return subprocess.Popen("AutoHotkey " + tmp_file, shell=True, stdout=subprocess.PIPE).communicate()[0].strip()

    def _log(self, msg, **kwargs):
        if self.logger is None:
            print msg
        else:
            self.logger.log(msg, **kwargs)

def main():
    x = Windows7()
    print x.get_rsa_token()

if __name__ == '__main__':
    main()
