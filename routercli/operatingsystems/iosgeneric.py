'''
Created on Apr 9, 2013

@author: ambarik
'''

import re
from time import strptime
import datetime
from routercli.router import Router
from collections import Counter
from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS

class IOSGeneric(Router):
    def short_to_full_intf_name(self, short_intf_name):
        # TODO: POS, HundredGig
        port = short_intf_name
        port_full_name = port
        if 'Gi' == port[:2]:    # IOS, XR
            port_full_name = 'GigabitEthernet' + port[2:]
        elif 'Eth' == port[:3]: # IOS
            port_full_name = 'Ethernet' + port[3:]
        elif 'Te' == port[:2]:  # IOS, XR
            if self.os_name == OS_NAME_IOS_XR:
                port_full_name = 'TenGigE' + port[2:]
            elif self.os_name == OS_NAME_IOS:
                port_full_name = 'TenGigabitEthernet' + port[2:]
        elif 'Po' == port[:2]:  # IOS, NXOS
            port_full_name = 'Port-channel' + port[2:]
        elif 'Lo' == port[:2]:  # IOS, NXOS, XR
            port_full_name = 'Loopback' + port[2:]
        elif 'BE' == port[:2]:  # XR
            port_full_name = 'Bundle-Ether' + port[2:]
        elif 'Mg' == port[:2]:  # XR
            port_full_name = 'MgmtEth' + port[2:]
        elif 'Nu' == port[:2]:  # XR
            port_full_name = 'Null' + port[2:]
        elif 'Fa' == port[:2]:
            port_full_name = 'FastEthernet' + port[2:]
        return port_full_name
    
    def get_dir(self, path, **kwargs):
        '''
        IOS>show fl
        -#- --length-- -----date/time------ path
        1     56499796 Jan 22 2009 15:21:04 c2800nm-advipservicesk9-mz.124-22.T.bin
        2         1646 Dec 27 2005 16:14:40 sdmconfig-2811.cfg
        
        RP/0/RSP0/CPU0:IOSXR#dir disk0:
        Fri Jun 14 15:51:38.472 UTC
        
        Directory of disk0:
        
        7474        -r--  476668      Thu Apr 18 22:08:52 2013  .bitmap
        
        Returns list of files in the directory
        '''
        cmd = kwargs.get('cmd', 'show {0}'.format(path))
        cmd_out = self.exec_cmd(cmd)
        list_of_files = re.findall('\d+\s+[drwx-]+\s+\d+\s+\w+\s\w+\s\d\d\s\d\d:\d\d:\d\d\s\d+\s+(.*)', cmd_out, re.MULTILINE)
        if kwargs.get('store_val', False) is True:
            self.cce_obj.configout.set(self.hostname, 'list_of_files_in_dir_{0}'.format(path), list_of_files)
        return list_of_files
        
    def get_current_time(self):
        '''
        18:46:47.821 UTC Mon May 20 2013
        
        Returns datetime object
        '''
        cmd = 'show clock'
        out = self.exec_cmd(cmd)
        clock_format = '(?P<hour>\d\d)\:(?P<minute>\d\d)\:(?P<second>\d\d)\.(?P<microsecond>\d+) (?P<tzone>\w+) (?P<wday>\w+) (?P<month>\w+) (?P<day>\d+) (?P<year>\d\d\d\d)'
        found_clock = re.search(clock_format, out)
        if found_clock is None:
            return found_clock
        else:
            kwargs_clock = found_clock.groupdict()
            return self.iostime_to_datetime(iostime_kwargs=kwargs_clock)
        
    def syslog_present(self, **kwargs):
        msg = kwargs.get('msg')
        report = False
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        msgs = msg.split('|')
        for single_msg in msgs:
            if single_msg in out:
                report = True
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def check_for_high_cpu(self, **kwargs):
        report = False
        msg = kwargs.get('msg')
        th = kwargs.get('th', 50)   # Return True if > 50% Process CPU utilized
        cmd = kwargs.get('cmd', 'show process cpu | i CPU utilization')
        show_cpu = self.exec_cmd(cmd, auto_complete=False, privileged_exec_mode=True)
        current_cpu = self.get_proc_cpu_utilized(show_cpu)
        if current_cpu is not None:
            if current_cpu > th:
                report = True
                self.cce_obj.configout.set(self.hostname, '{0}_cce_cpu_high'.format(msg), current_cpu)
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def get_proc_cpu_utilized(self, show_proc_cpu, **kwargs):
        found_cpu = re.search('CPU utilization for one minute: (\d+)%;', show_proc_cpu)
        if found_cpu is not None:
            return float(found_cpu.group(1))
        return None
    
    def get_bgp_idle_neighbors(self, show_bgp_summary_cmd):
        '''
        On IOS, show bgp all summary
        On IOS-XR, show bgp all all summary
        
        Arguments:
        show_bgp_summary_out
        
        Returns list
        '''
        
        idle_neighbors = list()
        show_bgp_summary = self.exec_cmd(show_bgp_summary_cmd)
        found_ipv4_outs = re.findall('\d+\.\d+\.\d+\.\d+.*?Idle|\d+\.\d+\.\d+\.\d+.*?Closing', show_bgp_summary, re.DOTALL)
        for found in found_ipv4_outs:
            if found.count('.') == 4 and '::' not in found:
                idle_neighbors.append(re.search('(\d+\.\d+\.\d+\.\d+)\s+', found).group())
                
        found_ipv6_outs = re.findall('\w+\:+.*?Idle|\w+\:+.*?Closing', show_bgp_summary, re.DOTALL)
        for found in found_ipv6_outs:
            if found.count('::') == 1 and '.' not in found:
                idle_neighbors.append(re.search('(\w+\:+.*?)\s+', found).group())
                
        
        return list(set(idle_neighbors))
    
    def msg_BGP_NOTIFICATION(self, **kwargs):
        '''
        Args:
        msg
        
        Report if BGP session is down
        Oct  1 06:34:38.430 utc: %BGP-3-NOTIFICATION: sent to neighbor 2001:558:340::216 4/0 (hold time expired) 0 bytes 
        Oct  1 06:34:55.838 utc: %BGP-3-NOTIFICATION: sent to neighbor 68.85.4.30 4/0 (hold time expired) 0 bytes
        '''
        report = False
        msg = kwargs.get('msg')
        show_bgp_summary_cmd = kwargs.get('show_bgp_summary_cmd')
        
        show_log = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        neighbors_found_down_v4 = list(set(re.findall('neighbor (\d+\.\d+\.\d+\.\d+)\s+', show_log)))
        neighbors_found_down_v6 = list(set(re.findall('neighbor (\w+\:+.*?)\s+', show_log)))
        
        idle_neighbors = self.get_bgp_idle_neighbors(show_bgp_summary_cmd=show_bgp_summary_cmd)
        
        bgp_neighbors_down1 = list(set(neighbors_found_down_v4) and set(idle_neighbors))
        bgp_neighbors_down2 = list(set(neighbors_found_down_v6) and set(idle_neighbors))
        bgp_neighbors_down = bgp_neighbors_down1 + bgp_neighbors_down2
        
        if len(bgp_neighbors_down) >= 1:
            report = True
            nsr_down_msg = 'BGP Neighbors down: {0}'.format(list(set(bgp_neighbors_down)))
            self.cce_obj.configout.set(self.hostname, 'bgp_neighbors_down', nsr_down_msg)
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    
    def get_show_log(self, **kwargs):
        self.log('Getting ')
        msg = kwargs.get('msg', self.msg)
        if msg is not None:
            self.show_log = self.router.exec_cmd('show logging | include {0}'.format(msg))
            self.msg = msg
            return self.show_log
        
    def get_frequency_of_msg(self, **kwargs):
        '''
        Returns the no. of messages seen as per the frequency
        
        Arguments:
        msg
        frequency_unit    -- 'perday'
                             'persecond'
        
        Msg:
        '*May 23 15:13:18.611: %CLNS-4-AUTH_FAIL: ISIS: Serial IIH authentication failed'
        '''
        msg = kwargs.get('msg', self.msg)
        frequency_unit = kwargs.get('frequency_unit', 'perday')
        self.log('Getting frequency in {0} for msg: {1}'.format(frequency_unit, msg))
        self.get_show_log(**kwargs)
        
        time_format = '(?P<month>\w+) (?P<day>\d\d) (?P<hour>\d\d)\:(?P<minute>\d\d)\:(?P<second>\d\d)\.(?P<microsecond>\d+)\s?:'
        timestamps = re.findall(time_format, self.show_log, re.MULTILINE)
        if frequency_unit == 'permonth':
            per_item_frequency = Counter(sublist[0] for sublist in timestamps)
        elif frequency_unit == 'perday':
            per_item_frequency = Counter(sublist[1] for sublist in timestamps)
        
        if len(per_item_frequency) == 0:
            frequency = 0
        else:
            frequency = sum(per_item_frequency.values())/len(per_item_frequency.values())
        return frequency
    
    def cce_report_msg(self, msg, **kwargs):
        ''' Take Troubleshoot steps to find out if message needs to be reported or not
        
        This executes a method by the name of syslog message
        with all _
        
        '''
        
        self.msg = msg
        self.get_show_log()
        method = self.msg.replace('-', '_')
        py_code = 'self.{method}(**kwargs)'.format(method=method)
        return_code = False
        try:
            return_code = eval('py_code')
        except Exception as e:
            msg = 'Syslog Message: {0} not a method'.format(e)
            self.log(msg, 'EXCEPTION')
            from routercli.wrapper import CFG_OUT_ERROR_SEC
            self.router.cce_obj.configout.set(CFG_OUT_ERROR_SEC, 'Error while executing Syslog.tshoot {0} for {1}'.format(repr(py_code), self.router.hostname), repr(msg))
            self.router.cce_obj._failed_device_list.append(self.router.hostname)
            return_code = True
        finally:
            self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(self.msg), str(return_code))
    
    def iostime_to_datetime(self, iostime_kwargs, **kwargs):
        '''
        @param iostime_kwargs: Accepts dictionary with keys that 
                                must be same as that accepted by datetime
                                e.g: iostime_kwargs = {'month':'Jun', 'year': 2014, ...}
        @param month_format: Month format to be converted to
                             (Defaults to: '%b'). e.i month from {'month':'Jun'} gets converted to 6
        
        Returns datetime.datetime() object
        '''
        
        month_format = kwargs.get('month_format', '%b')
        if 'month' in iostime_kwargs.keys():
            iostime_kwargs['month'] = int(strptime(iostime_kwargs['month'], month_format).tm_mon)
        k = {i:int(iostime_kwargs[i]) for i in iostime_kwargs if str(iostime_kwargs[i]).isdigit()}
        return datetime.datetime(**k)
    
    def get_config_register(self, **kwargs):
        '''
        @param report: If True, save to report, else don't
                       (Defaults to None)
        @param match_pattern: String pattern that needs to be matched 
                              to find out Config Register in show version output
                              (Defaults to 'Configuration register.*' for IOS/IOSXR)
                              
        Returns config_register as string if found, else return None
        '''
        
        report = kwargs.get('report')
        match_pattern = kwargs.get('match_pattern', 'Configuration register.*')
        config_register = None
        o = None
        if self.show_version_out:
            o = self.show_version_out
        else:
            o = self.show_version()
        r = re.search(match_pattern, o, re.IGNORECASE)
        if r:
            config_register = r.group()
            
        if report:
            self.cce_obj.configout.set(self.hostname, 'config_register', repr(config_register))
        return config_register
    