import re

from routercli.operatingsystems.iosgeneric import IOSGeneric

class NXOS(IOSGeneric):
    def show_vdc(self, **kwargs):
        o = self.exec_cmd('show vdc')
        return o
    
    