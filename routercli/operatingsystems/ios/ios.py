'''
Created on Mar 22, 2013

@author: ambarik
'''

SHOW_VERSION = 'Cisco IOS Software'

from routercli.operatingsystems.iosgeneric import IOSGeneric
import re


class IOS(IOSGeneric):
    def check_for_high_cpu(self, cmd='show process cpu | e   0.00%', **kwargs):
        return IOSGeneric.check_for_high_cpu(self, cmd=cmd, **kwargs)
    
    def check_for_high_proc_mem(self, **kwargs):
        report = False
        msg = kwargs.get('msg')
        th = kwargs.get('th', 50)   # Return True if > 50% Process Memory utilized
        cmd = kwargs.get('cmd', 'show processes memory sorted')
        show_proc_memory = self.exec_cmd(cmd, auto_complete=False, privileged_exec_mode=True)
        mem_utilized = self.get_proc_mem_utilized(show_proc_memory)
        if mem_utilized is not None:
            if mem_utilized > th:
                report = True
                self.cce_obj.configout.set(self.hostname, '{0}_cce_proc_memory_high'.format(msg), mem_utilized)
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def get_proc_mem_utilized(self, show_proc_memory, **kwargs):
        found_mem = re.search('Processor Pool Total:\s+?(?P<total>\d+)\s+Used:\s+(?P<used>\d+)\s+Free:\s+(?P<free>\d+)', show_proc_memory)
        if found_mem is not None:
            return (float(found_mem.groupdict()['used'])/float(found_mem.groupdict()['total']))*(float(100))
        return None
    
    def msg_BGP_NOTIFICATION(self, **kwargs):
        '''
        Args:
        msg
        
        Report if BGP session is down
        Oct  1 06:34:38.430 utc: %BGP-3-NOTIFICATION: sent to neighbor 2001:558:340::216 4/0 (hold time expired) 0 bytes 
        Oct  1 06:34:55.838 utc: %BGP-3-NOTIFICATION: sent to neighbor 68.85.4.30 4/0 (hold time expired) 0 bytes
        '''
        
        msg = kwargs.get('msg')
        show_bgp_summary_cmd = 'show bgp all summary'
        return IOSGeneric.msg_BGP_NOTIFICATION(self, show_bgp_summary_cmd=show_bgp_summary_cmd, msg=msg)
        