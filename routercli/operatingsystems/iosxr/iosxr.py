'''
Created on Mar 22, 2013

@author: ambarik
'''

import re

from routercli.operatingsystems.iosgeneric import IOSGeneric

MSG_LOC_PATT = '\w+/(\d/\d+/\w+):' # RP/0/RSP0/CPU0:Apr 25 20:07:08.720 :

class IOSXR(IOSGeneric):
    def get_bundle_assoc_intfs(self, bundle_intf):
        '''get asscoiated interfaces
        '''
        self.log('bundle found... getting individual int')
        o = self.exec_cmd('show interface {0}'.format(bundle_intf))
        bintfs = list()
        try:
            o = o[o.find('No. of members in this bundle:'):]
            ints_in_bundle = re.search('No. of members in this bundle:\s+(\d+)', o).group(1)
            o = o.splitlines()[1:int(ints_in_bundle)+1]
            bintfs = [bintf.strip().split(' ')[0] for bintf in o]
        except Exception as e:
            msg = 'err while getting assc intf for bundle {0}, err: {1}'.format(bundle_intf, e)
            self.log(msg, 'EXCEPTION')
        return bintfs
    
    def ccecode_syslog_present(self, msg):
        '''
        Report if message is seen and save the location seen in msg
        '''
        report = False
        out = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        msgs = msg.split('|')
        for single_msg in msgs:
            if single_msg in out:
                report = True
        
        if report is True:
            locations = list(set(re.findall('\w+/(\d/\d+/\w+):', out)))      # out=LC/0/14/CPU0:, get 0/14/CPU0
            cce_msg_location = '. Modules:' + ','.join(locations)
            self.cce_obj.configout.set(self.hostname, '{0}_location'.format(msg), cce_msg_location)
        
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
        
    def ccecode_card_reboot_syslog(self, **kwargs):
        '''
        Args:
        msg
        locations
        
        Report if syslog message found in last reboot of the card
        '''
        report = False
        msg = kwargs.get('msg')
        locations = kwargs.get('locations')
        show_log = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        
        if locations is None:
            locations = list(set(re.findall('\w+/(\d/\d+/\w+):', show_log)))      # out=LC/0/14/CPU0:, get 0/14/CPU0
        cce_card_resetting = list()
        for location in locations:
            last_syslog = self.exec_cmd('admin show reboot last syslog location {0}'.format(location))
            if 'No crashinfo data present' not in last_syslog:
                if len(set(show_log.split('\n')) & set(last_syslog.split('\n'))) >= 1:
                    self.exec_cmd('admin show reboot history reverse location {0}'.format(location))
                    cce_card_resetting.append(location)
                    report = True
        cce_card_resetting = ','.join(cce_card_resetting)
        if cce_card_resetting != str():
            cce_card_resetting = 'Modules Resetting: {0}'.format(cce_card_resetting)
            self.cce_obj.configout.set(self.hostname, 'cce_card_resetting', cce_card_resetting)
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def check_for_high_cpu(self, cmd='show process cpu | e       0%', **kwargs):
        return IOSGeneric.check_for_high_cpu(self, cmd=cmd, **kwargs)
    
    def ccecode_check_fan(self, msg):
        '''Report if msg not seen
        '''
        report = False
        if msg.split('|')[0] in self.exec_cmd('show logging | include {0}'.format(msg)):
            if 'FAN_FAIL' in msg:
                out = self.exec_cmd('show logging | include PLATFORM-ENVMON-2-FAN_CLEAR')
                if 'FAN_CLEAR' not in out:
                    report = True
            elif 'FANTRAY_FAIL' in msg:
                out = self.exec_cmd('show logging | include PLATFORM-ENVMON-2-FANTRAY_CLEAR')
                if 'FANTRAY_CLEAR' not in out:
                    report = True
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        self.exec_cmd('admin show environment fans')
        return report
    
    def ccecode_check_redundancy(self, msg):
        # Report if no redundancy in RSP or RSP not in IOS XR
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_check_redundancy')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            out = self.exec_cmd('admin show redundancy summary')
            if (re.search('(RSP\d.*?\s+.*?RSP\d)|(RP\d.*?\s+.*?RP\d)', out) is None or
                re.search('Active.*?\s+.*?Standby', out) is None):
                report = True
                self.log('Possibly RSP redundancy not present')
            if re.search('Ready.*?\s+.*?Not Configured', out) is None:
                if re.search('Ready.*?\s+.*?Ready', out) is None:
                    report = True
                if re.search('not', out, re.IGNORECASE) is not None:
                    report = True   # NSR: Not Ready
            self.cce_obj.configout.set(self.hostname, 'ccecode_check_redundancy', report)
        
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def ccecode_reset_state(self, msg):
        # Report if in RESET state
        already_checked = self.cce_obj.configout.get(self.hostname, 'ccecode_reset_state')
        if already_checked is not None:
            report = eval(already_checked)
        else:
            report = False
            not_in_iosxr = self.ccecode_check_platform()
            if not_in_iosxr is True:
                states = self.cce_obj.configout.get(self.hostname, 'cce_not_ios_xr_state')
                node = re.search('Node: (.*?) in state: .*?RESET', states)
                if node is not None:
                    self.cce_obj.configout.set(self.hostname, '{0}_cce_node'.format(msg), node)
                    report = True
            else:
                out = self.exec_cmd('admin show platform')
                l = re.findall('(\w+/\w+/\w+).*\s+RESET', out)
                node = ','.join(l)
                if len(l) != 0:
                    self.log('Module in RESET state')
                    report = True
                self.cce_obj.configout.set(self.hostname, '{0}_cce_node'.format(msg), node)
            self.cce_obj.configout.set(self.hostname, 'ccecode_reset_state', report)
        
        # Msg specific
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def get_interfaces_info(self, **kwargs):
        '''
        Arguments:
        list_of_interfaces : list of interface names
        
        Returns a nested dictionary:
        {'Loopback0': {'ipv4 address':'1.1.1.1', 'ipv6 address':'1::1', 'Status':'down', 'Line Protocol':'down'}}
        '''
        
        list_of_interfaces = kwargs.get('list_of_interfaces', [])
        interfaces = "|".join(list_of_interfaces)
        if len(list_of_interfaces) > 0:
            cmd1 = 'show ipv4 vrf all interface brief | include "{0}"'.format(interfaces)
            cmd2 = 'show ipv6 vrf all interface brief | include "{0}"'.format(interfaces)
        else:
            cmd1 = 'show ip vrf all interface brief'
            cmd2 = 'show ipv6 vrf all interface brief'
            
        show_ip_int_br = self.exec_cmd(cmd1)
        show_ipv6_int_br = self.exec_cmd(cmd2)
        #print repr(show_ipv6_int_br)
        return_data = dict()
        
        show_ip_int_br_data = re.findall('(.*?)\s+(\d+.*?|unassigned)\s+(Up|Down|Shutdown)\s+(Up|Down)', show_ip_int_br, re.MULTILINE)
        if len(show_ip_int_br_data) > 0:
            for interface_name, ipv4_address, status, line_protocol in show_ip_int_br_data:
                d = {'ipv4 address':ipv4_address, 'Status':status.lower(), 'Line Protocol':line_protocol.lower(), 'ipv6 address':None}
                return_data[interface_name] = d
        show_ipv6_int_br_data_name = re.findall('(.*?)\s+\[.*?\]', show_ipv6_int_br, re.MULTILINE)
        if ':' in show_ipv6_int_br_data_name[0]:
            show_ipv6_int_br_data_name.pop(0)        # Remove the Time
        show_ipv6_int_br_data_add = re.findall('(unassigned|\s\d+\:.*?)\s+', show_ipv6_int_br, re.MULTILINE)
        if '.' in show_ipv6_int_br_data_add[0]:
            show_ipv6_int_br_data_add.pop(0)        # Remove the Time
        show_ipv6_int_br_data = zip(show_ipv6_int_br_data_name, show_ipv6_int_br_data_add)
        print show_ipv6_int_br_data
        if len(show_ipv6_int_br_data) > 0:
            for interface_name, ipv6_address in show_ipv6_int_br_data:
                return_data[interface_name]['ipv6 address'] = ipv6_address.strip()
                
        return return_data
    
        
    def ccecode_BGP_NBR_NSR_DISABLED(self, **kwargs):
        '''
        Args:
        msg
        
        Report if NSR is disabled
        RP/0/RP0/CPU0:Sep 16 19:11:07.902 UTC: bgp[1045]: %ROUTING-BGP-3-NBR_NSR_DISABLED : NSR disabled on neighbor 107.1.77.38 due to TCP retransmissions 
        '''
        report = False
        msg = kwargs.get('msg')
        
        show_log = self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        neighbors_found_down_v4 = list(set(re.findall('neighbor (\d+\.\d+\.\d+\.\d+)\s+', show_log)))
        neighbors_found_down_v6 = list(set(re.findall('neighbor (\w+\:+.*?)\s+', show_log)))
        
        show_bgp_sessions = self.exec_cmd('show bgp sessions')
        show_bgp_sessions_standby = self.exec_cmd('show bgp sessions standby')
        
        nsr_down = list()
        for neighbor in neighbors_found_down_v4:
            if re.search('{0}\s+.*?(NSR Ready)'.format(neighbor), show_bgp_sessions) is None:
                nsr_down.append(neighbor)
                report = True
            if re.search('{0}\s+.*?(NSR Ready)'.format(neighbor), show_bgp_sessions_standby) is None:
                nsr_down.append(neighbor)
                report = True
                
        for neighbor in neighbors_found_down_v6:
            list_string = [show_bgp_sessions, show_bgp_sessions_standby]
            for string_out in list_string:
                found = re.search('{0}\s+.*?(NSR Ready)'.format(neighbor), string_out, re.DOTALL)  # To account for a newline
                if found is not None:
                    if found.group().count('::') == 1:      # There must be one neighbor captured only by the regex
                        nsr_down.append(neighbor)
                        report = True
            
        if len(nsr_down) >= 1:
            nsr_down_msg = 'BGP Neighbors with NSR down: {0}'.format(list(set(nsr_down)))
            self.cce_obj.configout.set(self.hostname, 'nsr_down', nsr_down_msg)
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    
    def msg_BGP_NOTIFICATION(self, **kwargs):
        '''
        Args:
        msg
        
        Report if BGP session is down
        Oct  1 06:34:38.430 utc: %BGP-3-NOTIFICATION: sent to neighbor 2001:558:340::216 4/0 (hold time expired) 0 bytes 
        Oct  1 06:34:55.838 utc: %BGP-3-NOTIFICATION: sent to neighbor 68.85.4.30 4/0 (hold time expired) 0 bytes
        '''
        
        msg = kwargs.get('msg')
        show_bgp_summary_cmd = 'show bgp all all summary'
        return IOSGeneric.msg_BGP_NOTIFICATION(self, show_bgp_summary_cmd=show_bgp_summary_cmd, msg=msg)
        
    
    def ccecode_BGP_QAD_TIMEOUT(self, **kwargs):
        '''
        Args:
        msg
        
        Report if run nsr_qad_ping is not successful
        
        For: ROUTING-BGP-3-QAD_TIMEOUT
        '''
        report = False
        msg = kwargs.get('msg')
        
        self.exec_cmd('show logging | include {msg}'.format(msg=msg))
        ping_out = self.exec_cmd('run nsr_qad_ping')
        
        if '100 percent' not in ping_out:
            report = True
            self.cce_obj.configout.set(self.hostname, 'run nsr_qad_ping', repr(ping_out))
        self.cce_obj.configout.set(self.hostname, '{0}_cce_report'.format(msg), str(report))
        return report
    