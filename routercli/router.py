'''
@author: wilson.amit@gmail.com
@copyright: Copyright (c) 2013 Amit Barik (wilson.amit@gmail.com)
@version: 1.0.0
@summary: 
Router Class used for interacting with a Cisco Router/Switch running IOS or XR or NX-OS

e.g:
>>> #! /usr/bin/env python
...
>>> import Router
>>> r1 = Router.Router('10.0.0.1')
>>> r1.login(username='cisco', password='cisco')
>>> r1.exec_cmd('show ip int br | i Lo')
'Loopback0                  10.1.1.3        YES NVRAM  up                    up'
>>> r1.logout()
>>> 
'''

#
# built-in related imports
#
import sys
import os
from StringIO import StringIO
import re
import time
import string

#
# routercli related imports
#
from routercli import OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS, OS_NAMES_ALL, OS_NAME_ARRIS_CMTS, OS_NAME_JUNOS, OS_NAME_SCREENOS, OS_NAME_LINUX0
from MODEL_NAMES import *
from utils import mypexpect
from routercli.mcast.mcast import Mcast
from pexpect import EOF         # For pexpect
from routercli.unicast.unicast import Unicast
from routercli.l2.l2 import Layer2
from routercli.isis.isis import Isis
from routercli.bgp.bgp import Bgp

# Import routercli package
from utils.logger import MyLogger


DEFAULT_LOG_DIR = '/tmp/'
LOGGER_NAME = 'routercli.Router'
SCRIPT_LOG = 'Script Logs.log'

# Sample IOS, JUNOS: hostname>
# Sample IOS, NXOS, IOSXR: hostname#
# Sample SCREENOS: hostname(B)->
# Sample LINUX: user@hostname:~/path$
PATTERN_EOL = r'(?P<eol>\(\w\)?->|>|#|\$)(\s+)?$'

# No White Spaces allowed in hostname
## First paranethesis contains all possible characters seen right in between prompt's left most 1st char and previous line
### \x1b[K is ESC Char seen when router is logged in via TTY line of term-serv (particularly for XR)
### hostname cannot have characters: > and #
### GENERIC_CISCO_PROMPT matches OS_NAME_ARRIS_CMTS also
GENERIC_CISCO_PROMPT = '(\n|\r|\x1b\[K)(?P<iosxr>\w+/[0-9]/\w+/\w+:)?(?P<hostname>[^\s>#]+?)(?P<mode>\((config|admin)-?.*\))?{eol}'.format(eol=PATTERN_EOL)
GENERIC_CISCO_PROMPT1 = '(\n|\r|\x1b\[K){prompt}(?P<mode>\(config-?.*\))?{eol}'      # Generic Pattern having hostname as prompt
CLOSED_CONN_PROMPT = 'Connection .*closed|Received disconnect from'      # Matches Ubuntu/Cygwin telnet/ssh connection closed prompt

# PROMPT_AFTER_LOGIN_PATTERN_GENERIC will be matched after you successfully login to the device
PROMPT_AFTER_LOGIN_PATTERN_GENERIC = '(?P<iosxr>\w+/[0-9]/\w+/\w+:)?(?P<junos_user>[^\s]+?@)?{hostname}{eol}'

MORE_OUTPUT_PROMPT_CISCO = "\s?--More--\s?"  # "--More--" is seen on NCS6K, whereas " --More-- " is seen on other devices
MORE_OUTPUT_PROMPT_JUNOS = "---\(more(\s+\d+\%)?\)---"
MORE_OUTPUT_PROMPT_ARRIS_CMTS = ", \? for help"     # Press <space> to continue, <cr> to single step, ? for help
MORE_OUTPUT_PROMPT_EXTREMEXOS = "Press <SPACE> to continue or <Q> to quit:"     # ExtremeOS
ASCII_DUE_TO_MORE_STRIP1 = '\s?--More--\s?\x08\x08\x08\x08\x08\x08\x08\x08\x08        \x08\x08\x08\x08\x08\x08\x08\x08\x08'
ASCII_DUE_TO_MORE_STRIP2 = '\s?--More--\s?\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \x08'
ASCII_DUE_TO_MORE_PATT = '\x08+[ ]+\x08+[ ]+'
ASCII_DUE_TO_TTY = '\x1b\[K'    # \x1b[K is ESC Char seen when router is logged in via TTY line of term-serv (particularly for XR)
ASCII_CHAR_PATT = '\x08|\x07'

DEFAULT_SEARCH_WINDOW = 50
DEFAULT_WIN_SIZE = (300, 1000)
DEFAULT_TIMEOUT_SESSION = 180

# In case of long input commands that exceeds the Window Size
# Match pattern like: 'show ABC\x08\x08$BCD \x08\x08\r\nReal Output'
# NOTE: All these along with --More-- can be stopped if "terminal length $L" and "terminal width $W" is configured
EXCEED_WS_PATT0 = '\x08$'
EXCEED_WS_PATT1 = '\x08+\$'; LINE_BREAKER = '\r\n'


class Router(object):
    CLI_MODE_USER_EXEC = 0                      # Router>
    CLI_MODE_PRIVILEGED_EXEC = 1                # Router#
    CLI_MODE_GLOBAL_CONFIG = 2                  # Router(config)#
    CLI_MODE_CONFIG_SUB = 3                     # Router(config-.*)#
    CLI_MODE_GLOBAL_ADMIN = 4                   # Router(admin)#
    CLI_MODE_ADMIN_SUB = 4                      # Router(admin-.*)#
    
    def __init__(self, hostname, **kwargs):
        self.hostname = hostname
        self.model_name = kwargs.get('model_name')
        self.os_name = kwargs.get('os_name', OS_NAME_IOS)        # IOS or IOS XR
        self.logger = kwargs.get('logger')  # If None, create utils.logger.Logger() with filename. If False, don't save any logs, but prinout logs
        self.logger_name = kwargs.get('logger_name', str(LOGGER_NAME + '.' + self.hostname))
        self.auto_clean = kwargs.get('auto_clean', True)    # Used for stripping ASCII characters from output
        self.rcli_obj = kwargs.get('rcli_obj')              # routercli.wrapper.Wrapper Object
        self.logged_in = False            # True, if Router logged in succesfully
        self.cli_mode = None              # An integer used to indicate configuration mode (Router.CLI_MODE_*)
        self.autoset_os_name = kwargs.get('autoset_os_name', False)     # If True, set os_name to one of the value from routercli.__init__ based on show version output
        self.autoset_cast = kwargs.get('autoset_cast', False)     # If True, set router.__class__ as a router operatingsystems or platforms
        self.autoset_terminal_length = kwargs.get('autoset_terminal_length', False)     # If True, set terminal length 0 for Cisco device
        self._last_expects = list()
        self._last_matched = str()
        self._last_prompt = str()          # Last prompt (e.g: '\nRP/0/RP1/CPU0:R1(config)#')
        self._real_prompt = str()          # Real prompt without mode (e.g: 'RP/0/RP1/CPU0:R1')
        self._last_out = str()
        self.log_dir = os.path.abspath(kwargs.get('log_dir', DEFAULT_LOG_DIR))
        if not os.path.isdir(self.log_dir):
            self.exception("No such directory: {0}".format(self.log_dir))
        
        # logfile_prgm = log_dir directory + logfile_prgm filename
        default_logfile_prgm = '{0}{1}{2}{3}'.format(self.hostname, '-prgm-logs-', time.strftime("%m-%d-%Y_%H-%M-%S"), '.log')
        self.logfile_prgm = os.path.join(self.log_dir, os.path.basename(kwargs.get('logfile_prgm', default_logfile_prgm)))
        
        self.logfile_prgm_filemode = kwargs.get('logfile_prgm_filemode', 'w')
        
        # logfile_console = log_dir directory + logfile_console filename
        default_logfile_console = '{0}{1}{2}{3}'.format(self.hostname, '-console-logs-', time.strftime("%m-%d-%Y_%H-%M-%S"), '.log')
        logfile_console = kwargs.get('logfile_console', default_logfile_console)
        if logfile_console:
            self.logfile_console = os.path.join(self.log_dir, os.path.basename(logfile_console))
        else:
            self.logfile_console = None
        
        self.delay_before_cmd = kwargs.get('delay_before_cmd', 0)
        self.login_cmd = kwargs.get('login_cmd', 'telnet {0}'.format(hostname))
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        self.enable_password = kwargs.get('enable_password')
        self.final_prompt = kwargs.get('final_prompt')
        self.nested_login = kwargs.get('nested_login', list())
        self.history = list()   # Old cmds executed with out - [(cmd1, out1)]
        self.timeout_login = kwargs.get('timeout_login', 60)     # Login timeout
        self.timeout_logout = kwargs.get('timeout_logout', 5)     # Logout timeout
        self.timeout_session = kwargs.get('timeout_session', DEFAULT_TIMEOUT_SESSION)  # Session timeout, e.i for long cmds
        self.show_version_out = str()
        self.setwinsize = DEFAULT_WIN_SIZE
        self.searchwindowsize = DEFAULT_SEARCH_WINDOW
        self.term_length = None
        self.term_width = None
        
        self._pexpect_session = None
        self._prompt_current = None
        self._prompt_session_start = None
        self._out_after_match = str()
        self._out_before_match = str()
        self._matched = str()
        self._out_buffer = str()
        self._cleaned_logfile = None
        
        self._CONFIRM_PROMPT = '\[confirm\]|\[Y\]'
        self._LOGIN_YES_PROMPTS = 'Are you sure you want to continue connecting (yes/no)'             # Seen during ssh
        self._LOGIN_USERNAME_PROMPTS = '(Username:|login:|login as:|username:)(\s+)?$'
        self._LOGIN_PASSWORD_PROMPTS = 'Password:|password:'
        self._LOGIN_TERM_SERV_LINE_OPEN_PROMPTS = r'Trying.*Open\r\n'
        self._LOGIN_FAILED_PROMPTS = ('Connection refused by remote host|'       # Seen on Cisco when VTY Line is in Use
                                      'unable to find computer address|'         # Seen on Cisco IOS when no telnet host was found
                                      'No user specified nor available for SSH client|' # Seen on Cisco IOS when no ssh host was found
                                      'host nor service provided, or not known|' # Seen on Cisco IOSXR when no telnet host was found
                                      'Invalid input detected at.*marker|'       # Seen on Cisco IOSXR when invalid command is typed 
                                      'Unknown Family or hostname|'              # Seen on Cisco IOSXR when no ssh host was found
                                      'command not found|'                       # Seen on bash when invalid command is typed
                                      'Connection timed out|'                    # Seen on bash when connection timed out
                                      'Can\'t lookup hostname|'                  # Seen on bash when DNS resolution failed
                                      'Connection refused|'                      # Seen by Ubuntu telnet/ssh fail
                                      'Name or service not known|'               # Seen by Ubuntu telnet/ssh fail
                                      'Could not resolve hostname|'              # Seen by Ubuntu telnet/ssh dns name lookup fail
                                      'Protocol major versions differ: 2 vs. 1|' # Seen by Ubuntu when login to cisco device running ssh v1)
                                      'Connection closed')                       # Seen by Ubuntu when login to ssh device is refused by server
        self._DELAY_AFTER_LINE_OPEN = 1

        self.PROMPT_SYNTAX_XR = '\w+/[0-9]/\w+/\w+:'
        if self.os_name == OS_NAME_IOS:
            self.PROMPT_USER_EXEC_MODE = hostname + '>'
            self.PROMPT_PRIVILEGED_EXEC_MODE = hostname + '#'
            self.PROMPT_GLOBAL_CONFIG_MODE = hostname + '\(config\)#'
            self.PROMPT_CONFIG_SUB_MODE = hostname + '\(config-.*\)#'
        elif self.os_name == OS_NAME_IOS_XR:
            prompt_syntax_xr = self.PROMPT_SYNTAX_XR
            self.PROMPT_USER_EXEC_MODE = prompt_syntax_xr + hostname + '>'
            self.PROMPT_PRIVILEGED_EXEC_MODE = (prompt_syntax_xr +
                                                hostname + '#')
            self.PROMPT_GLOBAL_CONFIG_MODE = (prompt_syntax_xr +
                                              hostname + '\(config\)#')
            self.PROMPT_CONFIG_SUB_MODE = (prompt_syntax_xr + hostname +
                                           '\(config-.*\)#')
            
        if self.logger is None:
            self.logger = MyLogger(name=self.logger_name, filename=self.logfile_prgm, filemode=self.logfile_prgm_filemode, propagate=0)
            self.log('Created new logger', 'INFO')
        elif self.logger is False:
            import logging
            self.logger = MyLogger(name=self.logger_name, level=logging.INFO)
            self.log('Not using any logger', 'INFO')
        else:
            self.log('Using existing logger', 'INFO')
        if self.logger.filename:
            self.log('Using Directory for logs: {0}'.format(self.log_dir), 'INFO')
            self.log('Using logfile_prgm: {0}'.format(os.path.basename(self.logfile_prgm)), 'INFO')
            if self.logfile_console:
                self.log('Using logfile_console: {0}'.format(os.path.basename(self.logfile_console)), 'INFO')
        
        self._console_log = None
        self._logfile_send = StringIO()
        
        #
        # router properties
        #
        self.mcast = Mcast(self)        # Multicast related
        self.unicast = Unicast(self)    # Unicast related
        self.l2 = Layer2(self)          # Layer2 related (cdp, lldp)
        self.isis = Isis(self)
        self.bgp = Bgp(self)
        
    def __del__(self):
        self._exit()
        
    def login(self, **kwargs):
        """
        Tries to Login. Returns (True, 'matched pattern') if successful, else
        (False, 'login failure reason')
        
        Args:
        login_cmd        -- Defaults to, e.g  - 'telnet hostname'
        username         -- Required, e.g  - 'myusername'
        password         -- Required, e.g  - 'mypwd'
        final_prompt     -- Pattern to expect after login and every command execution
        nested_login       -- [{'login_cmd': 'telnet device2', 'username': 'xyz'},
                               {'login_cmd': 'telnet device3'}]
        setwinsize         -- TTY Window Size Tuple - (row, col)
                              Default to (300, 1000)
                              where row and col are in length of string
        searchwindowsize   -- Size of window to be searched,
                              where size is in length of string
                              Default to 30
        timeout_login            -- timeout Defaults to self.timeout_session
        hostname_prompt    -- Hostname that will appear in the prompt
        """
        # Get args
        login_cmd = kwargs.get('login_cmd', self.login_cmd)
        username = kwargs.get('username', self.username)
        self.username = username
        password = kwargs.get('password', self.password)
        self.enable_password = self.password = password
        final_prompt = kwargs.get('final_prompt', self.final_prompt)
        self.final_prompt = final_prompt
        nested_login = kwargs.get('nested_login', self.nested_login)
        setwinsize = kwargs.get('setwinsize', self.setwinsize)
        searchwindowsize = kwargs.get('searchwindowsize', self.searchwindowsize)
        timeout_login = kwargs.get('timeout_login', self.timeout_login)
        hostname_prompt = kwargs.get('hostname_prompt')
        
        if login_cmd is None:
            msg = 'login-failed: login_cmd cannot be None. Please provide it next time'
            self.log(msg, 'WARNING')
            return (False, msg)
        
        os.environ["TERM"] = "dumb"
        self.log("Giving cmd: '{0}'".format(repr(login_cmd)))
        self._pexpect_session = mypexpect.spawn(login_cmd, env = {"TERM": "dumb"})
        self._pexpect_session.ignorecase = kwargs.get('ignorecase', True)
        msg = 'Started PID: {0}, with FD: {1}'.format(self._pexpect_session.child_fd, self._pexpect_session.pid)
        self.log(msg)
        
        if self.logfile_console:
            self._console_log = open(self.logfile_console, 'w+')
        else:
            self._console_log = None
            
        self._pexpect_session.setecho(False)
        self._pexpect_session.maxread = 99999
        self._pexpect_session.logfile_read = self._console_log
        self._pexpect_session.logfile_send = self._logfile_send
        # Set the window size
        if setwinsize is not None:
            self._pexpect_session.setwinsize(setwinsize[0], setwinsize[1])
            
        if final_prompt is None and hostname_prompt is None:
            # After login, match any prompt
            self._prompt_current = GENERIC_CISCO_PROMPT
            self._prompt_session_start = self._prompt_current
            
        if final_prompt:
            self._prompt_current = final_prompt
            self._prompt_session_start = final_prompt
            
        if hostname_prompt:
            hostname_prompt = PROMPT_AFTER_LOGIN_PATTERN_GENERIC.format(hostname=hostname_prompt, eol=PATTERN_EOL)
            self._prompt_current = hostname_prompt
            self._prompt_session_start = hostname_prompt
            
        expect_list = [self._LOGIN_YES_PROMPTS,
                       self._LOGIN_USERNAME_PROMPTS,
                       self._LOGIN_PASSWORD_PROMPTS,
                       self._LOGIN_TERM_SERV_LINE_OPEN_PROMPTS,
                       self._LOGIN_FAILED_PROMPTS,
                       self._prompt_current]
        
        try:
            i = self._pexpect_session.expect(expect_list, searchwindowsize=searchwindowsize, timeout=timeout_login)
        except Exception as e:
            msg = 'login-failed: due to pexpect err: {0}'.format(e)
            self.log(msg, 'EXCEPTION')
            self._pexpect_session.close()
            self._console_log.close()
            return (False, msg)
        matched = expect_list[i]
        
        login_no = 1
        if nested_login == list():
            nested_login = [{None:None}]
        else:
            nested_login = nested_login + [{None:None}]
        self.nested_login = nested_login
        for nxt_login_kwargs in nested_login:
            check_login_out = str(self._pexpect_session.before) + str(self._pexpect_session.buffer) + str(self._pexpect_session.after)
            check_login = re.search(self._LOGIN_FAILED_PROMPTS, check_login_out)
            if i == expect_list.index(self._LOGIN_FAILED_PROMPTS) or check_login is not None:
                if check_login:
                    check_login = check_login.group()
                msg = 'login-failed: due to following reason: {0}'.format(check_login)
                self.log(msg, 'WARNING')
                self._pexpect_session.close()
                self._console_log.close()
                return (False, msg)
                
            if i == expect_list.index(self._LOGIN_TERM_SERV_LINE_OPEN_PROMPTS):
                try:
                    i = self._pexpect_session.expect(expect_list, timeout=self._DELAY_AFTER_LINE_OPEN)
                except mypexpect.TIMEOUT:
                    self.log('Hitting Enter to see next line...')
                    matched = self.exec_cmd('', expects=expect_list,
                                            return_matched=True, timeout=timeout_login)
                    try:
                        i = expect_list.index(matched)
                    except ValueError:
                        msg = ('After hitting enter, {0} not in list {1}'
                               ''.format(matched, repr(expect_list)))
                        self.log(msg, level='WARNING')
                else:
                    # There was a Password/Username
                    self.log('No need to hit enter...')
                    
            if i == expect_list.index(self._prompt_current):
                matched = self._prompt_current
                msg = ('After giving cmd, prompt directly matched:'
                       ' {0}'.format(matched))
                self.log(msg)
                
            if i == expect_list.index(self._LOGIN_YES_PROMPTS):
                matched = self.exec_cmd("yes", expects=expect_list,
                                  return_matched=True, timeout=timeout_login)
                try:
                    i = expect_list.index(matched)
                except ValueError:
                    msg = ('After giving yes, {0} not in list {1}'
                           ''.format(matched, repr(expect_list)))
                    self.log(msg, level='WARNING')
    
            if i == expect_list.index(self._LOGIN_USERNAME_PROMPTS):
                if username == None:
                    msg = 'login-failed: Username not provided'
                    self.log(msg, 'WARNING')
                    self._pexpect_session.close()
                    self._console_log.close()
                    return (False, msg)
                matched = self.exec_cmd(username, expects=expect_list,
                                  return_matched=True, timeout=timeout_login)
                try:
                    i = expect_list.index(matched)
                except ValueError:
                    msg = ('After giving username, {0} not in list {1}'
                           ''.format(matched, repr(expect_list)))
                    self.log(msg, level='WARNING')
    
            if i == expect_list.index(self._LOGIN_PASSWORD_PROMPTS):
                if password == None:
                    msg = 'login-failed: Password not provided'
                    self.log(msg, 'WARNING')
                    self._pexpect_session.close()
                    self._console_log.close()
                    return (False, msg)
                matched = self.exec_cmd(password, expects=expect_list, return_matched=True, timeout=timeout_login)
                try:
                    i = expect_list.index(matched)
                except ValueError:
                    msg = ('After giving password, {0} not in list {1}'
                           ''.format(matched, repr(expect_list)))
                    self.log(msg, level='WARNING')
    
                if (i == expect_list.index(self._LOGIN_USERNAME_PROMPTS) or
                    i == expect_list.index(self._LOGIN_PASSWORD_PROMPTS)):
                    msg = ('Prompt: "{0}" returned again... Indicates '
                           'invalid Username/Password'
                           ''.format(self._LOGIN_USERNAME_PROMPTS))
                    self.log(msg, 'WARNING')
                    self._pexpect_session.close()
                    self._console_log.close()
                    return (False, msg)
                    
                elif i == expect_list.index(self._prompt_current):
                    msg = ('Login successful... with matched as prompt: {0}'.format(repr(self._out_after_match)))
                    self.log(msg, 'INFO')
                    out = self._out_before_match + self._out_after_match
                    m = re.search(GENERIC_CISCO_PROMPT, out)
                    if m is not None:
                        if 'iosxr' in m.groupdict():
                            if m.group('iosxr') != None:
                                if self.os_name != OS_NAME_IOS_XR:
                                    msg = ('Software version provided: {0}.'
                                           ' Based on prompt, actually the '
                                           'software version is: {1}'
                                           ''.format(self.os_name, OS_NAME_IOS_XR))
                                    self.log(msg)
                                    self.os_name = OS_NAME_IOS_XR
                            elif m.group('iosxr') == None:
                                if self.os_name != OS_NAME_IOS:
                                    msg = ('Software version provided: {0}.'
                                           ' Based on prompt, actually the '
                                           'software version is: {1}'
                                           ''.format(self.os_name, OS_NAME_IOS))
                                    self.log(msg)
                                    self.os_name = OS_NAME_IOS
                        else:
                            self.log(m.groupdict(), 'EXCEPTION')
                            
            # Nested Login
            if nxt_login_kwargs is not None:
                self.log('Nested Login No. {0} with args: {1}'.format(login_no, nxt_login_kwargs))
                nxt_login_cmd = nxt_login_kwargs.get('login_cmd')
                if nxt_login_cmd is None:
                    continue
                self._prompt_current = nxt_login_kwargs.get('final_prompt', self._prompt_current)
                username = nxt_login_kwargs.get('username', username)
                password = nxt_login_kwargs.get('password', password)
                matched = self.exec_cmd(nxt_login_cmd, expects=expect_list,
                                  return_matched=True, timeout=timeout_login)
                try:
                    i = expect_list.index(matched)
                except ValueError:
                    msg = ('After giving nested login: {2}, {0} not in list {1}'
                           ''.format(matched, repr(expect_list), nxt_login_cmd))
                    self.log(msg, level='WARNING')
                login_no = login_no + 1
                
        _real_prompt = self._last_prompt.strip()
        self._real_prompt = re.sub(PATTERN_EOL, '', _real_prompt)
        self._prompt_current = GENERIC_CISCO_PROMPT1.format(prompt=self._real_prompt, eol=PATTERN_EOL)
#         print 'test-_real_prompt', repr(_real_prompt), 'test-self._real_prompt', repr(self._real_prompt)
        self.log('After login, using _prompt_current as: {0}'.format(repr(self._prompt_current)))
        self._pexpect_session._PROMPT_CURRENT.append(self._prompt_current)
        self.cli_mode = self.get_mode()
        self.logged_in = True
        
        if self.autoset_os_name:
            if self.os_name == OS_NAME_IOS: # Based on prompt
                sv = self.show_version()
                if 'NX-OS' in sv:
                    self.set_os_name(OS_NAME_NX_OS)
                elif 'Cisco IOS Software' in sv:  # Confirm version
                    self.set_os_name(OS_NAME_IOS)
                elif 'Chassis Type:' in sv and 'Software Version' in sv:
                    self.set_os_name(OS_NAME_ARRIS_CMTS)
                elif 'JUNOS Base OS' in sv:
                    self.set_os_name(OS_NAME_JUNOS)
                elif '^-------unknown keyword show' in sv:
                    self.set_os_name(OS_NAME_SCREENOS)
                elif "The program 'show' is currently not installed." in sv or '-bash: show: command not found' in sv:
                    self.set_os_name(OS_NAME_LINUX0)
                for model_name in MODEL_NAMES_ALL:
                    if MODEL_NAMES_ALL[model_name] in sv:
                        self.set_model_name(model_name)
                        break
                
            elif self.os_name == OS_NAME_IOS_XR: # Based on prompt
                sv = self.show_version(cmd='show version brief')
                if 'IOS XR' in sv:  # Confirm version
                    self.set_os_name(OS_NAME_IOS_XR)
        if self.autoset_cast:
            self.set_router_cast(self.os_name)
            
        if self.autoset_terminal_length:
            if self.os_name in [OS_NAME_IOS, OS_NAME_IOS_XR, OS_NAME_NX_OS, OS_NAME_ARRIS_CMTS]:
                self.exec_cmd('terminal length 0', timeout=timeout_login)
            elif self.os_name in [OS_NAME_JUNOS]:
                self.exec_cmd('set cli screen-length 0', timeout=timeout_login)
            
        return (True, matched)
    
    def set_os_name(self, os_name):
        if os_name not in OS_NAMES_ALL:
            self.exception('set_os_name not correct!!')
        self.log('os_name set to {0}'.format(os_name), 'INFO')
        self.os_name = os_name
    
    def set_model_name(self, model_name):
        if model_name not in MODEL_NAMES_ALL.keys():
            self.exception('model_name not correct!!')
        self.log('model_name set to {0}'.format(model_name), 'INFO')
        self.model_name = model_name
    
    def set_router_cast(self, os_name=None):
        if os_name == None:
            os_name = self.os_name
        if os_name not in OS_NAMES_ALL:
            self.exception('set_router_cast os_name not correct!!')
        if os_name == OS_NAME_IOS:
            from routercli.operatingsystems.ios.ios import IOS
            self.__class__ = IOS
        elif os_name == OS_NAME_IOS_XR:
            from routercli.operatingsystems.iosxr.iosxr import IOSXR
            self.__class__ = IOSXR
        elif os_name == OS_NAME_NX_OS:
            from routercli.operatingsystems.nxos.nxos import NXOS
            self.__class__ = NXOS
    
    def logout(self):
        if self.logged_in is True:
            self.log('Logging out...')
            self._exit()

    def exec_cmd(self, cmd, **kwargs):
        """ Execute Command

        Arguments:
        cmd                  -- String. Command to be executed
                                (Mandatory)
        expects              -- List. List of possible matches
                                (Default to [])
        return_output        -- Boolean. If True, returns Output
                                (Default to True)
        return_matched        -- Boolean. If True, returns matched expect
                                (Default to False)
        prompt               -- String. Expected Prompt after
                                command execution (Default to None)
        auto_complete        -- Boolean. If True, hits spaces
                                till prompt is seen (in case of huge output)
                                (Default to True)
        auto_confirm         -- Boolean. If True, Hit Enter to Confirm if asked
                                (Default to True)
        correct_cmd          -- Boolean. If True, correct the cmd depending on os_name
                                (Default to True)
        setwinsize           -- TTY Window Size Tuple - (row, col)
                                Default to (300, 1000)
                                where row and col are in length of string
        searchwindowsize     -- Size of window to be searched,
                                where size is in length of string
                                (Default to 30)
        privileged_exec_mode -- Boolean. If True, go into privileged exec mode
                                (Default to False)
        auto_clean           -- Boolean. If True, Clean Output
                                (Default to self.auto_clean)
        timeout              -- Integer. Time in seconds to wait for prompt
                                (Default to self.timeout_session)
        report               -- Save the output in rclidb via rcli_obj
                                (Default to False)
                                
        Note:
        To hit enter only use cmd = ''
        
        Example:
        login("Telnet R1")                       # Go to User Exec Mode
        exec_cmd("show clock")                   # Returns Output
        exec_cmd("enable",
                 expects=["Password:", "R1#"],
                 prompt="R1#")                       # Go to Enable Mode
        exec_cmd("Conf t", prompt="R1\(Config\)#")   # Go to Config Mode

        """
        
        # Setting Parameters
        expects = kwargs.get('expects', [])
        return_output = kwargs.get('return_output', True)
        return_matched = kwargs.get('return_matched', False)
        auto_complete = kwargs.get('auto_complete', True)
        auto_confirm = kwargs.get('auto_confirm', True)
        correct_cmd = kwargs.get('correct_cmd', True)
        prompt = kwargs.get('prompt')
        privileged_exec_mode = kwargs.get('privileged_exec_mode', False)
        searchwindowsize = kwargs.get('searchwindowsize', self.searchwindowsize)
        setwinsize = kwargs.get('setwinsize', self.setwinsize)
        auto_clean = kwargs.get('auto_clean', self.auto_clean)
        timeout = kwargs.get('timeout', self.timeout_session)
        save_to_report = kwargs.get('save_to_report', False)
        
        # Set the window size
        if setwinsize is not None:
            self._pexpect_session.setwinsize(setwinsize[0], setwinsize[1])
            
        if return_matched is True and return_output is True:
            return_output = False
            
        # Correct cmds
        if correct_cmd is True:
            if cmd != '':
                cmd = self.correct_cmd(cmd)
        
        # privileged_exec_mode
        if privileged_exec_mode is True:
            self.go_into_mode(privileged_exec_mode=True)
            
        if cmd == '':
            self.log('Executing cmd: Hitting Enter Only...')
            self._pexpect_session.sendcontrol('m') # Send new line / Press Enter
        else:
            self.log('Executing cmd: "{0}"...'.format(cmd))
            self._pexpect_session.sendline(cmd)
        
        if prompt is not None:
            self._prompt_current = prompt
        if isinstance(expects, list) is False:
            expects = [expects]

        expects = expects[:]
        if self._CONFIRM_PROMPT in expects:
            expects.remove(self._CONFIRM_PROMPT)
        if MORE_OUTPUT_PROMPT_CISCO in expects:
            expects.remove(MORE_OUTPUT_PROMPT_CISCO)
        if MORE_OUTPUT_PROMPT_JUNOS in expects:
            expects.remove(MORE_OUTPUT_PROMPT_JUNOS)
        if MORE_OUTPUT_PROMPT_ARRIS_CMTS in expects:
            expects.remove(MORE_OUTPUT_PROMPT_ARRIS_CMTS)
        if MORE_OUTPUT_PROMPT_EXTREMEXOS in expects:
            expects.remove(MORE_OUTPUT_PROMPT_EXTREMEXOS)
        if self._prompt_current in expects:
            expects.remove(self._prompt_current)
            
        # Order of this list should be considered while modifying the code
        #                        confirm_prompt_i     more_output_prompt_extremexos_i   more_output_prompt_cisco_i   more_output_prompt_junos_i   more_output_prompt_arris_cmts_i   prompt_current_i         eof_i
        expects = (expects + [self._CONFIRM_PROMPT] + [MORE_OUTPUT_PROMPT_EXTREMEXOS] + [MORE_OUTPUT_PROMPT_CISCO] + [MORE_OUTPUT_PROMPT_JUNOS] + [MORE_OUTPUT_PROMPT_ARRIS_CMTS] + [self._prompt_current] + [EOF])
        l = len(expects)
        eof_i = l - 1
        prompt_current_i = l - 2
        more_output_prompt_arris_cmts_i = l - 3
        more_output_prompt_junos_i = l - 4
        more_output_prompt_cisco_i = l - 5
        more_output_prompt_extremexos_i = l - 6
        confirm_prompt_i = l - 7
#         print 'test-expects', timeout, expects, cmd
        try:
            if self.delay_before_cmd:
                time.sleep(self.delay_before_cmd)
            i = self._pexpect_session.expect(expects, searchwindowsize=searchwindowsize, timeout=timeout)
        except Exception as e:
            self.log('pexpect error with expects: {0}'.format(expects), 'EXCEPTION')
            self.exception('Raised Exception as something didnt expect: {0}'.format(e))
        
        if self._pexpect_session.before is None or self._pexpect_session.after is None:
            reason = ('Expected prompt was not matched, something else got matched.'
                      ' expects: {0}, i: {1}, before: {2}, after: {3}'
                      ''.format(expects, i, self._pexpect_session.before, self._pexpect_session.after))
            self.exception(reason)
        
        if i == eof_i:       # If EOF
            msg = ("""Expected Prompt not found
                      Match list: {0}
                      Output After Prompt: '{1}'""".format(expects,
                             repr(self._pexpect_session.after)))
            self.exception(msg)

        # Updating attributes
        self._update_attrs(expects, i)
        output = self._last_out

        self.log("The pattern: '{0}' matched: '{1}'"
                  "".format(repr(self._last_matched), repr(self._last_prompt)))

        if i != prompt_current_i:       # If not Current Prompt
            msg = ("Warning: Last command matched: '{0}'."
                   " Should have matched prompt: '{1}'"
                   "".format(repr(expects[i]), repr(expects[prompt_current_i])))
            self.log(msg)
            
            # [Confirm], [Y]
            if (i == confirm_prompt_i) and auto_confirm is False:
                # Just send 'n' character denying confirmation
                self._pexpect_session.send('n')
                j = self._pexpect_session.expect(expects, searchwindowsize=searchwindowsize, timeout=timeout)
                self._update_attrs(expects, j)
                output += self._last_out
                 
                if j != prompt_current_i:
                    reason = 'Matched: {0}, but expected to match prompt while not confirming'.format(expects[j])
                    self.exception(reason)
                
            elif (i == confirm_prompt_i) and auto_confirm is True:
                # If confirm is seen, Hit Enter
                self._pexpect_session.sendcontrol('m') # Send new line / Press Enter
                j = self._pexpect_session.expect(expects, searchwindowsize=searchwindowsize, timeout=timeout)
                self._update_attrs(expects, j)
                output += self._last_out
                 
                if j != prompt_current_i:
                    reason = 'Matched: {0}, but expected to match prompt while confirming'.format(expects[j])
                    self.exception(reason)
                
            # --More--
            if (i == more_output_prompt_cisco_i or i == more_output_prompt_junos_i or i == more_output_prompt_arris_cmts_i or more_output_prompt_extremexos_i == i) and auto_complete is False:
                # Just quit after seeing first More
                self._pexpect_session.send('q')
                j = self._pexpect_session.expect(expects, searchwindowsize=searchwindowsize, timeout=timeout)
                self._update_attrs(expects, j)
                output += self._last_out
                 
                if j != prompt_current_i:
                    reason = 'Matched: {0}, but expected to match prompt'.format(expects[j])
                    self.exception(reason)
                
            elif (i == more_output_prompt_cisco_i or i == more_output_prompt_junos_i or i == more_output_prompt_arris_cmts_i or more_output_prompt_extremexos_i == i) and auto_complete is True:
                # If --More-- is seen, keep hitting spaces till
                # current prompt is seen
                more_seen = True
                cnt = 1
                while more_seen:
                    self._pexpect_session.send(' ')
                    if (i == more_output_prompt_cisco_i):
                        new_expects = [MORE_OUTPUT_PROMPT_CISCO,
                                       self._prompt_current, EOF]
                    elif (i == more_output_prompt_junos_i):
                        new_expects = [MORE_OUTPUT_PROMPT_JUNOS,
                                       self._prompt_current, EOF]
                    elif (i == more_output_prompt_arris_cmts_i):
                        new_expects = [MORE_OUTPUT_PROMPT_ARRIS_CMTS,
                                       self._prompt_current, EOF]
                    elif (i == more_output_prompt_extremexos_i):
                        new_expects = [MORE_OUTPUT_PROMPT_EXTREMEXOS,
                                       self._prompt_current, EOF]
                    j = self._pexpect_session.expect(new_expects, searchwindowsize=searchwindowsize, timeout=timeout)
                    
                    # Update attributes
                    self._update_attrs(new_expects, j)
                    output += self._last_out
                    
                    if j == 2:      # Matched EOF
                        msg = ("""Expected Prompt not found
                                  Match list: {0}
                                  Output After Prompt: '{1}'"""
                                  """""".format(new_expects,
                                        repr(self._pexpect_session.after)))
                        self.exception(msg)
                    elif j == 1:        # Matched prompt
                        self.log("'{0}' stopped showing. Matched '{1}' finally"
                                  "".format(new_expects[0], new_expects[1]))
                        break
                    elif j == 0:        # Matched --More--
                        self.log('{0} seen {1} times'
                                  ''.format(new_expects[0], cnt))
                    cnt += 1
                    
        if auto_clean is True:
            clean_out = self._clean_out(str_in=output, cmd_given=cmd)
        else:
            clean_out = output
            
        if save_to_report is True:
            self.rcli_obj.save_to_report(self.hostname, cmd, repr(clean_out))
            
        self.history.append((cmd, clean_out))
        self.cli_mode = self.get_mode()
        if return_output is True:
            return clean_out
        
        elif return_matched is True:
            return self._last_matched
    
    def _update_attrs(self, expects, matched_index, **kwargs):
        self._last_prompt = self._pexpect_session.match.group()
        self._last_expects = expects
        self._last_matched = expects[matched_index]
        self._last_out = str(self._pexpect_session.before) + str(self._pexpect_session.buffer) + str(self._pexpect_session.after)
        self._out_before_match = self._pexpect_session.before
        self._out_after_match = self._pexpect_session.after
        self._out_buffer = self._pexpect_session.buffer
        
    def convert_cmd_to_fullcmd(self, cmd):
        # TODO: Future
        return cmd
    
    def get_mode(self):
        found_prompt = re.search(GENERIC_CISCO_PROMPT, self._last_prompt)
        if found_prompt is None:
            self.log('Prompt not matched while getting mode')
            return None
        
        prompt_dict = found_prompt.groupdict()
        if prompt_dict['mode'] is None:
            if prompt_dict['eol'] == '#':
                self.cli_mode = self.CLI_MODE_PRIVILEGED_EXEC
            elif prompt_dict['eol'] == '>':
                self.cli_mode = self.CLI_MODE_USER_EXEC
        else:
            if '(config)' in prompt_dict['mode']:
                self.cli_mode = self.CLI_MODE_GLOBAL_CONFIG
            elif '(admin)' in prompt_dict['mode']:
                self.cli_mode = self.CLI_MODE_GLOBAL_ADMIN
            elif '(config-' in prompt_dict['mode']:
                self.cli_mode = self.CLI_MODE_CONFIG_SUB
            elif '(admin-' in prompt_dict['mode']:
                self.cli_mode = self.CLI_MODE_ADMIN_SUB
        return self.cli_mode
        
    def go_into_mode(self, **kwargs):
        '''
        privileged_exec_mode
        config_mode
        '''
        self.log('Going into mode {0}'.format(kwargs))
        privileged_exec_mode = kwargs.get('privileged_exec_mode', False)
        config_mode = kwargs.get('config_mode', False)
        if privileged_exec_mode is True and self.cli_mode != self.CLI_MODE_PRIVILEGED_EXEC:
            if self.cli_mode == self.CLI_MODE_USER_EXEC:
                enable_out = self.exec_cmd('enable', expects=['Password:'], auto_clean=False)
                if 'Password' in enable_out:
                    after_pwd_o = self.exec_cmd(self.enable_password, auto_clean=False)
                    if 'Password' in after_pwd_o:
                        msg = 'Invalid EnablePassword: {0}'.format(self.enable_password)
                        self.exception(msg)
            else:
                self.exec_cmd('end    ! Going to privilege mode')
        
        if config_mode is True and self.cli_mode != self.CLI_MODE_GLOBAL_CONFIG:
            if self.cli_mode != self.CLI_MODE_USER_EXEC:
                if self.cli_mode != self.CLI_MODE_PRIVILEGED_EXEC:
                    self.exec_cmd('end    ! Going to config mode')
            if self.cli_mode == self.CLI_MODE_USER_EXEC:
                self.go_into_mode(privileged_exec_mode=True)
            self.exec_cmd('configure terminal')
                
        return self.cli_mode
    
    def correct_cmd(self, cmd_given):
        '''
        Change the cmd based on software version
        e.g for IOS - cmd | include x|y
            for IOS XR - cmd | include "x|y"
        '''
        
        cmd_given = self.convert_cmd_to_fullcmd(cmd_given)
        corrected_cmd = cmd_given
        
        # Fix show logging include pipe
        include_statement = ' | include'
        include_st_with_pipe = '\| include (.*[\|].*)'    # e.g - show logging | include x|y
        if include_statement in cmd_given:
            found_cmd = re.search(include_st_with_pipe, cmd_given)
            if found_cmd is not None:
                include_piped = found_cmd.group(1)   # e.g - x|y from above patt
                if '|' in include_piped:
                    new_show_include = include_piped.lstrip('"').rstrip('"')    # Remove " char
                    if self.os_name == OS_NAME_IOS:
                        # IOS cannot contain "x|y" in include
                        corrected_cmd = cmd_given.replace(include_piped, new_show_include)
                    elif self.os_name == OS_NAME_IOS_XR:
                        # IOS XR must contain "x|y" in include
                        new_show_include = '"{0}"'.format(new_show_include)     # Add " again
                        corrected_cmd = cmd_given.replace(include_piped, new_show_include)
                    
        if corrected_cmd != cmd_given:
            self.log('Corrected cmd from "{0}" to "{1}"'.format(cmd_given, corrected_cmd))
        return corrected_cmd
        
    def _clean_out(self, str_in, cmd_given=None, keep_cmd=False):
        if self.auto_clean is False:
            return str_in
        
        if re.search(ASCII_CHAR_PATT, str_in) is not None:
            self.log('Cleaning original raw output...')
        
        # In case of Window size was exceeded due to a long cmd
        if EXCEED_WS_PATT0 in str_in:
            if cmd_given is None:
                self._logfile_send.seek(0)
                cmds = self._logfile_send.readlines()
            else:
                cmds = [cmd_given]
            for line in str_in.split(LINE_BREAKER):
                if EXCEED_WS_PATT0 in line:
                    line = line.replace(self._last_prompt, '')
                    part_of_cmd = line[:line.find(re.search(EXCEED_WS_PATT1, line).group())]
                    for cmd in cmds:
                        if part_of_cmd in cmd:
                            if keep_cmd is False:
                                str_in = str_in.replace(line, '')
                            else:
                                str_in = str_in.replace(line, cmd)
                        
        # In case of --More--
        # Specific way of stripping
        for all_more_prompt in set(re.findall(ASCII_DUE_TO_MORE_STRIP1, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        for all_more_prompt in set(re.findall(ASCII_DUE_TO_MORE_STRIP2, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        
        # TODO: Strip particular patterns (add here when you find new patterns)
        patts = [ASCII_DUE_TO_MORE_PATT, ASCII_DUE_TO_TTY]
        for patt in patts:
            matches = re.findall(patt, str_in)
            matches = list(set(matches))    # Make it unique
            matches.sort(key=len, reverse=True)    #    Match most specific 1st
            for matched in matches:
                str_in = str_in.replace(matched, '')
        for all_more_prompt in set(re.findall(MORE_OUTPUT_PROMPT_CISCO, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        for all_more_prompt in set(re.findall(MORE_OUTPUT_PROMPT_JUNOS, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        for all_more_prompt in set(re.findall(MORE_OUTPUT_PROMPT_ARRIS_CMTS, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        for all_more_prompt in set(re.findall(MORE_OUTPUT_PROMPT_EXTREMEXOS, str_in)):
            str_in = str_in.replace(all_more_prompt, '')
        
        # Strip of prompt and cmd from output
        if keep_cmd is False and cmd_given is not None:
            str_in = str_in.replace(self._last_prompt, '')
            # Cmd must be 1st
            loc_of_cmd = str_in.find(cmd_given)
            if loc_of_cmd == 0:
                str_in = str_in[len(cmd_given):]
                
        # In general
        str_in = filter(lambda x: x in string.printable, str_in)
        
        # Remove any leftover ASCII char
        # Generic way of stripping
        matches = re.findall(ASCII_CHAR_PATT, str_in)
        matches = list(set(matches))    # Make it unique
        matches.sort(key=len, reverse=True)    #    Match most specific 1st
        for matched in matches:
            str_in = str_in.replace(matched, '')
            
        return str_in.strip()

    def get_output(self, cmd, latest=True):
        out = None
        for cmds_outs in self.history:
            cmd_found, out_found = cmds_outs
            if cmd == cmd_found:
                out = out_found
                if latest is True:
                    pass    # No break, therefore get latest data
                else:
                    break   # break to get oldest data
        return out
    
    def show_version(self, **kwargs):
        '''
        Get "show version" output
        
        Arguments:
        cmd            -- Override the command being used
        '''
        cmd = kwargs.get('cmd', 'show version')
        if self.show_version_out == str():
            self.show_version_out = self.exec_cmd(cmd)
        return self.show_version_out
        
    def exception(self, reason):
        self.log(str(reason), level='WARNING')
        self._exit()
        raise Exception(reason)
    
    def log(self, msg, level='DEBUG'):
        if self.logger is None:
            print msg
        else:
            self.logger.select_logger(self.logger_name)
            self.logger.log(msg, level=level)

    def _clean_logfile(self):
        if self.logfile_console and not self._cleaned_logfile:
            self.log('Cleaning logfile...')
            out = open(self.logfile_console).read()
            out = self._clean_out(out, keep_cmd=True)
            with open(self.logfile_console, 'w') as logfile:
                logfile.write(out)
            self._cleaned_logfile = True

    def _exit(self):
        if not self.__dict__.get('_pexpect_session'):
            return
        self.log('Exiting routercli...')
        if self._pexpect_session.isalive() is True:
            try:
                self.log('Hitting Ctrl+Shift+6 and X to come out of any nested sessions...')
                self._pexpect_session.send(chr(30))     # Ctrl+Shift+6
                no_nested_sessions = 'x'
                self._pexpect_session.send(no_nested_sessions)         # x
                expect_list = [GENERIC_CISCO_PROMPT,    # Prompt seen
                               CLOSED_CONN_PROMPT,      # If connection is closed
                               self._CONFIRM_PROMPT,     # Seen on when there are nested sessions open
                               no_nested_sessions,      # Seen if there is no nested session
                               ]
                check_if_loggedin = str(self._pexpect_session.before) + str(self._pexpect_session.buffer) + str(self._pexpect_session.after)
                if re.search(self._LOGIN_PASSWORD_PROMPTS, check_if_loggedin) is not None or re.search(self._LOGIN_USERNAME_PROMPTS, check_if_loggedin) is not None:
                    self.log('It looks like still not logged in. Therefore, just killing the session', 'WARNING')
                else:
                    i = self._pexpect_session.expect(expect_list, searchwindowsize=self.searchwindowsize, timeout=self.timeout_logout) #TODO: Failing here sometimes
                    if i == expect_list.index(GENERIC_CISCO_PROMPT) or i == expect_list.index(no_nested_sessions):
                        expect_list.remove(no_nested_sessions)
                        self.log('Hitting Ctrl+C to come out of any sub config mode')
                        self._pexpect_session.sendcontrol('C')
                        i = self._pexpect_session.expect(expect_list, searchwindowsize=self.searchwindowsize, timeout=self.timeout_logout)
                        if i == expect_list.index(GENERIC_CISCO_PROMPT):
                            self.log('exiting...')
                            self._pexpect_session.sendline('exit')
                            i = self._pexpect_session.expect(expect_list, searchwindowsize=self.searchwindowsize, timeout=self.timeout_logout)
                            if i == expect_list.index(self._CONFIRM_PROMPT):
                                self.log('Closing any open nested sessions by hitting enter on confirm')
                                self._pexpect_session.sendcontrol('m') # Send new line / Press Enter
                                i = self._pexpect_session.expect(expect_list, searchwindowsize=self.searchwindowsize, timeout=self.timeout_logout)
                    if i == expect_list.index(CLOSED_CONN_PROMPT):
                        self.log('Successfully exited')
                    else:
                        self.log('Not successfully exited: {0}:{1}'.format(i, repr(expect_list[i])))
            except Exception as e:
                pass
#                 msg1 = ('Exception caught during closing '
#                        'connection: {0}'.format(e))
#                 self.log(msg1, level='EXCEPTION')
            finally:
                if self._pexpect_session:
                    self._pexpect_session.close()
                
        if self._pexpect_session:
            if self._pexpect_session.terminated is False or self._pexpect_session.closed is False:
                # self.log('Worst case logout...')
                self._pexpect_session.close()
                self._pexpect_session.terminate()
                
        # Close the console file
        if self._console_log is not None:
            if self._console_log.closed is False:
                self._console_log.close()
                
        # Reopen the file and clean it up
        self._clean_logfile()
        
        if not self._logfile_send:
            if self._logfile_send.closed is False:
                self._logfile_send.close()
        
        # Close the logfile_prgm
        if self.logger:
            self.logger.__del__()
        self.logged_in = False

def main():
    pass


if __name__ == '__main__':
    main()

'''
# TODO: FUTURE
* Huge timeout_session causes the entire program
to slow down, rather than proceeding with next set of threads: asynch
'''
    